package org.eclipse.eatop.metamodelgen.serialization.generators.util;

@SuppressWarnings("nls")
public interface IEASTADLGeneratorConstants {
	// adapt version number here
	public static String EASTADL_VERSION = "2.2.0";
	// public static String EASTADL_VERSION = "2.1.12";

	// take first major and minor revision from EASTADL_VERSION
	public static String SHORT_VERSION = EASTADL_VERSION.replace(".", "").substring(0, 2);

	public static String EASTADL_NS_PREFIX = "http://east-adl.info";
	public static String GEASTADL_NAMESPACE = EASTADL_NS_PREFIX + "/geastadl";
	public static String EA_GLOBAL_NS = EASTADL_NS_PREFIX + "/" + EASTADL_VERSION;
	public static String EA_ROOT_PACKAGE_NS = EASTADL_NS_PREFIX + "/" + EASTADL_VERSION + "/eastadl" + SHORT_VERSION;
}
