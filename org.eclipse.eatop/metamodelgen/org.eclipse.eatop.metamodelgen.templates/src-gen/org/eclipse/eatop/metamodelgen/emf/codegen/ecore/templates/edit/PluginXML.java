package org.eclipse.eatop.metamodelgen.emf.codegen.ecore.templates.edit;

import java.util.*;
import org.eclipse.emf.codegen.ecore.genmodel.*;
import org.eclipse.eatop.metamodelgen.templates.internal.util.GenModels;

public class PluginXML
{
  protected static String nl;
  public static synchronized PluginXML create(String lineSeparator)
  {
    nl = lineSeparator;
    PluginXML result = new PluginXML();
    nl = null;
    return result;
  }

  public final String NL = nl == null ? (System.getProperties().getProperty("line.separator")) : nl;
  protected final String TEXT_1 = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" + NL + "<?eclipse version=\"3.0\"?>" + NL;
  protected final String TEXT_2 = NL;
  protected final String TEXT_3 = "<!--";
  protected final String TEXT_4 = NL + " ";
  protected final String TEXT_5 = NL + "   <copyright>" + NL + "  " + NL + "  Copyright (c) 2014 itemis and others." + NL + "  All rights reserved. This program and the accompanying materials" + NL + "  are made available under the terms of the Eclipse Public License v1.0" + NL + "  which accompanies this distribution, and is available at" + NL + "  http://www.eclipse.org/legal/epl-v10.html" + NL + "  " + NL + "  Contributors: " + NL + "      itemis - Initial API and implementation" + NL + "  " + NL + "  </copyright>";
  protected final String TEXT_6 = NL + "-->" + NL;
  protected final String TEXT_7 = NL + "<plugin>";
  protected final String TEXT_8 = NL + "<plugin" + NL + "      name=\"%pluginName\"" + NL + "      id=\"";
  protected final String TEXT_9 = "\"" + NL + "      version=\"0.5.0\"" + NL + "      provider-name=\"%providerName\"" + NL + "      class=\"";
  protected final String TEXT_10 = "$Implementation\">" + NL + "" + NL + "   <requires>";
  protected final String TEXT_11 = NL + "      <import plugin=\"";
  protected final String TEXT_12 = "\"";
  protected final String TEXT_13 = " export=\"true\"";
  protected final String TEXT_14 = "/>";
  protected final String TEXT_15 = NL + "   </requires>" + NL + "" + NL + "   <runtime>";
  protected final String TEXT_16 = NL + "      <library name=\"";
  protected final String TEXT_17 = ".jar\">";
  protected final String TEXT_18 = NL + "      <library name=\".\">";
  protected final String TEXT_19 = NL + "         <export name=\"*\"/>" + NL + "      </library>" + NL + "   </runtime>";
  protected final String TEXT_20 = NL + "  " + NL + "  <extension" + NL + "         point=\"org.eclipse.core.runtime.adapters\">" + NL + "      <factory" + NL + "            adaptableType=\"";
  protected final String TEXT_21 = "\"" + NL + "            class=\"";
  protected final String TEXT_22 = ".ItemLabelProviderAdapterFactory\">" + NL + "         <adapter" + NL + "               type=\"org.eclipse.emf.edit.provider.IItemLabelProvider\" />" + NL + "      </factory>" + NL + "   </extension>";
  protected final String TEXT_23 = NL + NL + "   <extension point=\"org.eclipse.emf.edit.itemProviderAdapterFactories\">" + NL + "      <factory" + NL + "            uri=\"";
  protected final String TEXT_24 = "\"" + NL + "            supportedTypes=";
  protected final String TEXT_25 = NL + "              ";
  protected final String TEXT_26 = "\"/>";
  protected final String TEXT_27 = NL + "   </extension>";
  protected final String TEXT_28 = NL + NL + "   <extension point=\"org.eclipse.emf.edit.childCreationExtenders\">";
  protected final String TEXT_29 = NL + "      <extender" + NL + "            uri=\"";
  protected final String TEXT_30 = "$";
  protected final String TEXT_31 = NL + NL + "   <extension point=\"org.eclipse.emf.ecore.generated_package\">" + NL + "      <package" + NL + "            uri=\"";
  protected final String TEXT_32 = NL + "            class=\"";
  protected final String TEXT_33 = "\"" + NL + "            genModel=\"";
  protected final String TEXT_34 = NL + NL + "   <extension point=\"org.eclipse.emf.ecore.content_parser\">" + NL + "      <parser" + NL + "            contentTypeIdentifier=\"";
  protected final String TEXT_35 = "\"/>" + NL + "   </extension>" + NL + "" + NL + "   <extension point=\"org.eclipse.core.contenttype.contentTypes\">" + NL + "      <content-type" + NL + "            base-type=\"";
  protected final String TEXT_36 = "\"" + NL + "            file-extensions=\"";
  protected final String TEXT_37 = "\"" + NL + "            id=\"";
  protected final String TEXT_38 = "\"" + NL + "            name=\"%_UI_";
  protected final String TEXT_39 = "_content_type\"" + NL + "            priority=\"normal\">" + NL + "         <describer class=\"org.eclipse.emf.ecore.xmi.impl.RootXMLContentHandlerImpl$Describer\">";
  protected final String TEXT_40 = NL + "            <parameter name=\"namespace\" value=\"";
  protected final String TEXT_41 = NL + "            <parameter name=\"kind\" value=\"xmi\"/>";
  protected final String TEXT_42 = NL + "         </describer>" + NL + "      </content-type>" + NL + "   </extension>";
  protected final String TEXT_43 = NL + NL + "   <extension point=\"org.eclipse.emf.ecore.extension_parser\">" + NL + "      <parser" + NL + "            type=\"";
  protected final String TEXT_44 = "\"/>" + NL + "   </extension>";
  protected final String TEXT_45 = NL + NL + "</plugin>";

  public String generate(Object argument)
  {
    final StringBuffer stringBuffer = new StringBuffer();
    
/**
 * <copyright>
 *
 * Copyright (c) 2002-2005 IBM Corporation and others.
 * All rights reserved.   This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors: 
 *   IBM - Initial API and implementation
 *
 * </copyright>
 */

    GenModel genModel = (GenModel)argument;
    stringBuffer.append(TEXT_1);
    stringBuffer.append(TEXT_2);
    stringBuffer.append(TEXT_3);
    {GenBase copyrightHolder = argument instanceof GenBase ? (GenBase)argument : argument instanceof Object[] && ((Object[])argument)[0] instanceof GenBase ? (GenBase)((Object[])argument)[0] : null;
    if (copyrightHolder != null && copyrightHolder.hasCopyright()) {
    stringBuffer.append(TEXT_4);
    stringBuffer.append(copyrightHolder.getCopyright(copyrightHolder.getGenModel().getIndentation(stringBuffer)));
    } else {
    stringBuffer.append(TEXT_5);
    }}
    stringBuffer.append(TEXT_6);
    if (genModel.isBundleManifest()) {
    stringBuffer.append(TEXT_7);
    } else {
    stringBuffer.append(TEXT_8);
    stringBuffer.append(genModel.getEditPluginID());
    stringBuffer.append(TEXT_9);
    stringBuffer.append(genModel.getQualifiedEditPluginClassName());
    stringBuffer.append(TEXT_10);
    for (String pluginID : genModel.getEditRequiredPlugins()) {
    stringBuffer.append(TEXT_11);
    stringBuffer.append(pluginID);
    stringBuffer.append(TEXT_12);
    if (!pluginID.startsWith("org.eclipse.core.runtime")) {
    stringBuffer.append(TEXT_13);
    }
    stringBuffer.append(TEXT_14);
    }
    stringBuffer.append(TEXT_15);
    if (genModel.isRuntimeJar()) {
    stringBuffer.append(TEXT_16);
    stringBuffer.append(genModel.getEditPluginID());
    stringBuffer.append(TEXT_17);
    } else {
    stringBuffer.append(TEXT_18);
    }
    stringBuffer.append(TEXT_19);
    }
    List<GenPackage> genPackagesWithClassifiers = genModel.getAllGenPackagesWithClassifiers();

    for (GenPackage genPackage : genPackagesWithClassifiers) {
    if(genPackage == GenModels.getRootGenPackage(genModel)) {
    stringBuffer.append(TEXT_20);
    stringBuffer.append(GenModels.getUtilityClassName(genModel, "ReleaseDescriptor"));
    stringBuffer.append(TEXT_21);
    stringBuffer.append(genModel.getEditPluginPackageName());
    stringBuffer.append(TEXT_22);
    }
    if (!genPackage.getGenClasses().isEmpty()) {
    stringBuffer.append(TEXT_23);
    stringBuffer.append(genPackage.getNSURI());
    stringBuffer.append(TEXT_21);
    stringBuffer.append(genPackage.getQualifiedItemProviderAdapterFactoryClassName());
    stringBuffer.append(TEXT_24);
    for (ListIterator<?> j = genPackage.getProviderSupportedTypes().listIterator(); j.hasNext(); ) {
    stringBuffer.append(TEXT_25);
    stringBuffer.append(j.hasPrevious() ? " " : "\"");
    stringBuffer.append(j.next());
    if (!j.hasNext()) {
    stringBuffer.append(TEXT_26);
    }
    }
    stringBuffer.append(TEXT_27);
    if (genPackage.isChildCreationExtenders()) { Map<GenPackage, Map<GenClass, List<GenClass.ChildCreationData>>> extendedChildCreationData = genPackage.getExtendedChildCreationData();
    if (!extendedChildCreationData.isEmpty()) {
    stringBuffer.append(TEXT_28);
    for (Map.Entry<GenPackage, Map<GenClass, List<GenClass.ChildCreationData>>> entry : extendedChildCreationData.entrySet()) {
    stringBuffer.append(TEXT_29);
    stringBuffer.append(entry.getKey().getNSURI());
    stringBuffer.append(TEXT_21);
    stringBuffer.append(genPackage.getQualifiedItemProviderAdapterFactoryClassName());
    stringBuffer.append(TEXT_30);
    stringBuffer.append(genPackage.getChildCreationExtenderName(entry.getKey()));
    stringBuffer.append(TEXT_26);
    }
    stringBuffer.append(TEXT_27);
    }
    }
    }
    }
    if (genModel.sameModelEditProject()) {
    for (GenPackage genPackage : genModel.getAllGenPackagesWithClassifiers()) {
    stringBuffer.append(TEXT_31);
    stringBuffer.append(genPackage.getNSURI());
    stringBuffer.append(TEXT_12);
    if (genModel.hasLocalGenModel()) {
    stringBuffer.append(TEXT_32);
    stringBuffer.append(genPackage.getQualifiedPackageInterfaceName());
    stringBuffer.append(TEXT_33);
    stringBuffer.append(genModel.getRelativeGenModelLocation());
    stringBuffer.append(TEXT_26);
    } else {
    stringBuffer.append(TEXT_32);
    stringBuffer.append(genPackage.getQualifiedPackageInterfaceName());
    stringBuffer.append(TEXT_26);
    }
    stringBuffer.append(TEXT_27);
    if (genPackage.isContentType()) {
    stringBuffer.append(TEXT_34);
    stringBuffer.append(genPackage.getContentTypeIdentifier());
    stringBuffer.append(TEXT_21);
    stringBuffer.append(genPackage.getQualifiedEffectiveResourceFactoryClassName());
    stringBuffer.append(TEXT_35);
    stringBuffer.append(genPackage.isXMIResource() ? "org.eclipse.emf.ecore.xmi" : "org.eclipse.core.runtime.xml");
    stringBuffer.append(TEXT_36);
    stringBuffer.append(genPackage.getFileExtensions());
    stringBuffer.append(TEXT_37);
    stringBuffer.append(genPackage.getContentTypeIdentifier());
    stringBuffer.append(TEXT_38);
    stringBuffer.append(genPackage.getPrefix());
    stringBuffer.append(TEXT_39);
    if (genPackage.hasTargetNamespace()) {
    stringBuffer.append(TEXT_40);
    stringBuffer.append(genPackage.getNSURI());
    stringBuffer.append(TEXT_26);
    }
    if (genPackage.isXMIResource()) {
    stringBuffer.append(TEXT_41);
    }
    stringBuffer.append(TEXT_42);
    } else if (genPackage.getResource() != GenResourceKind.NONE_LITERAL) {
    stringBuffer.append(TEXT_43);
    stringBuffer.append(genPackage.getFileExtension());
    stringBuffer.append(TEXT_21);
    stringBuffer.append(genPackage.getQualifiedResourceFactoryClassName());
    stringBuffer.append(TEXT_44);
    }
    }
    }
    stringBuffer.append(TEXT_45);
    stringBuffer.append(TEXT_2);
    return stringBuffer.toString();
  }
}
