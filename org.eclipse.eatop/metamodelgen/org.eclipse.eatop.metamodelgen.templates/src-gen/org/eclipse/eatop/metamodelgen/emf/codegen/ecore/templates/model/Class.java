package org.eclipse.eatop.metamodelgen.emf.codegen.ecore.templates.model;

import java.util.*;
import org.eclipse.emf.codegen.ecore.genmodel.*;
import org.eclipse.emf.codegen.util.CodeGenUtil;
import org.eclipse.emf.ecore.*;
import org.eclipse.eatop.metamodelgen.templates.internal.genmodel.geastadl.*;
import org.eclipse.eatop.metamodelgen.templates.internal.util.*;

public class Class
{
  protected static String nl;
  public static synchronized Class create(String lineSeparator)
  {
    nl = lineSeparator;
    Class result = new Class();
    nl = null;
    return result;
  }

  public final String NL = nl == null ? (System.getProperties().getProperty("line.separator")) : nl;
  protected final String TEXT_1 = "";
  protected final String TEXT_2 = "/**";
  protected final String TEXT_3 = NL + " * ";
  protected final String TEXT_4 = NL + " * <copyright>" + NL + " * " + NL + " * Copyright (c) 2014 itemis and others." + NL + " * All rights reserved. This program and the accompanying materials" + NL + " * are made available under the terms of the Eclipse Public License v1.0" + NL + " * which accompanies this distribution, and is available at" + NL + " * http://www.eclipse.org/legal/epl-v10.html" + NL + " * " + NL + " * Contributors: " + NL + " *     itemis - Initial API and implementation" + NL + " * " + NL + " * </copyright>";
  protected final String TEXT_5 = NL + " */";
  protected final String TEXT_6 = NL + "package ";
  protected final String TEXT_7 = ";";
  protected final String TEXT_8 = NL;
  protected final String TEXT_9 = NL + "/**" + NL + " * <!-- begin-user-doc -->" + NL + " * A representation of the model object '<em><b>";
  protected final String TEXT_10 = "</b></em>'." + NL + " * <!-- end-user-doc -->";
  protected final String TEXT_11 = NL + " *" + NL + " * <!-- begin-model-doc -->" + NL + " * ";
  protected final String TEXT_12 = NL + " * <!-- end-model-doc -->";
  protected final String TEXT_13 = NL + " * " + NL + " * @deprecated ";
  protected final String TEXT_14 = NL + " *";
  protected final String TEXT_15 = NL + " * <p>" + NL + " * The following features are supported:" + NL + " * <ul>";
  protected final String TEXT_16 = NL + " *   <li>{@link ";
  protected final String TEXT_17 = "#";
  protected final String TEXT_18 = " <em>";
  protected final String TEXT_19 = "</em>}</li>";
  protected final String TEXT_20 = NL + " * </ul>" + NL + " * </p>";
  protected final String TEXT_21 = NL + " * @see ";
  protected final String TEXT_22 = "#get";
  protected final String TEXT_23 = "()";
  protected final String TEXT_24 = NL + " * @model ";
  protected final String TEXT_25 = NL + " *        ";
  protected final String TEXT_26 = NL + " * @model";
  protected final String TEXT_27 = NL + " * @extends ";
  protected final String TEXT_28 = NL + " * @generated" + NL + " */";
  protected final String TEXT_29 = NL + "@Deprecated";
  protected final String TEXT_30 = NL + "/**" + NL + " * <!-- begin-user-doc -->" + NL + " * An implementation of the model object '<em><b>";
  protected final String TEXT_31 = "</b></em>'." + NL + " * <!-- end-user-doc -->" + NL + " * <p>";
  protected final String TEXT_32 = NL + " * The following features are implemented:" + NL + " * <ul>";
  protected final String TEXT_33 = NL + " * </ul>";
  protected final String TEXT_34 = NL + " * </p>" + NL + " *" + NL + " * @generated" + NL + " */";
  protected final String TEXT_35 = NL + "public";
  protected final String TEXT_36 = " abstract";
  protected final String TEXT_37 = " class ";
  protected final String TEXT_38 = NL + "public interface ";
  protected final String TEXT_39 = NL + "{";
  protected final String TEXT_40 = NL + "\t/**" + NL + "\t * <!-- begin-user-doc -->" + NL + "\t * <!-- end-user-doc -->" + NL + "\t * @generated" + NL + "\t */" + NL + "\t";
  protected final String TEXT_41 = " copyright = ";
  protected final String TEXT_42 = NL + "\t/**" + NL + "\t * <!-- begin-user-doc -->" + NL + "\t * <!-- end-user-doc -->" + NL + "\t * @generated" + NL + "\t */" + NL + "\tpublic static final ";
  protected final String TEXT_43 = " mofDriverNumber = \"";
  protected final String TEXT_44 = "\";";
  protected final String TEXT_45 = NL + "\t/**" + NL + "\t * <!-- begin-user-doc -->" + NL + "\t * <!-- end-user-doc -->" + NL + "\t * @generated" + NL + "\t */" + NL + "\tprivate static final long serialVersionUID = 1L;" + NL;
  protected final String TEXT_46 = NL + "\t/**" + NL + "\t * An array of objects representing the values of non-primitive features." + NL + "\t * <!-- begin-user-doc -->" + NL + "\t * <!-- end-user-doc -->" + NL + "\t * @generated" + NL + "\t */";
  protected final String TEXT_47 = NL + "\t@";
  protected final String TEXT_48 = NL + "\tprotected Object[] ";
  protected final String TEXT_49 = ";" + NL;
  protected final String TEXT_50 = NL + "\t/**" + NL + "\t * A bit field representing the indices of non-primitive feature values." + NL + "\t * <!-- begin-user-doc -->" + NL + "\t * <!-- end-user-doc -->" + NL + "\t * @generated" + NL + "\t */";
  protected final String TEXT_51 = NL + "\tprotected int ";
  protected final String TEXT_52 = NL + "\t/**" + NL + "\t * A set of bit flags representing the values of boolean attributes and whether unsettable features have been set." + NL + "\t * <!-- begin-user-doc -->" + NL + "\t * <!-- end-user-doc -->" + NL + "\t * @generated" + NL + "\t * @ordered" + NL + "\t */";
  protected final String TEXT_53 = " = 0;" + NL;
  protected final String TEXT_54 = NL + "\t/**" + NL + "\t * The cached setting delegate for the '{@link #";
  protected final String TEXT_55 = "() <em>";
  protected final String TEXT_56 = "</em>}' ";
  protected final String TEXT_57 = "." + NL + "\t * <!-- begin-user-doc -->" + NL + "\t * <!-- end-user-doc -->" + NL + "\t * @see #";
  protected final String TEXT_58 = "()" + NL + "\t * @generated" + NL + "\t * @ordered" + NL + "\t */";
  protected final String TEXT_59 = NL + "\tprotected ";
  protected final String TEXT_60 = ".Internal.SettingDelegate ";
  protected final String TEXT_61 = "__ESETTING_DELEGATE = ((";
  protected final String TEXT_62 = ".Internal)";
  protected final String TEXT_63 = ").getSettingDelegate();" + NL;
  protected final String TEXT_64 = NL + "\t/**" + NL + "\t * The cached value of the '{@link #";
  protected final String TEXT_65 = " ";
  protected final String TEXT_66 = NL + "\t/**" + NL + "\t * The empty value for the '{@link #";
  protected final String TEXT_67 = "</em>}' array accessor." + NL + "\t * <!-- begin-user-doc -->" + NL + "\t * <!-- end-user-doc -->" + NL + "\t * @see #";
  protected final String TEXT_68 = NL + "\t@SuppressWarnings(\"unchecked\")";
  protected final String TEXT_69 = NL + "\tprotected static final ";
  protected final String TEXT_70 = "[] ";
  protected final String TEXT_71 = "_EEMPTY_ARRAY = new ";
  protected final String TEXT_72 = " [0]";
  protected final String TEXT_73 = NL + "\t/**" + NL + "\t * The default value of the '{@link #";
  protected final String TEXT_74 = "; // TODO The default value literal \"";
  protected final String TEXT_75 = "\" is not valid.";
  protected final String TEXT_76 = " = ";
  protected final String TEXT_77 = NL + "\t/**" + NL + "\t * An additional set of bit flags representing the values of boolean attributes and whether unsettable features have been set." + NL + "\t * <!-- begin-user-doc -->" + NL + "\t * <!-- end-user-doc -->" + NL + "\t * @generated" + NL + "\t * @ordered" + NL + "\t */";
  protected final String TEXT_78 = NL + "\t/**" + NL + "\t * The offset of the flags representing the value of the '{@link #";
  protected final String TEXT_79 = "." + NL + "\t * <!-- begin-user-doc -->" + NL + "\t * <!-- end-user-doc -->" + NL + "\t * @generated" + NL + "\t * @ordered" + NL + "\t */" + NL + "\tprotected static final int ";
  protected final String TEXT_80 = "_EFLAG_OFFSET = ";
  protected final String TEXT_81 = ";" + NL + "" + NL + "\t/**" + NL + "\t * The flags representing the default value of the '{@link #";
  protected final String TEXT_82 = "_EFLAG_DEFAULT = ";
  protected final String TEXT_83 = ".ordinal()";
  protected final String TEXT_84 = ".VALUES.indexOf(";
  protected final String TEXT_85 = ")";
  protected final String TEXT_86 = " << ";
  protected final String TEXT_87 = "_EFLAG_OFFSET;" + NL + "" + NL + "\t/**" + NL + "\t * The array of enumeration values for '{@link ";
  protected final String TEXT_88 = "}'" + NL + "\t * <!-- begin-user-doc -->" + NL + "\t * <!-- end-user-doc -->" + NL + "\t * @generated" + NL + "\t * @ordered" + NL + "\t */" + NL + "\tprivate static final ";
  protected final String TEXT_89 = "_EFLAG_VALUES = ";
  protected final String TEXT_90 = ".values()";
  protected final String TEXT_91 = "(";
  protected final String TEXT_92 = "[])";
  protected final String TEXT_93 = ".VALUES.toArray(new ";
  protected final String TEXT_94 = "[";
  protected final String TEXT_95 = ".VALUES.size()])";
  protected final String TEXT_96 = NL + "\t/**" + NL + "\t * The flag";
  protected final String TEXT_97 = " representing the value of the '{@link #";
  protected final String TEXT_98 = "()" + NL + "\t * @generated" + NL + "\t * @ordered" + NL + "\t */" + NL + "\tprotected static final int ";
  protected final String TEXT_99 = "_EFLAG = ";
  protected final String TEXT_100 = "_EFLAG_OFFSET";
  protected final String TEXT_101 = NL + "\t/**" + NL + "\t * The flag representing whether the ";
  protected final String TEXT_102 = " has been set." + NL + "\t * <!-- begin-user-doc -->" + NL + "\t * <!-- end-user-doc -->" + NL + "\t * @generated" + NL + "\t * @ordered" + NL + "\t */" + NL + "\tprotected static final int ";
  protected final String TEXT_103 = "_ESETFLAG = 1 << ";
  protected final String TEXT_104 = NL + "\t/**" + NL + "\t * This is true if the ";
  protected final String TEXT_105 = " has been set." + NL + "\t * <!-- begin-user-doc -->" + NL + "\t * <!-- end-user-doc -->" + NL + "\t * @generated" + NL + "\t * @ordered" + NL + "\t */";
  protected final String TEXT_106 = NL + "\tprotected boolean ";
  protected final String TEXT_107 = "ESet;" + NL;
  protected final String TEXT_108 = NL + "\t/**" + NL + "\t * <!-- begin-user-doc -->" + NL + "\t * <!-- end-user-doc -->" + NL + "\t * @generated" + NL + "\t */" + NL + "\tprivate static final int ";
  protected final String TEXT_109 = ".getFeatureID(";
  protected final String TEXT_110 = ") - ";
  protected final String TEXT_111 = NL + "\t/**" + NL + "\t * <!-- begin-user-doc -->" + NL + "\t * <!-- end-user-doc -->" + NL + "\t * @generated" + NL + "\t */" + NL + "\tprivate static final int \"EOPERATION_OFFSET_CORRECTION\" = ";
  protected final String TEXT_112 = ".getOperationID(";
  protected final String TEXT_113 = "public";
  protected final String TEXT_114 = "protected";
  protected final String TEXT_115 = "()" + NL + "\t{" + NL + "\t\tsuper();";
  protected final String TEXT_116 = NL + "\t\t";
  protected final String TEXT_117 = " |= ";
  protected final String TEXT_118 = "_EFLAG";
  protected final String TEXT_119 = "_DEFAULT";
  protected final String TEXT_120 = NL + "\t}" + NL + "" + NL + "\t/**" + NL + "\t * <!-- begin-user-doc -->" + NL + "\t * <!-- end-user-doc -->" + NL + "\t * @generated" + NL + "\t */";
  protected final String TEXT_121 = NL + "\t@Override";
  protected final String TEXT_122 = " eStaticClass()" + NL + "\t{" + NL + "\t\treturn ";
  protected final String TEXT_123 = ";" + NL + "\t}" + NL;
  protected final String TEXT_124 = NL + "\t/**" + NL + "\t * <!-- begin-user-doc -->" + NL + "\t * <!-- end-user-doc -->" + NL + "\t * @generated" + NL + "\t */";
  protected final String TEXT_125 = NL + "\tprotected int eStaticFeatureCount()" + NL + "\t{" + NL + "\t\treturn ";
  protected final String TEXT_126 = NL + "\t";
  protected final String TEXT_127 = "();" + NL;
  protected final String TEXT_128 = NL + "\tpublic ";
  protected final String TEXT_129 = "()" + NL + "\t{";
  protected final String TEXT_130 = " list = (";
  protected final String TEXT_131 = "();" + NL + "\t\tif (list.isEmpty()) return ";
  protected final String TEXT_132 = "_EEMPTY_ARRAY;";
  protected final String TEXT_133 = NL + "\t\tif (";
  protected final String TEXT_134 = " == null || ";
  protected final String TEXT_135 = ".isEmpty()) return ";
  protected final String TEXT_136 = "_EEMPTY_ARRAY;" + NL + "\t\t";
  protected final String TEXT_137 = NL + "\t\tlist.shrink();" + NL + "\t\treturn (";
  protected final String TEXT_138 = "[])list.data();" + NL + "\t}" + NL;
  protected final String TEXT_139 = " get";
  protected final String TEXT_140 = "(int index);" + NL;
  protected final String TEXT_141 = "(int index)" + NL + "\t{" + NL + "\t\treturn ";
  protected final String TEXT_142 = "().get(index);" + NL + "\t}" + NL;
  protected final String TEXT_143 = NL + "\tint get";
  protected final String TEXT_144 = "Length();" + NL;
  protected final String TEXT_145 = NL + "\tpublic int get";
  protected final String TEXT_146 = "Length()" + NL + "\t{";
  protected final String TEXT_147 = NL + "\t\treturn ";
  protected final String TEXT_148 = "().size();";
  protected final String TEXT_149 = " == null ? 0 : ";
  protected final String TEXT_150 = ".size();";
  protected final String TEXT_151 = NL + "\t}" + NL;
  protected final String TEXT_152 = NL + "\tvoid ";
  protected final String TEXT_153 = "[] new";
  protected final String TEXT_154 = ");" + NL;
  protected final String TEXT_155 = NL + "\tpublic void ";
  protected final String TEXT_156 = ")" + NL + "\t{" + NL + "\t\t((";
  protected final String TEXT_157 = "()).setData(new";
  protected final String TEXT_158 = ".length, new";
  protected final String TEXT_159 = ");" + NL + "\t}" + NL;
  protected final String TEXT_160 = "(int index, ";
  protected final String TEXT_161 = " element);" + NL;
  protected final String TEXT_162 = " element)" + NL + "\t{" + NL + "\t\t";
  protected final String TEXT_163 = "().set(index, element);" + NL + "\t}" + NL;
  protected final String TEXT_164 = NL + "\t/**" + NL + "\t * Returns the value of the '<em><b>";
  protected final String TEXT_165 = "</b></em>' ";
  protected final String TEXT_166 = ".";
  protected final String TEXT_167 = NL + "\t * The key is of type ";
  protected final String TEXT_168 = "list of {@link ";
  protected final String TEXT_169 = "}";
  protected final String TEXT_170 = "{@link ";
  protected final String TEXT_171 = "," + NL + "\t * and the value is of type ";
  protected final String TEXT_172 = ",";
  protected final String TEXT_173 = NL + "\t * The list contents are of type {@link ";
  protected final String TEXT_174 = NL + "\t * The default value is <code>";
  protected final String TEXT_175 = "</code>.";
  protected final String TEXT_176 = NL + "\t * The literals are from the enumeration {@link ";
  protected final String TEXT_177 = "}.";
  protected final String TEXT_178 = NL + "\t * It is bidirectional and its opposite is '{@link ";
  protected final String TEXT_179 = "</em>}'.";
  protected final String TEXT_180 = NL + "\t * <!-- begin-user-doc -->";
  protected final String TEXT_181 = NL + "\t * <p>" + NL + "\t * If the meaning of the '<em>";
  protected final String TEXT_182 = "</em>' ";
  protected final String TEXT_183 = " isn't clear," + NL + "\t * there really should be more of a description here..." + NL + "\t * </p>";
  protected final String TEXT_184 = NL + "\t * <!-- end-user-doc -->";
  protected final String TEXT_185 = NL + "\t * <!-- begin-model-doc -->" + NL + "\t * ";
  protected final String TEXT_186 = NL + "\t * <!-- end-model-doc -->";
  protected final String TEXT_187 = NL + "\t * @return the value of the '<em>";
  protected final String TEXT_188 = NL + "\t * @see ";
  protected final String TEXT_189 = NL + "\t * @see #";
  protected final String TEXT_190 = NL + "\t * @see #set";
  protected final String TEXT_191 = NL + "\t * @model ";
  protected final String TEXT_192 = NL + "\t *        ";
  protected final String TEXT_193 = NL + "\t * @model";
  protected final String TEXT_194 = NL + "\t * @generated" + NL + "\t */";
  protected final String TEXT_195 = "_";
  protected final String TEXT_196 = ")eDynamicGet(";
  protected final String TEXT_197 = ", ";
  protected final String TEXT_198 = ", true, ";
  protected final String TEXT_199 = ").";
  protected final String TEXT_200 = ")eGet(";
  protected final String TEXT_201 = ", true)";
  protected final String TEXT_202 = "__ESETTING_DELEGATE.dynamicGet(this, null, 0, true, false)";
  protected final String TEXT_203 = " = (";
  protected final String TEXT_204 = ")eVirtualGet(";
  protected final String TEXT_205 = ");";
  protected final String TEXT_206 = " == null)" + NL + "\t\t{";
  protected final String TEXT_207 = NL + "\t\t\teVirtualSet(";
  protected final String TEXT_208 = " = new ";
  protected final String TEXT_209 = NL + "\t\t\t";
  protected final String TEXT_210 = NL + "\t\t}" + NL + "\t\treturn ";
  protected final String TEXT_211 = NL + "\t\tif (eContainerFeatureID() != ";
  protected final String TEXT_212 = ") return null;" + NL + "\t\treturn (";
  protected final String TEXT_213 = ")eContainer();";
  protected final String TEXT_214 = " != null && ";
  protected final String TEXT_215 = ".eIsProxy())" + NL + "\t\t{" + NL + "\t\t\t";
  protected final String TEXT_216 = " old";
  protected final String TEXT_217 = ";" + NL + "\t\t\t";
  protected final String TEXT_218 = "eResolveProxy(old";
  protected final String TEXT_219 = ");" + NL + "\t\t\tif (";
  protected final String TEXT_220 = " != old";
  protected final String TEXT_221 = ")" + NL + "\t\t\t{";
  protected final String TEXT_222 = NL + "\t\t\t\t";
  protected final String TEXT_223 = " new";
  protected final String TEXT_224 = " msgs = old";
  protected final String TEXT_225 = ".eInverseRemove(this, EOPPOSITE_FEATURE_BASE - ";
  protected final String TEXT_226 = ", null, null);";
  protected final String TEXT_227 = " msgs =  old";
  protected final String TEXT_228 = ".eInverseRemove(this, ";
  protected final String TEXT_229 = ".class, null);";
  protected final String TEXT_230 = NL + "\t\t\t\tif (new";
  protected final String TEXT_231 = ".eInternalContainer() == null)" + NL + "\t\t\t\t{";
  protected final String TEXT_232 = NL + "\t\t\t\t\tmsgs = new";
  protected final String TEXT_233 = ".eInverseAdd(this, EOPPOSITE_FEATURE_BASE - ";
  protected final String TEXT_234 = ", null, msgs);";
  protected final String TEXT_235 = NL + "\t\t\t\t\tmsgs =  new";
  protected final String TEXT_236 = ".eInverseAdd(this, ";
  protected final String TEXT_237 = ".class, msgs);";
  protected final String TEXT_238 = NL + "\t\t\t\t}" + NL + "\t\t\t\tif (msgs != null) msgs.dispatch();";
  protected final String TEXT_239 = NL + "\t\t\t\teVirtualSet(";
  protected final String TEXT_240 = NL + "\t\t\t\tif (eNotificationRequired())" + NL + "\t\t\t\t\teNotify(new ";
  protected final String TEXT_241 = "(this, ";
  protected final String TEXT_242 = ".RESOLVE, ";
  protected final String TEXT_243 = ", old";
  protected final String TEXT_244 = "));";
  protected final String TEXT_245 = NL + "\t\t\t}" + NL + "\t\t}";
  protected final String TEXT_246 = NL + "\t\treturn (";
  protected final String TEXT_247 = " & ";
  protected final String TEXT_248 = "_EFLAG) != 0;";
  protected final String TEXT_249 = "_EFLAG_VALUES[(";
  protected final String TEXT_250 = "_EFLAG) >>> ";
  protected final String TEXT_251 = "_EFLAG_OFFSET];";
  protected final String TEXT_252 = " = basicGet";
  protected final String TEXT_253 = "();" + NL + "\t\treturn ";
  protected final String TEXT_254 = ".eIsProxy() ? ";
  protected final String TEXT_255 = "eResolveProxy((";
  protected final String TEXT_256 = ") : ";
  protected final String TEXT_257 = NL + "\t\treturn new ";
  protected final String TEXT_258 = "((";
  protected final String TEXT_259 = ".Internal)((";
  protected final String TEXT_260 = ".Internal.Wrapper)get";
  protected final String TEXT_261 = "()).featureMap().";
  protected final String TEXT_262 = "list(";
  protected final String TEXT_263 = ")get";
  protected final String TEXT_264 = "().";
  protected final String TEXT_265 = NL + "\t\treturn ((";
  protected final String TEXT_266 = "()).featureMap().list(";
  protected final String TEXT_267 = NL + "\t\treturn get";
  protected final String TEXT_268 = "().list(";
  protected final String TEXT_269 = "()).featureMap().get(";
  protected final String TEXT_270 = "get";
  protected final String TEXT_271 = "().get(";
  protected final String TEXT_272 = "ERROR: The mapping to the concrete features could not be resolved." + NL + "\t";
  protected final String TEXT_273 = "\t\t\t\t" + NL + "\t\t\t\t\t";
  protected final String TEXT_274 = " untypedList = ";
  protected final String TEXT_275 = "();" + NL + "\t\t\t\t\treturn untypedList;" + NL + "\t\t\t\t";
  protected final String TEXT_276 = NL + "\t\t\t\t\t";
  protected final String TEXT_277 = "ERROR: This case is not supported by the GEASTADL generator!" + NL + "\t\t\t\t\t[List => JavaMappedType]" + NL + "\t\t\t\t";
  protected final String TEXT_278 = "ERROR: This case is not supported by the GEASTADL generator!" + NL + "\t\t\t\t\t[List => Enum] " + NL + "\t\t\t\t";
  protected final String TEXT_279 = NL + "\t\t\t\t\t// TODO: Provide an implementation which allows to modify the content of the EList in an appropriate way." + NL + "\t\t\t\t\t";
  protected final String TEXT_280 = "();\t" + NL + "\t\t\t\t\tif(";
  protected final String TEXT_281 = " != null) {" + NL + "\t\t\t  \t\t\treturn new ";
  protected final String TEXT_282 = ".UnmodifiableEList<";
  protected final String TEXT_283 = ">(this, eClass().getEStructuralFeature(\"";
  protected final String TEXT_284 = "\"), 1, new Object[]{";
  protected final String TEXT_285 = "()});" + NL + "\t\t\t\t\t}" + NL + "\t\t\t\t\treturn new ";
  protected final String TEXT_286 = "\"), 0, new Object[]{});\t\t\t\t\t\t\t\t\t" + NL + "\t\t\t\t";
  protected final String TEXT_287 = "\t\t\t" + NL + "\t\t\t";
  protected final String TEXT_288 = "ERROR: This case is not supported by the GEASTADL generator!" + NL + "\t\t\t\t\t[JavaMappedType => List]" + NL + "\t\t\t\t";
  protected final String TEXT_289 = " tgtFeatureValue = ";
  protected final String TEXT_290 = "();" + NL + "\t\t\t\t\t";
  protected final String TEXT_291 = NL + "\treturn tgtFeatureValue.intValue();";
  protected final String TEXT_292 = NL + "\treturn new Integer(tgtFeatureValue);";
  protected final String TEXT_293 = NL + "\treturn\tnew Double(tgtFeatureValue);";
  protected final String TEXT_294 = NL + "\treturn new ";
  protected final String TEXT_295 = "(tgtFeatureValue);";
  protected final String TEXT_296 = "(tgtFeatureValue);\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
  protected final String TEXT_297 = "\t\t\t\t\t\t" + NL + "\treturn tgtFeatureValue;";
  protected final String TEXT_298 = "\t\t\t\t\t" + NL + "\t\t\t\t";
  protected final String TEXT_299 = "ERROR: This case is not supported by the GEASTADL generator!" + NL + "\t\t\t\t\t[JavaMappedType => Enum] " + NL + "\t\t\t\t";
  protected final String TEXT_300 = "ERROR: This case is not supported by the GEASTADL generator!" + NL + "\t\t\t\t\t[JavaMappedType => EObject] " + NL + "\t\t\t\t";
  protected final String TEXT_301 = "\t\t\t\t" + NL + "\t\t\t";
  protected final String TEXT_302 = NL + "\t\t\t\t\treturn ";
  protected final String TEXT_303 = ".getByName(";
  protected final String TEXT_304 = "().getName());" + NL + "\t\t\t\t";
  protected final String TEXT_305 = "ERROR: This case is not supported by the GEASTADL generator!" + NL + "\t\t\t\t\t[Enum => List]" + NL + "\t\t\t\t";
  protected final String TEXT_306 = "ERROR: This case is not supported by the GEASTADL generator!" + NL + "\t\t\t\t\t[Enum => JavaMappedType]\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t" + NL + "\t\t\t\t";
  protected final String TEXT_307 = "ERROR: This case is not supported by the GEASTADL generator!" + NL + "\t\t\t\t\t[Enum => EObject]" + NL + "\t\t\t\t";
  protected final String TEXT_308 = "   \t\t\t\t\t\t\t" + NL + "\t\t\t";
  protected final String TEXT_309 = " list = ";
  protected final String TEXT_310 = "();" + NL + "\t\t\t\t\tif(list.isEmpty()) {" + NL + "\t\t\t\t\t\treturn null;" + NL + "\t\t\t\t\t}" + NL + "\t\t\t\t\tif(list.size() > 1) {" + NL + "\t\t\t\t\t\tthrow new UnsupportedOperationException(\"Multiplicity Conflict: More than one element were found. GEastadl can not reduce them to one.\");" + NL + "\t\t\t\t\t}" + NL + "\t\t\t\t\t";
  protected final String TEXT_311 = NL + "\t\t\t\t\t\treturn (";
  protected final String TEXT_312 = ")list.get(0);" + NL + "\t\t\t\t\t";
  protected final String TEXT_313 = NL + "\t\t\t\t\t\treturn list.get(0);" + NL + "\t\t\t\t\t";
  protected final String TEXT_314 = "ERROR: This case is not supported by the GEASTADL generator!" + NL + "\t\t\t\t\t[EObject => JavaMappedType]\t" + NL + "\t\t\t\t";
  protected final String TEXT_315 = "ERROR: This case is not supported by the GEASTADL generator!" + NL + "\t\t\t\t\t[EObject => Enum]\t\t\t\t" + NL + "\t\t\t\t";
  protected final String TEXT_316 = "\t\t\t\t\t" + NL + "\t\t\t\t\treturn ";
  protected final String TEXT_317 = "();\t\t\t\t\t\t\t\t" + NL + "\t\t\t\t";
  protected final String TEXT_318 = "\t" + NL + "\t\t";
  protected final String TEXT_319 = "\t\t\t\t\t\t" + NL + "\t\t\t";
  protected final String TEXT_320 = " untypedList = new ";
  protected final String TEXT_321 = "();\t\t   \t\t";
  protected final String TEXT_322 = NL + "\tuntypedList.addAll(";
  protected final String TEXT_323 = "());";
  protected final String TEXT_324 = "();" + NL + "\tif(";
  protected final String TEXT_325 = " != null) {" + NL + "\t\tuntypedList.add(";
  protected final String TEXT_326 = "());" + NL + "\t}";
  protected final String TEXT_327 = "  \t\t";
  protected final String TEXT_328 = " resultObjects = new ";
  protected final String TEXT_329 = "();\t\t   \t\t\t";
  protected final String TEXT_330 = NL + "\tresultObjects.clear();" + NL + "\tfor(Object previousObject : untypedList) {" + NL + "\t\t";
  protected final String TEXT_331 = NL + "\t\t\tresultObjects.addAll(((";
  protected final String TEXT_332 = ")previousObject).";
  protected final String TEXT_333 = "());\t" + NL + "\t\t";
  protected final String TEXT_334 = NL + "\t\t\tObject resultObject = ((";
  protected final String TEXT_335 = "();\t\t\t\t\t\t\t\t\t" + NL + "\t\t\tif(resultObject != null) {\t\t   \t\t\t\t\t\t" + NL + "\t\t\t\tresultObjects.add(resultObject);" + NL + "\t\t\t}" + NL + "\t\t";
  protected final String TEXT_336 = NL + "\t}" + NL + "\tuntypedList.clear();" + NL + "\tuntypedList.addAll(resultObjects);\t\t\t\t\t" + NL + "\t";
  protected final String TEXT_337 = NL + "\t\t\t\t\treturn new ";
  protected final String TEXT_338 = "<";
  protected final String TEXT_339 = "\"), untypedList.size(), untypedList.toArray());\t\t\t\t\t\t\t\t\t" + NL + "\t\t\t\t";
  protected final String TEXT_340 = "ERROR: This case is not supported by the GEASTADL generator!" + NL + "\t\t\t\t\t[List => Enum]" + NL + "\t\t\t\t";
  protected final String TEXT_341 = "ERROR: This case is not supported by the GEASTADL generator!" + NL + "\t\t\t\t\t[List => EObject]\t\t\t\t\t\t\t\t\t" + NL + "\t\t\t\t";
  protected final String TEXT_342 = "\t\t\t\t\t " + NL + "\t\t\t\t\t";
  protected final String TEXT_343 = " == null) {" + NL + "\t\t";
  protected final String TEXT_344 = ".eINSTANCE.create";
  protected final String TEXT_345 = "();" + NL + "\t}" + NL + "\t";
  protected final String TEXT_346 = "();\t\t\t\t\t" + NL + "\t\t\t\t\t";
  protected final String TEXT_347 = "ERROR: This case is not supported by the GEASTADL generator!" + NL + "\t\t\t\t\t[JavaMappedType => Enum]" + NL + "\t\t\t\t";
  protected final String TEXT_348 = "ERROR: This case is not supported by the GEASTADL generator!" + NL + "\t\t\t\t\t[EObject => List]" + NL + "\t\t\t\t";
  protected final String TEXT_349 = NL + "\t\t\t\t\tif(untypedList.isEmpty()) {" + NL + "\t\t\t\t\t\treturn null;" + NL + "\t\t\t\t\t}" + NL + "\t\t\t\t\treturn (";
  protected final String TEXT_350 = ")untypedList.get(0);\t\t\t\t\t\t\t\t\t\t\t\t\t" + NL + "\t\t\t\t";
  protected final String TEXT_351 = NL + "\t\t\tERROR: An empty TargetFeaturePath was provided to the generator for this Feature!" + NL + "\t\t";
  protected final String TEXT_352 = "\t" + NL + "\t";
  protected final String TEXT_353 = NL + "\t\t// TODO: implement this method to return the '";
  protected final String TEXT_354 = "' ";
  protected final String TEXT_355 = NL + "\t\t// Ensure that you remove @generated or mark it @generated NOT" + NL + "\t";
  protected final String TEXT_356 = NL + "\t\t// The list is expected to implement org.eclipse.emf.ecore.util.InternalEList and org.eclipse.emf.ecore.EStructuralFeature.Setting" + NL + "\t\t// so it's likely that an appropriate subclass of org.eclipse.emf.ecore.util.";
  protected final String TEXT_357 = "EcoreEMap";
  protected final String TEXT_358 = "BasicFeatureMap";
  protected final String TEXT_359 = "EcoreEList";
  protected final String TEXT_360 = " should be used." + NL + "\t";
  protected final String TEXT_361 = NL + "\t\tthrow new UnsupportedOperationException();";
  protected final String TEXT_362 = "\t";
  protected final String TEXT_363 = " basicGet";
  protected final String TEXT_364 = ", false, ";
  protected final String TEXT_365 = "__ESETTING_DELEGATE.dynamicGet(this, null, 0, false, false)";
  protected final String TEXT_366 = ")eInternalContainer();";
  protected final String TEXT_367 = ")((";
  protected final String TEXT_368 = ", false);";
  protected final String TEXT_369 = NL + "\t\t// -> do not perform proxy resolution" + NL + "\t\t// Ensure that you remove @generated or mark it @generated NOT" + NL + "\t\tthrow new UnsupportedOperationException();";
  protected final String TEXT_370 = " basicSet";
  protected final String TEXT_371 = " msgs)" + NL + "\t{";
  protected final String TEXT_372 = NL + "\t\tmsgs = eBasicSetContainer((";
  protected final String TEXT_373 = ")new";
  protected final String TEXT_374 = ", msgs);";
  protected final String TEXT_375 = NL + "\t\treturn msgs;";
  protected final String TEXT_376 = NL + "\t\tmsgs = eDynamicInverseAdd((";
  protected final String TEXT_377 = NL + "\t\tObject old";
  protected final String TEXT_378 = " = eVirtualSet(";
  protected final String TEXT_379 = ", new";
  protected final String TEXT_380 = ";" + NL + "\t\t";
  protected final String TEXT_381 = " = new";
  protected final String TEXT_382 = NL + "\t\tboolean isSetChange = old";
  protected final String TEXT_383 = " == EVIRTUAL_NO_VALUE;";
  protected final String TEXT_384 = NL + "\t\tboolean old";
  protected final String TEXT_385 = "ESet = (";
  protected final String TEXT_386 = "_ESETFLAG) != 0;";
  protected final String TEXT_387 = "_ESETFLAG;";
  protected final String TEXT_388 = "ESet = ";
  protected final String TEXT_389 = "ESet;";
  protected final String TEXT_390 = "ESet = true;";
  protected final String TEXT_391 = NL + "\t\tif (eNotificationRequired())" + NL + "\t\t{";
  protected final String TEXT_392 = " notification = new ";
  protected final String TEXT_393 = ".SET, ";
  protected final String TEXT_394 = "isSetChange ? null : old";
  protected final String TEXT_395 = "old";
  protected final String TEXT_396 = "isSetChange";
  protected final String TEXT_397 = "!old";
  protected final String TEXT_398 = "ESet";
  protected final String TEXT_399 = " == EVIRTUAL_NO_VALUE ? null : old";
  protected final String TEXT_400 = NL + "\t\t\tif (msgs == null) msgs = notification; else msgs.add(notification);" + NL + "\t\t}";
  protected final String TEXT_401 = "()).featureMap()).basicAdd(";
  protected final String TEXT_402 = ".Internal)get";
  protected final String TEXT_403 = "()).basicAdd(";
  protected final String TEXT_404 = NL + "\t\t// TODO: implement this method to set the contained '";
  protected final String TEXT_405 = NL + "\t\t// -> this method is automatically invoked to keep the containment relationship in synch" + NL + "\t\t// -> do not modify other features" + NL + "\t\t// -> return msgs, after adding any generated Notification to it (if it is null, a NotificationChain object must be created first)" + NL + "\t\t// Ensure that you remove @generated or mark it @generated NOT" + NL + "\t\tthrow new UnsupportedOperationException();";
  protected final String TEXT_406 = NL + "\t/**" + NL + "\t * Sets the value of the '{@link ";
  protected final String TEXT_407 = NL + "\t * <!-- begin-user-doc -->" + NL + "\t * <!-- end-user-doc -->" + NL + "\t * @param value the new value of the '<em>";
  protected final String TEXT_408 = "()" + NL + "\t * @generated" + NL + "\t */";
  protected final String TEXT_409 = " value);" + NL;
  protected final String TEXT_410 = ")" + NL + "\t{";
  protected final String TEXT_411 = NL + "\t\teDynamicSet(";
  protected final String TEXT_412 = "new ";
  protected final String TEXT_413 = "new";
  protected final String TEXT_414 = NL + "\t\teSet(";
  protected final String TEXT_415 = "__ESETTING_DELEGATE.dynamicSet(this, null, 0, ";
  protected final String TEXT_416 = NL + "\t\tif (new";
  protected final String TEXT_417 = " != eInternalContainer() || (eContainerFeatureID() != ";
  protected final String TEXT_418 = " && new";
  protected final String TEXT_419 = " != null))" + NL + "\t\t{" + NL + "\t\t\tif (";
  protected final String TEXT_420 = ".isAncestor(this, ";
  protected final String TEXT_421 = "))" + NL + "\t\t\t\tthrow new ";
  protected final String TEXT_422 = "(\"Recursive containment not allowed for \" + toString());";
  protected final String TEXT_423 = " msgs = null;" + NL + "\t\t\tif (eInternalContainer() != null)" + NL + "\t\t\t\tmsgs = eBasicRemoveFromContainer(msgs);" + NL + "\t\t\tif (new";
  protected final String TEXT_424 = " != null)" + NL + "\t\t\t\tmsgs = ((";
  protected final String TEXT_425 = ").eInverseAdd(this, ";
  protected final String TEXT_426 = ".class, msgs);" + NL + "\t\t\tmsgs = basicSet";
  protected final String TEXT_427 = ", msgs);" + NL + "\t\t\tif (msgs != null) msgs.dispatch();" + NL + "\t\t}";
  protected final String TEXT_428 = NL + "\t\telse if (eNotificationRequired())" + NL + "\t\t\teNotify(new ";
  protected final String TEXT_429 = " != ";
  protected final String TEXT_430 = ")" + NL + "\t\t{" + NL + "\t\t\t";
  protected final String TEXT_431 = " msgs = null;" + NL + "\t\t\tif (";
  protected final String TEXT_432 = " != null)";
  protected final String TEXT_433 = NL + "\t\t\t\tmsgs = ((";
  protected final String TEXT_434 = ").eInverseRemove(this, EOPPOSITE_FEATURE_BASE - ";
  protected final String TEXT_435 = ", null, msgs);" + NL + "\t\t\tif (new";
  protected final String TEXT_436 = ").eInverseAdd(this, EOPPOSITE_FEATURE_BASE - ";
  protected final String TEXT_437 = ").eInverseRemove(this, ";
  protected final String TEXT_438 = ".class, msgs);" + NL + "\t\t\tif (new";
  protected final String TEXT_439 = NL + "\t\t\tmsgs = basicSet";
  protected final String TEXT_440 = NL + "\t\telse" + NL + "\t\t{";
  protected final String TEXT_441 = NL + "\t\t\tboolean old";
  protected final String TEXT_442 = "ESet = eVirtualIsSet(";
  protected final String TEXT_443 = NL + "\t\t\tif (eNotificationRequired())" + NL + "\t\t\t\teNotify(new ";
  protected final String TEXT_444 = ", !old";
  protected final String TEXT_445 = "ESet));";
  protected final String TEXT_446 = NL + "\t\t}";
  protected final String TEXT_447 = ") ";
  protected final String TEXT_448 = "_EFLAG; else ";
  protected final String TEXT_449 = " &= ~";
  protected final String TEXT_450 = "_EFLAG;";
  protected final String TEXT_451 = " == null) new";
  protected final String TEXT_452 = "_EDEFAULT;" + NL + "\t\t";
  protected final String TEXT_453 = " & ~";
  protected final String TEXT_454 = "_EFLAG | ";
  protected final String TEXT_455 = ".VALUES.indexOf(new";
  protected final String TEXT_456 = "_EFLAG_OFFSET;";
  protected final String TEXT_457 = " == null ? ";
  protected final String TEXT_458 = " : new";
  protected final String TEXT_459 = NL + "\t\tif (eNotificationRequired())" + NL + "\t\t\teNotify(new ";
  protected final String TEXT_460 = "isSetChange ? ";
  protected final String TEXT_461 = " : old";
  protected final String TEXT_462 = " == EVIRTUAL_NO_VALUE ? ";
  protected final String TEXT_463 = NL + "\t\t((";
  protected final String TEXT_464 = "()).featureMap()).set(";
  protected final String TEXT_465 = "()).set(";
  protected final String TEXT_466 = "\t\t\t" + NL + "\t\t";
  protected final String TEXT_467 = "\t\t\t\t\t" + NL + "\t\t\t";
  protected final String TEXT_468 = "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t" + NL + "\t\t\t\t\t";
  protected final String TEXT_469 = "set";
  protected final String TEXT_470 = ".longValue());";
  protected final String TEXT_471 = "(Integer.toString(";
  protected final String TEXT_472 = ".toString());";
  protected final String TEXT_473 = ".toPlainString());";
  protected final String TEXT_474 = ".intValueExact());\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
  protected final String TEXT_475 = "\t\t\t\t\t\t" + NL + "\t";
  protected final String TEXT_476 = NL + "\t\t\t\t\tset";
  protected final String TEXT_477 = ".getName()));" + NL + "\t\t\t\t";
  protected final String TEXT_478 = "();" + NL + "\t\t\t\t\tlist.clear();" + NL + "\t\t\t\t\tlist.add((";
  protected final String TEXT_479 = ");" + NL + "\t\t\t\t";
  protected final String TEXT_480 = "\t\t\t\t\t\t\t\t\t\t" + NL + "\t\t\t\t\tset";
  protected final String TEXT_481 = ");\t\t\t\t\t\t\t\t\t\t\t\t\t" + NL + "\t\t\t\t";
  protected final String TEXT_482 = "();" + NL + "\t\t";
  protected final String TEXT_483 = ");" + NL + "\t}" + NL + "\t";
  protected final String TEXT_484 = "\t\t\t\t\t\t\t\t\t\t" + NL + "\t\t\t\t\t";
  protected final String TEXT_485 = "\t\t\t\t\t" + NL + "\t\t\t\t\t";
  protected final String TEXT_486 = ".set";
  protected final String TEXT_487 = ".getName()));\t\t\t\t\t\t\t\t\t\t" + NL + "\t\t\t\t";
  protected final String TEXT_488 = "ERROR: This case is not supported by the GEASTADL generator!" + NL + "\t\t\t\t\t[EObject => List]\t" + NL + "\t\t\t\t";
  protected final String TEXT_489 = NL + "\t\t\t\t\t\t";
  protected final String TEXT_490 = "();" + NL + "\t\t\t\t\t\tif(";
  protected final String TEXT_491 = " == null) {" + NL + "\t\t\t\t\t\t\t";
  protected final String TEXT_492 = "();" + NL + "\t\t\t\t\t\t}" + NL + "\t\t\t\t\t\t";
  protected final String TEXT_493 = ");\t\t\t\t\t\t" + NL + "\t\t\t\t\t\t";
  protected final String TEXT_494 = "\t\t\t\t\t\t" + NL + "\t\t";
  protected final String TEXT_495 = NL + "\t\t// Ensure that you remove @generated or mark it @generated NOT" + NL + "\t\tthrow new UnsupportedOperationException();";
  protected final String TEXT_496 = " basicUnset";
  protected final String TEXT_497 = NL + "\t\treturn eDynamicInverseRemove((";
  protected final String TEXT_498 = "basicGet";
  protected final String TEXT_499 = "(), ";
  protected final String TEXT_500 = "Object old";
  protected final String TEXT_501 = "eVirtualUnset(";
  protected final String TEXT_502 = " = null;";
  protected final String TEXT_503 = " != EVIRTUAL_NO_VALUE;";
  protected final String TEXT_504 = "ESet = false;";
  protected final String TEXT_505 = NL + "\t\tif (eNotificationRequired())" + NL + "\t\t{" + NL + "\t\t\t";
  protected final String TEXT_506 = ".UNSET, ";
  protected final String TEXT_507 = "isSetChange ? old";
  protected final String TEXT_508 = " : null";
  protected final String TEXT_509 = ", null, ";
  protected final String TEXT_510 = ");" + NL + "\t\t\tif (msgs == null) msgs = notification; else msgs.add(notification);" + NL + "\t\t}" + NL + "\t\treturn msgs;";
  protected final String TEXT_511 = NL + "\t\t// TODO: implement this method to unset the contained '";
  protected final String TEXT_512 = NL + "\t/**" + NL + "\t * Unsets the value of the '{@link ";
  protected final String TEXT_513 = NL + "\t * <!-- begin-user-doc -->" + NL + "\t * <!-- end-user-doc -->";
  protected final String TEXT_514 = NL + "\t\teDynamicUnset(";
  protected final String TEXT_515 = NL + "\t\teUnset(";
  protected final String TEXT_516 = "__ESETTING_DELEGATE.dynamicUnset(this, null, 0);";
  protected final String TEXT_517 = " != null) ((";
  protected final String TEXT_518 = ".Unsettable";
  protected final String TEXT_519 = ").unset();";
  protected final String TEXT_520 = " != null)" + NL + "\t\t{" + NL + "\t\t\t";
  protected final String TEXT_521 = " msgs = null;";
  protected final String TEXT_522 = NL + "\t\t\tmsgs = ((";
  protected final String TEXT_523 = NL + "\t\t\tmsgs = basicUnset";
  protected final String TEXT_524 = "(msgs);" + NL + "\t\t\tif (msgs != null) msgs.dispatch();" + NL + "\t\t}" + NL + "\t\telse" + NL + "\t\t{";
  protected final String TEXT_525 = ", null, null, old";
  protected final String TEXT_526 = " = eVirtualUnset(";
  protected final String TEXT_527 = "_EFLAG_DEFAULT;";
  protected final String TEXT_528 = " : ";
  protected final String TEXT_529 = "()).featureMap()).clear(";
  protected final String TEXT_530 = "()).clear(";
  protected final String TEXT_531 = "// Class/GEastadl/gUnsetImplementation.insert.javajetinc" + NL;
  protected final String TEXT_532 = "ERROR: The mapping to the concrete features could not be resolved.";
  protected final String TEXT_533 = "();" + NL + "\t";
  protected final String TEXT_534 = NL + "\t\tERROR: An empty TargetFeaturePath was provided to the generator for this Feature!\t" + NL + "\t";
  protected final String TEXT_535 = NL + "\t/**" + NL + "\t * Returns whether the value of the '{@link ";
  protected final String TEXT_536 = " is set.";
  protected final String TEXT_537 = NL + "\t * <!-- begin-user-doc -->" + NL + "\t * <!-- end-user-doc -->" + NL + "\t * @return whether the value of the '<em>";
  protected final String TEXT_538 = NL + "\tboolean ";
  protected final String TEXT_539 = NL + "\tpublic boolean ";
  protected final String TEXT_540 = NL + "\t\treturn eDynamicIsSet(";
  protected final String TEXT_541 = NL + "\t\treturn eIsSet(";
  protected final String TEXT_542 = "__ESETTING_DELEGATE.dynamicIsSet(this, null, 0);";
  protected final String TEXT_543 = " != null && ((";
  protected final String TEXT_544 = ").isSet();";
  protected final String TEXT_545 = NL + "\t\treturn eVirtualIsSet(";
  protected final String TEXT_546 = NL + "\t\treturn !((";
  protected final String TEXT_547 = "()).featureMap()).isEmpty(";
  protected final String TEXT_548 = "()).isEmpty(";
  protected final String TEXT_549 = "// Class/GEastadl/gIsSetImplementation.insert.javajetinc" + NL + NL;
  protected final String TEXT_550 = "();\t\t" + NL + "\t";
  protected final String TEXT_551 = NL + "\t/**" + NL + "\t * The cached validation expression for the '{@link #";
  protected final String TEXT_552 = ") <em>";
  protected final String TEXT_553 = "</em>}' invariant operation." + NL + "\t * <!-- begin-user-doc -->" + NL + "\t * <!-- end-user-doc -->" + NL + "\t * @see #";
  protected final String TEXT_554 = ")" + NL + "\t * @generated" + NL + "\t * @ordered" + NL + "\t */" + NL + "\tprotected static final ";
  protected final String TEXT_555 = "__EEXPRESSION = \"";
  protected final String TEXT_556 = NL + "\t/**" + NL + "\t * The cached invocation delegate for the '{@link #";
  protected final String TEXT_557 = "</em>}' operation." + NL + "\t * <!-- begin-user-doc -->" + NL + "\t * <!-- end-user-doc -->" + NL + "\t * @see #";
  protected final String TEXT_558 = ".Internal.InvocationDelegate ";
  protected final String TEXT_559 = "__EINVOCATION_DELEGATE = ((";
  protected final String TEXT_560 = ").getInvocationDelegate();" + NL;
  protected final String TEXT_561 = NL + "\t/**";
  protected final String TEXT_562 = NL + "\t * <!-- begin-model-doc -->";
  protected final String TEXT_563 = NL + "\t * ";
  protected final String TEXT_564 = NL + "\t * @param ";
  protected final String TEXT_565 = NL + "\t *   ";
  protected final String TEXT_566 = NL + "\t{";
  protected final String TEXT_567 = NL + "\t\treturn" + NL + "\t\t\t";
  protected final String TEXT_568 = ".validate" + NL + "\t\t\t\t(";
  protected final String TEXT_569 = "," + NL + "\t\t\t\t this," + NL + "\t\t\t\t ";
  protected final String TEXT_570 = "," + NL + "\t\t\t\t ";
  protected final String TEXT_571 = "," + NL + "\t\t\t\t \"";
  protected final String TEXT_572 = "\",";
  protected final String TEXT_573 = NL + "\t\t\t\t ";
  protected final String TEXT_574 = "__EEXPRESSION," + NL + "\t\t\t\t ";
  protected final String TEXT_575 = ".ERROR," + NL + "\t\t\t\t ";
  protected final String TEXT_576 = ".DIAGNOSTIC_SOURCE," + NL + "\t\t\t\t ";
  protected final String TEXT_577 = NL + "\t\t// TODO: implement this method" + NL + "\t\t// -> specify the condition that violates the invariant" + NL + "\t\t// -> verify the details of the diagnostic, including severity and message" + NL + "\t\t// Ensure that you remove @generated or mark it @generated NOT" + NL + "\t\tif (false)" + NL + "\t\t{" + NL + "\t\t\tif (";
  protected final String TEXT_578 = " != null)" + NL + "\t\t\t{" + NL + "\t\t\t\t";
  protected final String TEXT_579 = ".add" + NL + "\t\t\t\t\t(new ";
  protected final String TEXT_580 = NL + "\t\t\t\t\t\t(";
  protected final String TEXT_581 = ".ERROR," + NL + "\t\t\t\t\t\t ";
  protected final String TEXT_582 = ".DIAGNOSTIC_SOURCE," + NL + "\t\t\t\t\t\t ";
  protected final String TEXT_583 = "," + NL + "\t\t\t\t\t\t ";
  protected final String TEXT_584 = ".INSTANCE.getString(\"_UI_GenericInvariant_diagnostic\", new Object[] { \"";
  protected final String TEXT_585 = "\", ";
  protected final String TEXT_586 = ".getObjectLabel(this, ";
  protected final String TEXT_587 = ") }),";
  protected final String TEXT_588 = NL + "\t\t\t\t\t\t new Object [] { this }));" + NL + "\t\t\t}" + NL + "\t\t\treturn false;" + NL + "\t\t}" + NL + "\t\treturn true;";
  protected final String TEXT_589 = NL + "\t\ttry" + NL + "\t\t{";
  protected final String TEXT_590 = "__EINVOCATION_DELEGATE.dynamicInvoke(this, ";
  protected final String TEXT_591 = ".UnmodifiableEList<Object>(";
  protected final String TEXT_592 = "null";
  protected final String TEXT_593 = NL + "\t\t\treturn ";
  protected final String TEXT_594 = NL + "\t\t}" + NL + "\t\tcatch (";
  protected final String TEXT_595 = " ite)" + NL + "\t\t{" + NL + "\t\t\tthrow new ";
  protected final String TEXT_596 = "(ite);" + NL + "\t\t}";
  protected final String TEXT_597 = NL + " \t\t";
  protected final String TEXT_598 = "();" + NL + "\t\treturn untypedList;";
  protected final String TEXT_599 = "();";
  protected final String TEXT_600 = NL + "\t\t\tset";
  protected final String TEXT_601 = ");\t\t";
  protected final String TEXT_602 = "\t" + NL + " \t\t";
  protected final String TEXT_603 = NL + " \t\t// TODO: implement this method" + NL + "\t\t// Ensure that you remove @generated or mark it @generated NOT" + NL + "\t\tthrow new UnsupportedOperationException();\t\t\t\t";
  protected final String TEXT_604 = NL + NL + "\t";
  protected final String TEXT_605 = NL + "\t\t// TODO: implement this method" + NL + "\t\t// Ensure that you remove @generated or mark it @generated NOT" + NL + "\t\tthrow new UnsupportedOperationException();";
  protected final String TEXT_606 = " eInverseAdd(";
  protected final String TEXT_607 = " otherEnd, int featureID, ";
  protected final String TEXT_608 = " msgs)" + NL + "\t{" + NL + "\t\tswitch (featureID";
  protected final String TEXT_609 = ")" + NL + "\t\t{";
  protected final String TEXT_610 = NL + "\t\t\tcase ";
  protected final String TEXT_611 = ":";
  protected final String TEXT_612 = NL + "\t\t\t\treturn ((";
  protected final String TEXT_613 = ".InternalMapView";
  protected final String TEXT_614 = "()).eMap()).basicAdd(otherEnd, msgs);";
  protected final String TEXT_615 = NL + "\t\t\t\treturn (";
  protected final String TEXT_616 = "()).basicAdd(otherEnd, msgs);";
  protected final String TEXT_617 = NL + "\t\t\t\tif (eInternalContainer() != null)" + NL + "\t\t\t\t\tmsgs = eBasicRemoveFromContainer(msgs);";
  protected final String TEXT_618 = NL + "\t\t\t\treturn basicSet";
  protected final String TEXT_619 = ")otherEnd, msgs);";
  protected final String TEXT_620 = NL + "\t\t\t\treturn eBasicSetContainer(otherEnd, ";
  protected final String TEXT_621 = NL + "\t\t\t\tif (";
  protected final String TEXT_622 = NL + "\t\t\t\t\tmsgs = ((";
  protected final String TEXT_623 = NL + "\t\treturn super.eInverseAdd(otherEnd, featureID, msgs);";
  protected final String TEXT_624 = NL + "\t\treturn eDynamicInverseAdd(otherEnd, featureID, msgs);";
  protected final String TEXT_625 = " eInverseRemove(";
  protected final String TEXT_626 = "()).eMap()).basicRemove(otherEnd, msgs);";
  protected final String TEXT_627 = ".Internal.Wrapper)";
  protected final String TEXT_628 = "()).featureMap()).basicRemove(otherEnd, msgs);";
  protected final String TEXT_629 = "()).basicRemove(otherEnd, msgs);";
  protected final String TEXT_630 = NL + "\t\t\t\treturn eBasicSetContainer(null, ";
  protected final String TEXT_631 = NL + "\t\t\t\treturn basicUnset";
  protected final String TEXT_632 = "(msgs);";
  protected final String TEXT_633 = "(null, msgs);";
  protected final String TEXT_634 = NL + "\t\treturn super.eInverseRemove(otherEnd, featureID, msgs);";
  protected final String TEXT_635 = NL + "\t\treturn eDynamicInverseRemove(otherEnd, featureID, msgs);";
  protected final String TEXT_636 = " eBasicRemoveFromContainerFeature(";
  protected final String TEXT_637 = " msgs)" + NL + "\t{" + NL + "\t\tswitch (eContainerFeatureID()";
  protected final String TEXT_638 = ":" + NL + "\t\t\t\treturn eInternalContainer().eInverseRemove(this, ";
  protected final String TEXT_639 = NL + "\t\treturn super.eBasicRemoveFromContainerFeature(msgs);";
  protected final String TEXT_640 = NL + "\t\treturn eDynamicBasicRemoveFromContainer(msgs);";
  protected final String TEXT_641 = NL + "\tpublic Object eGet(int featureID, boolean resolve, boolean coreType)" + NL + "\t{" + NL + "\t\tswitch (featureID";
  protected final String TEXT_642 = NL + "\t\t\t\treturn ";
  protected final String TEXT_643 = "() ? Boolean.TRUE : Boolean.FALSE;";
  protected final String TEXT_644 = NL + "\t\t\t\treturn new ";
  protected final String TEXT_645 = NL + "\t\t\t\tif (resolve) return ";
  protected final String TEXT_646 = "();" + NL + "\t\t\t\treturn basicGet";
  protected final String TEXT_647 = NL + "\t\t\t\tif (coreType) return ((";
  protected final String TEXT_648 = "()).eMap();" + NL + "\t\t\t\telse return ";
  protected final String TEXT_649 = NL + "\t\t\t\tif (coreType) return ";
  protected final String TEXT_650 = "();" + NL + "\t\t\t\telse return ";
  protected final String TEXT_651 = "().map();";
  protected final String TEXT_652 = "()).featureMap();" + NL + "\t\t\t\treturn ";
  protected final String TEXT_653 = "();" + NL + "\t\t\t\treturn ((";
  protected final String TEXT_654 = "()).getWrapper();";
  protected final String TEXT_655 = NL + "\t\treturn super.eGet(featureID, resolve, coreType);";
  protected final String TEXT_656 = NL + "\t\treturn eDynamicGet(featureID, resolve, coreType);";
  protected final String TEXT_657 = NL + "\tpublic void eSet(int featureID, Object newValue)" + NL + "\t{" + NL + "\t\tswitch (featureID";
  protected final String TEXT_658 = NL + "\t\t\t\t((";
  protected final String TEXT_659 = "()).featureMap()).set(newValue);";
  protected final String TEXT_660 = "()).set(newValue);";
  protected final String TEXT_661 = ".Setting)((";
  protected final String TEXT_662 = "()).eMap()).set(newValue);";
  protected final String TEXT_663 = ".Setting)";
  protected final String TEXT_664 = "().clear();" + NL + "\t\t\t\t";
  protected final String TEXT_665 = "().addAll((";
  protected final String TEXT_666 = "<? extends ";
  protected final String TEXT_667 = ">";
  protected final String TEXT_668 = ")newValue);";
  protected final String TEXT_669 = NL + "            ";
  protected final String TEXT_670 = "(((";
  protected final String TEXT_671 = ")newValue).";
  protected final String TEXT_672 = NL + "   \t\t\t";
  protected final String TEXT_673 = "newValue);";
  protected final String TEXT_674 = NL + "\t\t\t\treturn;";
  protected final String TEXT_675 = NL + "\t\tsuper.eSet(featureID, newValue);";
  protected final String TEXT_676 = NL + "\t\teDynamicSet(featureID, newValue);";
  protected final String TEXT_677 = NL + "\tpublic void eUnset(int featureID)" + NL + "\t{" + NL + "\t\tswitch (featureID";
  protected final String TEXT_678 = "()).featureMap().clear();";
  protected final String TEXT_679 = "().clear();";
  protected final String TEXT_680 = NL + "\t\t    \t";
  protected final String TEXT_681 = ")null);";
  protected final String TEXT_682 = NL + "\t\t\t   ";
  protected final String TEXT_683 = NL + "\t\tsuper.eUnset(featureID);";
  protected final String TEXT_684 = NL + "\t\teDynamicUnset(featureID);";
  protected final String TEXT_685 = NL + "\tpublic boolean eIsSet(int featureID)" + NL + "\t{" + NL + "\t\tswitch (featureID";
  protected final String TEXT_686 = NL + "\t\t\t\treturn isSet";
  protected final String TEXT_687 = NL + "\t\t\t\treturn !((";
  protected final String TEXT_688 = "()).featureMap().isEmpty();";
  protected final String TEXT_689 = " != null && !";
  protected final String TEXT_690 = ".featureMap().isEmpty();";
  protected final String TEXT_691 = ".isEmpty();";
  protected final String TEXT_692 = ");" + NL + "\t\t\t\treturn ";
  protected final String TEXT_693 = NL + "\t\t\t\treturn !";
  protected final String TEXT_694 = "().isEmpty();";
  protected final String TEXT_695 = " != null;";
  protected final String TEXT_696 = NL + "\t\t\t\treturn eVirtualGet(";
  protected final String TEXT_697 = ") != null;";
  protected final String TEXT_698 = NL + "\t\t\t\treturn basicGet";
  protected final String TEXT_699 = "() != null;";
  protected final String TEXT_700 = "_EFLAG) != 0) != ";
  protected final String TEXT_701 = "_EFLAG) != ";
  protected final String TEXT_702 = ") != ";
  protected final String TEXT_703 = "() != ";
  protected final String TEXT_704 = " != null : !";
  protected final String TEXT_705 = ".equals(";
  protected final String TEXT_706 = "() != null : !";
  protected final String TEXT_707 = NL + "\t\treturn super.eIsSet(featureID);";
  protected final String TEXT_708 = NL + "\t\treturn eDynamicIsSet(featureID);";
  protected final String TEXT_709 = NL + "\tpublic int eBaseStructuralFeatureID(int derivedFeatureID, Class";
  protected final String TEXT_710 = " baseClass)" + NL + "\t{";
  protected final String TEXT_711 = NL + "\t\tif (baseClass == ";
  protected final String TEXT_712 = ".class)" + NL + "\t\t{" + NL + "\t\t\tswitch (derivedFeatureID";
  protected final String TEXT_713 = NL + "\t\t\t\tcase ";
  protected final String TEXT_714 = ": return ";
  protected final String TEXT_715 = NL + "\t\t\t\tdefault: return -1;" + NL + "\t\t\t}" + NL + "\t\t}";
  protected final String TEXT_716 = NL + "\t\treturn super.eBaseStructuralFeatureID(derivedFeatureID, baseClass);" + NL + "\t}";
  protected final String TEXT_717 = NL + NL + "\t/**" + NL + "\t * <!-- begin-user-doc -->" + NL + "\t * <!-- end-user-doc -->" + NL + "\t * @generated" + NL + "\t */";
  protected final String TEXT_718 = NL + "\tpublic int eDerivedStructuralFeatureID(int baseFeatureID, Class";
  protected final String TEXT_719 = ".class)" + NL + "\t\t{" + NL + "\t\t\tswitch (baseFeatureID)" + NL + "\t\t\t{";
  protected final String TEXT_720 = ".class)" + NL + "\t\t{" + NL + "\t\t\tswitch (baseFeatureID";
  protected final String TEXT_721 = NL + "\t\treturn super.eDerivedStructuralFeatureID(baseFeatureID, baseClass);" + NL + "\t}" + NL;
  protected final String TEXT_722 = NL + "\tpublic int eDerivedOperationID(int baseOperationID, Class";
  protected final String TEXT_723 = ".class)" + NL + "\t\t{" + NL + "\t\t\tswitch (baseOperationID)" + NL + "\t\t\t{";
  protected final String TEXT_724 = NL + "\t\t\t\tdefault: return super.eDerivedOperationID(baseOperationID, baseClass);" + NL + "\t\t\t}" + NL + "\t\t}";
  protected final String TEXT_725 = ".class)" + NL + "\t\t{" + NL + "\t\t\tswitch (baseOperationID";
  protected final String TEXT_726 = NL + "\t\treturn super.eDerivedOperationID(baseOperationID, baseClass);" + NL + "\t}" + NL;
  protected final String TEXT_727 = NL + "\tprotected Object[] eVirtualValues()" + NL + "\t{" + NL + "\t\treturn ";
  protected final String TEXT_728 = ";" + NL + "\t}" + NL + "" + NL + "\t/**" + NL + "\t * <!-- begin-user-doc -->" + NL + "\t * <!-- end-user-doc -->" + NL + "\t * @generated" + NL + "\t */";
  protected final String TEXT_729 = NL + "\tprotected void eSetVirtualValues(Object[] newValues)" + NL + "\t{" + NL + "\t\t";
  protected final String TEXT_730 = " = newValues;" + NL + "\t}" + NL;
  protected final String TEXT_731 = NL + "\tprotected int eVirtualIndexBits(int offset)" + NL + "\t{" + NL + "\t\tswitch (offset)" + NL + "\t\t{";
  protected final String TEXT_732 = " :" + NL + "\t\t\t\treturn ";
  protected final String TEXT_733 = NL + "\t\t\tdefault :" + NL + "\t\t\t\tthrow new IndexOutOfBoundsException();" + NL + "\t\t}" + NL + "\t}" + NL + "" + NL + "\t/**" + NL + "\t * <!-- begin-user-doc -->" + NL + "\t * <!-- end-user-doc -->" + NL + "\t * @generated" + NL + "\t */";
  protected final String TEXT_734 = NL + "\tprotected void eSetVirtualIndexBits(int offset, int newIndexBits)" + NL + "\t{" + NL + "\t\tswitch (offset)" + NL + "\t\t{";
  protected final String TEXT_735 = " :" + NL + "\t\t\t\t";
  protected final String TEXT_736 = " = newIndexBits;" + NL + "\t\t\t\tbreak;";
  protected final String TEXT_737 = NL + "\t\t\tdefault :" + NL + "\t\t\t\tthrow new IndexOutOfBoundsException();" + NL + "\t\t}" + NL + "\t}" + NL;
  protected final String TEXT_738 = NL + "\tpublic Object eInvoke(int operationID, ";
  protected final String TEXT_739 = " arguments) throws ";
  protected final String TEXT_740 = NL + "\t{" + NL + "\t\tswitch (operationID";
  protected final String TEXT_741 = "arguments.get(";
  protected final String TEXT_742 = ");" + NL + "\t\t\t\treturn null;";
  protected final String TEXT_743 = NL + "\t\treturn super.eInvoke(operationID, arguments);";
  protected final String TEXT_744 = NL + "\t\treturn eDynamicInvoke(operationID, arguments);";
  protected final String TEXT_745 = NL + "\tpublic String toString()" + NL + "\t{" + NL + "\t\tif (eIsProxy()) return super.toString();" + NL + "" + NL + "\t\tStringBuffer result = new StringBuffer(super.toString());";
  protected final String TEXT_746 = NL + "\t\tresult.append(\" (";
  protected final String TEXT_747 = ": \");";
  protected final String TEXT_748 = NL + "\t\tresult.append(\", ";
  protected final String TEXT_749 = NL + "\t\tif (eVirtualIsSet(";
  protected final String TEXT_750 = ")) result.append(eVirtualGet(";
  protected final String TEXT_751 = ")); else result.append(\"<unset>\");";
  protected final String TEXT_752 = "_ESETFLAG) != 0";
  protected final String TEXT_753 = ") result.append((";
  protected final String TEXT_754 = "_EFLAG) != 0); else result.append(\"<unset>\");";
  protected final String TEXT_755 = ") result.append(";
  protected final String TEXT_756 = "_EFLAG_OFFSET]); else result.append(\"<unset>\");";
  protected final String TEXT_757 = "); else result.append(\"<unset>\");";
  protected final String TEXT_758 = NL + "\t\tresult.append(eVirtualGet(";
  protected final String TEXT_759 = NL + "\t\tresult.append((";
  protected final String TEXT_760 = "_EFLAG) != 0);";
  protected final String TEXT_761 = NL + "\t\tresult.append(";
  protected final String TEXT_762 = "_EFLAG_OFFSET]);";
  protected final String TEXT_763 = NL + "\t\tresult.append(')');" + NL + "\t\treturn result.toString();" + NL + "\t}" + NL;
  protected final String TEXT_764 = NL + "\tprotected int hash = -1;" + NL + "" + NL + "\t/**" + NL + "\t * <!-- begin-user-doc -->" + NL + "\t * <!-- end-user-doc -->" + NL + "\t * @generated" + NL + "\t */" + NL + "\tpublic int getHash()" + NL + "\t{" + NL + "\t\tif (hash == -1)" + NL + "\t\t{" + NL + "\t\t\t";
  protected final String TEXT_765 = " theKey = getKey();" + NL + "\t\t\thash = (theKey == null ? 0 : theKey.hashCode());" + NL + "\t\t}" + NL + "\t\treturn hash;" + NL + "\t}" + NL + "" + NL + "\t/**" + NL + "\t * <!-- begin-user-doc -->" + NL + "\t * <!-- end-user-doc -->" + NL + "\t * @generated" + NL + "\t */" + NL + "\tpublic void setHash(int hash)" + NL + "\t{" + NL + "\t\tthis.hash = hash;" + NL + "\t}" + NL + "" + NL + "\t/**" + NL + "\t * <!-- begin-user-doc -->" + NL + "\t * <!-- end-user-doc -->" + NL + "\t * @generated" + NL + "\t */" + NL + "\tpublic ";
  protected final String TEXT_766 = " getKey()" + NL + "\t{";
  protected final String TEXT_767 = "(getTypedKey());";
  protected final String TEXT_768 = NL + "\t\treturn getTypedKey();";
  protected final String TEXT_769 = NL + "\t}" + NL + "" + NL + "\t/**" + NL + "\t * <!-- begin-user-doc -->" + NL + "\t * <!-- end-user-doc -->" + NL + "\t * @generated" + NL + "\t */" + NL + "\tpublic void setKey(";
  protected final String TEXT_770 = " key)" + NL + "\t{";
  protected final String TEXT_771 = NL + "\t\tgetTypedKey().addAll(";
  protected final String TEXT_772 = "key);";
  protected final String TEXT_773 = NL + "\t\tsetTypedKey(key);";
  protected final String TEXT_774 = NL + "\t\tsetTypedKey(((";
  protected final String TEXT_775 = ")key).";
  protected final String TEXT_776 = NL + "\t\tsetTypedKey((";
  protected final String TEXT_777 = ")key);";
  protected final String TEXT_778 = NL + "\t}" + NL + "" + NL + "\t/**" + NL + "\t * <!-- begin-user-doc -->" + NL + "\t * <!-- end-user-doc -->" + NL + "\t * @generated" + NL + "\t */" + NL + "\tpublic ";
  protected final String TEXT_779 = " getValue()" + NL + "\t{";
  protected final String TEXT_780 = "(getTypedValue());";
  protected final String TEXT_781 = NL + "\t\treturn getTypedValue();";
  protected final String TEXT_782 = " setValue(";
  protected final String TEXT_783 = " value)" + NL + "\t{" + NL + "\t\t";
  protected final String TEXT_784 = " oldValue = getValue();";
  protected final String TEXT_785 = NL + "\t\tgetTypedValue().clear();" + NL + "\t\tgetTypedValue().addAll(";
  protected final String TEXT_786 = "value);";
  protected final String TEXT_787 = NL + "\t\tsetTypedValue(value);";
  protected final String TEXT_788 = NL + "\t\tsetTypedValue(((";
  protected final String TEXT_789 = ")value).";
  protected final String TEXT_790 = NL + "\t\tsetTypedValue((";
  protected final String TEXT_791 = ")value);";
  protected final String TEXT_792 = NL + "\t\treturn oldValue;" + NL + "\t}" + NL + "" + NL + "\t/**" + NL + "\t * <!-- begin-user-doc -->" + NL + "\t * <!-- end-user-doc -->" + NL + "\t * @generated" + NL + "\t */";
  protected final String TEXT_793 = " getEMap()" + NL + "\t{" + NL + "\t\t";
  protected final String TEXT_794 = " container = eContainer();" + NL + "\t\treturn container == null ? null : (";
  protected final String TEXT_795 = ")container.eGet(eContainmentFeature());" + NL + "\t}" + NL;
  protected final String TEXT_796 = NL + "} //";

  public String generate(Object argument)
  {
    final StringBuffer stringBuffer = new StringBuffer();
    
/**
 * <copyright>
 *
 * Copyright (c) 2002-2010 IBM Corporation and others.
 * All rights reserved.   This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *   IBM - Initial API and implementation
 *
 * </copyright>
 */

    final GAwareGenClass genClass = (GAwareGenClass) ((Object[]) argument)[0];
    final GenPackage genPackage = genClass.getGenPackage();
    final GenModel genModel = genPackage.getGenModel();
    final boolean isJDK50 = genModel.getComplianceLevel().getValue() >= GenJDKLevel.JDK50;
    final boolean isInterface = Boolean.TRUE.equals(((Object[])argument)[1]); final boolean isImplementation = Boolean.TRUE.equals(((Object[])argument)[2]);
    final boolean isGWT = genModel.getRuntimePlatform() == GenRuntimePlatform.GWT;
    final String publicStaticFinalFlag = isImplementation ? "public static final " : "";
    final String singleWildcard = isJDK50 ? "<?>" : "";
    final String negativeOffsetCorrection = genClass.hasOffsetCorrection() ? " - " + genClass.getOffsetCorrectionField(null) : "";
    final String positiveOffsetCorrection = genClass.hasOffsetCorrection() ? " + " + genClass.getOffsetCorrectionField(null) : "";
    final String negativeOperationOffsetCorrection = genClass.hasOffsetCorrection() ? " - EOPERATION_OFFSET_CORRECTION" : "";
    final String positiveOperationOffsetCorrection = genClass.hasOffsetCorrection() ? " + EOPERATION_OFFSET_CORRECTION" : "";
    stringBuffer.append(TEXT_1);
    stringBuffer.append(TEXT_2);
    {GenBase copyrightHolder = argument instanceof GenBase ? (GenBase)argument : argument instanceof Object[] && ((Object[])argument)[0] instanceof GenBase ? (GenBase)((Object[])argument)[0] : null;
    if (copyrightHolder != null && copyrightHolder.hasCopyright()) {
    stringBuffer.append(TEXT_3);
    stringBuffer.append(copyrightHolder.getCopyright(copyrightHolder.getGenModel().getIndentation(stringBuffer)));
    } else {
    stringBuffer.append(TEXT_4);
    }}
    stringBuffer.append(TEXT_5);
    if (isInterface) {
    stringBuffer.append(TEXT_6);
    stringBuffer.append(genPackage.getInterfacePackageName());
    stringBuffer.append(TEXT_7);
    } else {
    stringBuffer.append(TEXT_6);
    stringBuffer.append(genPackage.getClassPackageName());
    stringBuffer.append(TEXT_7);
    }
    stringBuffer.append(TEXT_8);
    genModel.markImportLocation(stringBuffer, genPackage);
    if (isImplementation) { genClass.addClassPsuedoImports(); }
    stringBuffer.append(TEXT_8);
    if (isInterface) {
    stringBuffer.append(TEXT_9);
    stringBuffer.append(genClass.getFormattedName());
    stringBuffer.append(TEXT_10);
    if (genClass.hasDocumentation()) {
    stringBuffer.append(TEXT_11);
    stringBuffer.append(genClass.getDocumentation(genModel.getIndentation(stringBuffer)));
    stringBuffer.append(TEXT_12);
    }
    if(genClass.hasDeprecationJavadoc()) {
    stringBuffer.append(TEXT_13);
    stringBuffer.append(genClass.getDeprecationJavadoc());
    }
    stringBuffer.append(TEXT_14);
    if (!genClass.getGenFeatures().isEmpty()) {
    stringBuffer.append(TEXT_15);
    for (GenFeature genFeature : genClass.getGenFeatures()) {
    if (!genFeature.isSuppressedGetVisibility()) {
    stringBuffer.append(TEXT_16);
    stringBuffer.append(genClass.getQualifiedInterfaceName());
    stringBuffer.append(TEXT_17);
    stringBuffer.append(genFeature.getGetAccessor());
    stringBuffer.append(TEXT_18);
    stringBuffer.append(genFeature.getFormattedName());
    stringBuffer.append(TEXT_19);
    }
    }
    stringBuffer.append(TEXT_20);
    }
    stringBuffer.append(TEXT_14);
    if (!genModel.isSuppressEMFMetaData()) {
    stringBuffer.append(TEXT_21);
    stringBuffer.append(genPackage.getQualifiedPackageInterfaceName());
    stringBuffer.append(TEXT_22);
    stringBuffer.append(genClass.getClassifierAccessorName());
    stringBuffer.append(TEXT_23);
    }
    if (!genModel.isSuppressEMFModelTags()) { boolean first = true; for (StringTokenizer stringTokenizer = new StringTokenizer(genClass.getModelInfo(), "\n\r"); stringTokenizer.hasMoreTokens(); ) { String modelInfo = stringTokenizer.nextToken(); if (first) { first = false;
    stringBuffer.append(TEXT_24);
    stringBuffer.append(modelInfo);
    } else {
    stringBuffer.append(TEXT_25);
    stringBuffer.append(modelInfo);
    }} if (first) {
    stringBuffer.append(TEXT_26);
    }}
    if (genClass.needsRootExtendsInterfaceExtendsTag()) {
    stringBuffer.append(TEXT_27);
    stringBuffer.append(genModel.getImportedName(genModel.getRootExtendsInterface()));
    }
    stringBuffer.append(TEXT_28);
    if(genClass.isDeprecated()) {
    stringBuffer.append(TEXT_29);
    }
    } else {
    stringBuffer.append(TEXT_30);
    stringBuffer.append(genClass.getFormattedName());
    stringBuffer.append(TEXT_31);
    if (!genClass.getImplementedGenFeatures().isEmpty()) {
    stringBuffer.append(TEXT_32);
    for (GenFeature genFeature : genClass.getImplementedGenFeatures()) {
    stringBuffer.append(TEXT_16);
    stringBuffer.append(genClass.getQualifiedClassName());
    stringBuffer.append(TEXT_17);
    stringBuffer.append(genFeature.getGetAccessor());
    stringBuffer.append(TEXT_18);
    stringBuffer.append(genFeature.getFormattedName());
    stringBuffer.append(TEXT_19);
    }
    stringBuffer.append(TEXT_33);
    }
    stringBuffer.append(TEXT_34);
    }
    if (isImplementation) {
    stringBuffer.append(TEXT_35);
    if (genClass.isAbstract()) {
    stringBuffer.append(TEXT_36);
    }
    stringBuffer.append(TEXT_37);
    stringBuffer.append(genClass.getClassName());
    stringBuffer.append(genClass.getTypeParameters().trim());
    stringBuffer.append(genClass.getClassExtends());
    stringBuffer.append(genClass.getClassImplements());
    } else {
    stringBuffer.append(TEXT_38);
    stringBuffer.append(genClass.getInterfaceName());
    stringBuffer.append(genClass.getTypeParameters().trim());
    stringBuffer.append(genClass.getInterfaceExtends());
    }
    stringBuffer.append(TEXT_39);
    if (genModel.hasCopyrightField()) {
    stringBuffer.append(TEXT_40);
    stringBuffer.append(publicStaticFinalFlag);
    stringBuffer.append(genModel.getImportedName("java.lang.String"));
    stringBuffer.append(TEXT_41);
    stringBuffer.append(genModel.getCopyrightFieldLiteral());
    stringBuffer.append(TEXT_7);
    stringBuffer.append(genModel.getNonNLS());
    stringBuffer.append(TEXT_8);
    }
    if (isImplementation && genModel.getDriverNumber() != null) {
    stringBuffer.append(TEXT_42);
    stringBuffer.append(genModel.getImportedName("java.lang.String"));
    stringBuffer.append(TEXT_43);
    stringBuffer.append(genModel.getDriverNumber());
    stringBuffer.append(TEXT_44);
    stringBuffer.append(genModel.getNonNLS());
    stringBuffer.append(TEXT_8);
    }
    if (isImplementation && genClass.isJavaIOSerializable()) {
    stringBuffer.append(TEXT_45);
    }
    if (isImplementation && genModel.isVirtualDelegation()) { String eVirtualValuesField = genClass.getEVirtualValuesField();
    if (eVirtualValuesField != null) {
    stringBuffer.append(TEXT_46);
    if (isGWT) {
    stringBuffer.append(TEXT_47);
    stringBuffer.append(genModel.getImportedName("com.google.gwt.user.client.rpc.GwtTransient"));
    }
    stringBuffer.append(TEXT_48);
    stringBuffer.append(eVirtualValuesField);
    stringBuffer.append(TEXT_49);
    }
    { List<String> eVirtualIndexBitFields = genClass.getEVirtualIndexBitFields(new ArrayList<String>());
    if (!eVirtualIndexBitFields.isEmpty()) {
    for (String eVirtualIndexBitField : eVirtualIndexBitFields) {
    stringBuffer.append(TEXT_50);
    if (isGWT) {
    stringBuffer.append(TEXT_47);
    stringBuffer.append(genModel.getImportedName("com.google.gwt.user.client.rpc.GwtTransient"));
    }
    stringBuffer.append(TEXT_51);
    stringBuffer.append(eVirtualIndexBitField);
    stringBuffer.append(TEXT_49);
    }
    }
    }
    }
    if (isImplementation && genClass.isModelRoot() && genModel.isBooleanFlagsEnabled() && genModel.getBooleanFlagsReservedBits() == -1) {
    stringBuffer.append(TEXT_52);
    if (isGWT) {
    stringBuffer.append(TEXT_47);
    stringBuffer.append(genModel.getImportedName("com.google.gwt.user.client.rpc.GwtTransient"));
    }
    stringBuffer.append(TEXT_51);
    stringBuffer.append(genModel.getBooleanFlagsField());
    stringBuffer.append(TEXT_53);
    }
    if (isImplementation && !genModel.isReflectiveDelegation()) {
    for (GenFeature genFeature : genClass.getDeclaredFieldGenFeatures()) {
    if (genFeature.hasSettingDelegate()) {
    stringBuffer.append(TEXT_54);
    stringBuffer.append(genFeature.getGetAccessor());
    stringBuffer.append(TEXT_55);
    stringBuffer.append(genFeature.getFormattedName());
    stringBuffer.append(TEXT_56);
    stringBuffer.append(genFeature.getFeatureKind());
    stringBuffer.append(TEXT_57);
    stringBuffer.append(genFeature.getGetAccessor());
    stringBuffer.append(TEXT_58);
    if (isGWT) {
    stringBuffer.append(TEXT_47);
    stringBuffer.append(genModel.getImportedName("com.google.gwt.user.client.rpc.GwtTransient"));
    }
    stringBuffer.append(TEXT_59);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.EStructuralFeature"));
    stringBuffer.append(TEXT_60);
    stringBuffer.append(genFeature.getUpperName());
    stringBuffer.append(TEXT_61);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.EStructuralFeature"));
    stringBuffer.append(TEXT_62);
    stringBuffer.append(genFeature.getQualifiedFeatureAccessor());
    stringBuffer.append(TEXT_63);
    } else if (genFeature.isListType() || genFeature.isReferenceType()) {
    if (genClass.isField(genFeature)) {
    stringBuffer.append(TEXT_64);
    stringBuffer.append(genFeature.getGetAccessor());
    stringBuffer.append(TEXT_55);
    stringBuffer.append(genFeature.getFormattedName());
    stringBuffer.append(TEXT_56);
    stringBuffer.append(genFeature.getFeatureKind());
    stringBuffer.append(TEXT_57);
    stringBuffer.append(genFeature.getGetAccessor());
    stringBuffer.append(TEXT_58);
    if (isGWT) {
    stringBuffer.append(TEXT_47);
    stringBuffer.append(genModel.getImportedName("com.google.gwt.user.client.rpc.GwtTransient"));
    }
    stringBuffer.append(TEXT_59);
    stringBuffer.append(genFeature.getImportedInternalType(genClass));
    stringBuffer.append(TEXT_65);
    stringBuffer.append(genFeature.getSafeName());
    stringBuffer.append(TEXT_49);
    }
    if (genModel.isArrayAccessors() && genFeature.isListType() && !genFeature.isFeatureMapType() && !genFeature.isMapType()) { String rawListItemType = genFeature.getRawListItemType(); int index = rawListItemType.indexOf('['); String head = rawListItemType; String tail = ""; if (index != -1) { head = rawListItemType.substring(0, index); tail = rawListItemType.substring(index); } 
    stringBuffer.append(TEXT_66);
    stringBuffer.append(genFeature.getGetArrayAccessor());
    stringBuffer.append(TEXT_55);
    stringBuffer.append(genFeature.getFormattedName());
    stringBuffer.append(TEXT_67);
    stringBuffer.append(genFeature.getGetArrayAccessor());
    stringBuffer.append(TEXT_58);
    if (genFeature.getQualifiedListItemType(genClass).contains("<")) {
    stringBuffer.append(TEXT_68);
    }
    stringBuffer.append(TEXT_69);
    stringBuffer.append(rawListItemType);
    stringBuffer.append(TEXT_70);
    stringBuffer.append(genFeature.getUpperName());
    stringBuffer.append(TEXT_71);
    stringBuffer.append(head);
    stringBuffer.append(TEXT_72);
    stringBuffer.append(tail);
    stringBuffer.append(TEXT_49);
    }
    } else {
    if (genFeature.hasEDefault() && (!genFeature.isVolatile() || !genModel.isReflectiveDelegation() && (!genFeature.hasDelegateFeature() || !genFeature.isUnsettable()))) { String staticDefaultValue = genFeature.getStaticDefaultValue();
    stringBuffer.append(TEXT_73);
    stringBuffer.append(genFeature.getGetAccessor());
    stringBuffer.append(TEXT_55);
    stringBuffer.append(genFeature.getFormattedName());
    stringBuffer.append(TEXT_56);
    stringBuffer.append(genFeature.getFeatureKind());
    stringBuffer.append(TEXT_57);
    stringBuffer.append(genFeature.getGetAccessor());
    stringBuffer.append(TEXT_58);
    if (genModel.useGenerics() && genFeature.isListDataType() && genFeature.isSetDefaultValue()) {
    stringBuffer.append(TEXT_68);
    }
    stringBuffer.append(TEXT_69);
    stringBuffer.append(genFeature.getImportedType(genClass));
    stringBuffer.append(TEXT_65);
    stringBuffer.append(genFeature.getEDefault());
    if ("".equals(staticDefaultValue)) {
    stringBuffer.append(TEXT_74);
    stringBuffer.append(genFeature.getEcoreFeature().getDefaultValueLiteral());
    stringBuffer.append(TEXT_75);
    } else {
    stringBuffer.append(TEXT_76);
    stringBuffer.append(staticDefaultValue);
    stringBuffer.append(TEXT_7);
    stringBuffer.append(genModel.getNonNLS(staticDefaultValue));
    }
    stringBuffer.append(TEXT_8);
    }
    if (genClass.isField(genFeature)) {
    if (genClass.isFlag(genFeature)) { int flagIndex = genClass.getFlagIndex(genFeature);
    if (flagIndex > 31 && flagIndex % 32 == 0) {
    stringBuffer.append(TEXT_77);
    if (isGWT) {
    stringBuffer.append(TEXT_47);
    stringBuffer.append(genModel.getImportedName("com.google.gwt.user.client.rpc.GwtTransient"));
    }
    stringBuffer.append(TEXT_51);
    stringBuffer.append(genClass.getFlagsField(genFeature));
    stringBuffer.append(TEXT_53);
    }
    if (genFeature.isEnumType()) {
    stringBuffer.append(TEXT_78);
    stringBuffer.append(genFeature.getGetAccessor());
    stringBuffer.append(TEXT_55);
    stringBuffer.append(genFeature.getFormattedName());
    stringBuffer.append(TEXT_56);
    stringBuffer.append(genFeature.getFeatureKind());
    stringBuffer.append(TEXT_79);
    stringBuffer.append(genFeature.getUpperName());
    stringBuffer.append(TEXT_80);
    stringBuffer.append(flagIndex % 32);
    stringBuffer.append(TEXT_81);
    stringBuffer.append(genFeature.getGetAccessor());
    stringBuffer.append(TEXT_55);
    stringBuffer.append(genFeature.getFormattedName());
    stringBuffer.append(TEXT_56);
    stringBuffer.append(genFeature.getFeatureKind());
    stringBuffer.append(TEXT_79);
    stringBuffer.append(genFeature.getUpperName());
    stringBuffer.append(TEXT_82);
    if (isJDK50) {
    stringBuffer.append(genFeature.getEDefault());
    stringBuffer.append(TEXT_83);
    } else {
    stringBuffer.append(genFeature.getImportedType(genClass));
    stringBuffer.append(TEXT_84);
    stringBuffer.append(genFeature.getEDefault());
    stringBuffer.append(TEXT_85);
    }
    stringBuffer.append(TEXT_86);
    stringBuffer.append(genFeature.getUpperName());
    stringBuffer.append(TEXT_87);
    stringBuffer.append(genFeature.getImportedType(genClass));
    stringBuffer.append(TEXT_65);
    stringBuffer.append(genFeature.getTypeGenClassifier().getFormattedName());
    stringBuffer.append(TEXT_88);
    stringBuffer.append(genFeature.getImportedType(genClass));
    stringBuffer.append(TEXT_70);
    stringBuffer.append(genFeature.getUpperName());
    stringBuffer.append(TEXT_89);
    if (isJDK50) {
    stringBuffer.append(genFeature.getImportedType(genClass));
    stringBuffer.append(TEXT_90);
    } else {
    stringBuffer.append(TEXT_91);
    stringBuffer.append(genFeature.getImportedType(genClass));
    stringBuffer.append(TEXT_92);
    stringBuffer.append(genFeature.getImportedType(genClass));
    stringBuffer.append(TEXT_93);
    stringBuffer.append(genFeature.getImportedType(genClass));
    stringBuffer.append(TEXT_94);
    stringBuffer.append(genFeature.getImportedType(genClass));
    stringBuffer.append(TEXT_95);
    }
    stringBuffer.append(TEXT_49);
    }
    stringBuffer.append(TEXT_96);
    stringBuffer.append(genClass.getFlagSize(genFeature) > 1 ? "s" : "");
    stringBuffer.append(TEXT_97);
    stringBuffer.append(genFeature.getGetAccessor());
    stringBuffer.append(TEXT_55);
    stringBuffer.append(genFeature.getFormattedName());
    stringBuffer.append(TEXT_56);
    stringBuffer.append(genFeature.getFeatureKind());
    stringBuffer.append(TEXT_57);
    stringBuffer.append(genFeature.getGetAccessor());
    stringBuffer.append(TEXT_98);
    stringBuffer.append(genFeature.getUpperName());
    stringBuffer.append(TEXT_99);
    stringBuffer.append(genClass.getFlagMask(genFeature));
    stringBuffer.append(TEXT_86);
    if (genFeature.isEnumType()) {
    stringBuffer.append(genFeature.getUpperName());
    stringBuffer.append(TEXT_100);
    } else {
    stringBuffer.append(flagIndex % 32);
    }
    stringBuffer.append(TEXT_49);
    } else {
    stringBuffer.append(TEXT_64);
    stringBuffer.append(genFeature.getGetAccessor());
    stringBuffer.append(TEXT_55);
    stringBuffer.append(genFeature.getFormattedName());
    stringBuffer.append(TEXT_56);
    stringBuffer.append(genFeature.getFeatureKind());
    stringBuffer.append(TEXT_57);
    stringBuffer.append(genFeature.getGetAccessor());
    stringBuffer.append(TEXT_58);
    if (isGWT) {
    stringBuffer.append(TEXT_47);
    stringBuffer.append(genModel.getImportedName("com.google.gwt.user.client.rpc.GwtTransient"));
    }
    stringBuffer.append(TEXT_59);
    stringBuffer.append(genFeature.getImportedType(genClass));
    stringBuffer.append(TEXT_65);
    stringBuffer.append(genFeature.getSafeName());
    if (genFeature.hasEDefault()) {
    stringBuffer.append(TEXT_76);
    stringBuffer.append(genFeature.getEDefault());
    }
    stringBuffer.append(TEXT_49);
    }
    }
    }
    if (genClass.isESetField(genFeature)) {
    if (genClass.isESetFlag(genFeature)) { int flagIndex = genClass.getESetFlagIndex(genFeature);
    if (flagIndex > 31 && flagIndex % 32 == 0) {
    stringBuffer.append(TEXT_77);
    if (isGWT) {
    stringBuffer.append(TEXT_47);
    stringBuffer.append(genModel.getImportedName("com.google.gwt.user.client.rpc.GwtTransient"));
    }
    stringBuffer.append(TEXT_51);
    stringBuffer.append(genClass.getESetFlagsField(genFeature));
    stringBuffer.append(TEXT_53);
    }
    stringBuffer.append(TEXT_101);
    stringBuffer.append(genFeature.getFormattedName());
    stringBuffer.append(TEXT_65);
    stringBuffer.append(genFeature.getFeatureKind());
    stringBuffer.append(TEXT_102);
    stringBuffer.append(genFeature.getUpperName());
    stringBuffer.append(TEXT_103);
    stringBuffer.append(flagIndex % 32 );
    stringBuffer.append(TEXT_49);
    } else {
    stringBuffer.append(TEXT_104);
    stringBuffer.append(genFeature.getFormattedName());
    stringBuffer.append(TEXT_65);
    stringBuffer.append(genFeature.getFeatureKind());
    stringBuffer.append(TEXT_105);
    if (isGWT) {
    stringBuffer.append(TEXT_47);
    stringBuffer.append(genModel.getImportedName("com.google.gwt.user.client.rpc.GwtTransient"));
    }
    stringBuffer.append(TEXT_106);
    stringBuffer.append(genFeature.getUncapName());
    stringBuffer.append(TEXT_107);
    }
    }
    //Class/declaredFieldGenFeature.override.javajetinc
    }
    }
    if (isImplementation && genClass.hasOffsetCorrection() && !genClass.getImplementedGenFeatures().isEmpty()) {
    stringBuffer.append(TEXT_108);
    stringBuffer.append(genClass.getOffsetCorrectionField(null));
    stringBuffer.append(TEXT_76);
    stringBuffer.append(genClass.getQualifiedClassifierAccessor());
    stringBuffer.append(TEXT_109);
    stringBuffer.append(genClass.getImplementedGenFeatures().get(0).getQualifiedFeatureAccessor());
    stringBuffer.append(TEXT_110);
    stringBuffer.append(genClass.getQualifiedFeatureID(genClass.getImplementedGenFeatures().get(0)));
    stringBuffer.append(TEXT_49);
    }
    if (isImplementation && !genModel.isReflectiveDelegation()) {
    for (GenFeature genFeature : genClass.getImplementedGenFeatures()) { GenFeature reverseFeature = genFeature.getReverse();
    if (reverseFeature != null && reverseFeature.getGenClass().hasOffsetCorrection()) {
    stringBuffer.append(TEXT_108);
    stringBuffer.append(genClass.getOffsetCorrectionField(genFeature));
    stringBuffer.append(TEXT_76);
    stringBuffer.append(reverseFeature.getGenClass().getQualifiedClassifierAccessor());
    stringBuffer.append(TEXT_109);
    stringBuffer.append(reverseFeature.getQualifiedFeatureAccessor());
    stringBuffer.append(TEXT_110);
    stringBuffer.append(reverseFeature.getGenClass().getQualifiedFeatureID(reverseFeature));
    stringBuffer.append(TEXT_49);
    }
    }
    }
    if (genModel.isOperationReflection() && isImplementation && genClass.hasOffsetCorrection() && !genClass.getImplementedGenOperations().isEmpty()) {
    stringBuffer.append(TEXT_111);
    stringBuffer.append(genClass.getQualifiedClassifierAccessor());
    stringBuffer.append(TEXT_112);
    stringBuffer.append(genClass.getImplementedGenOperations().get(0).getQualifiedOperationAccessor());
    stringBuffer.append(TEXT_110);
    stringBuffer.append(genClass.getQualifiedOperationID(genClass.getImplementedGenOperations().get(0)));
    stringBuffer.append(TEXT_49);
    }
    if (isImplementation) {
    stringBuffer.append(TEXT_40);
    if (genModel.isPublicConstructors()) {
    stringBuffer.append(TEXT_113);
    } else {
    stringBuffer.append(TEXT_114);
    }
    stringBuffer.append(TEXT_65);
    stringBuffer.append(genClass.getClassName());
    stringBuffer.append(TEXT_115);
    for (GenFeature genFeature : genClass.getFlagGenFeaturesWithDefault()) {
    stringBuffer.append(TEXT_116);
    stringBuffer.append(genClass.getFlagsField(genFeature));
    stringBuffer.append(TEXT_117);
    stringBuffer.append(genFeature.getUpperName());
    stringBuffer.append(TEXT_118);
    if (!genFeature.isBooleanType()) {
    stringBuffer.append(TEXT_119);
    }
    stringBuffer.append(TEXT_7);
    }
    stringBuffer.append(TEXT_120);
    if (genModel.useClassOverrideAnnotation()) {
    stringBuffer.append(TEXT_121);
    }
    stringBuffer.append(TEXT_59);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.EClass"));
    stringBuffer.append(TEXT_122);
    stringBuffer.append(genClass.getQualifiedClassifierAccessor());
    stringBuffer.append(TEXT_123);
    }
    if (isImplementation && (genModel.getFeatureDelegation() == GenDelegationKind.REFLECTIVE_LITERAL || genModel.isDynamicDelegation()) && (genClass.getClassExtendsGenClass() == null || (genClass.getClassExtendsGenClass().getGenModel().getFeatureDelegation() != GenDelegationKind.REFLECTIVE_LITERAL && !genClass.getClassExtendsGenClass().getGenModel().isDynamicDelegation()))) {
    stringBuffer.append(TEXT_124);
    if (genModel.useClassOverrideAnnotation()) {
    stringBuffer.append(TEXT_121);
    }
    stringBuffer.append(TEXT_125);
    stringBuffer.append(genClass.getClassExtendsGenClass() == null ? 0 : genClass.getClassExtendsGenClass().getAllGenFeatures().size());
    stringBuffer.append(TEXT_123);
    }
    //Class/reflectiveDelegation.override.javajetinc
    new Runnable() { public void run() {
    for (GenFeature genFeature : (isImplementation ? genClass.getImplementedGenFeatures() : genClass.getDeclaredGenFeatures())) {
    if (genModel.isArrayAccessors() && genFeature.isListType() && !genFeature.isFeatureMapType() && !genFeature.isMapType()) {
    stringBuffer.append(TEXT_124);
    if (!isImplementation) {
    stringBuffer.append(TEXT_126);
    stringBuffer.append(genFeature.getListItemType(genClass));
    stringBuffer.append(TEXT_70);
    stringBuffer.append(genFeature.getGetArrayAccessor());
    stringBuffer.append(TEXT_127);
    } else {
    stringBuffer.append(TEXT_128);
    stringBuffer.append(genFeature.getListItemType(genClass));
    stringBuffer.append(TEXT_70);
    stringBuffer.append(genFeature.getGetArrayAccessor());
    stringBuffer.append(TEXT_129);
    if (genFeature.isVolatile()) {
    stringBuffer.append(TEXT_116);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.common.util.BasicEList"));
    stringBuffer.append(genFeature.getListTemplateArguments(genClass));
    stringBuffer.append(TEXT_130);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.common.util.BasicEList"));
    stringBuffer.append(genFeature.getListTemplateArguments(genClass));
    stringBuffer.append(TEXT_85);
    stringBuffer.append(genFeature.getGetAccessor());
    stringBuffer.append(TEXT_131);
    if (genModel.useGenerics() && !genFeature.getListItemType(genClass).contains("<") && !genFeature.getListItemType(null).equals(genFeature.getListItemType(genClass))) {
    stringBuffer.append(TEXT_91);
    stringBuffer.append(genFeature.getListItemType(genClass));
    stringBuffer.append(TEXT_92);
    }
    stringBuffer.append(genFeature.getUpperName());
    stringBuffer.append(TEXT_132);
    } else {
    stringBuffer.append(TEXT_133);
    stringBuffer.append(genFeature.getSafeName());
    stringBuffer.append(TEXT_134);
    stringBuffer.append(genFeature.getSafeName());
    stringBuffer.append(TEXT_135);
    if (genModel.useGenerics() && !genFeature.getListItemType(genClass).contains("<") && !genFeature.getListItemType(null).equals(genFeature.getListItemType(genClass))) {
    stringBuffer.append(TEXT_91);
    stringBuffer.append(genFeature.getListItemType(genClass));
    stringBuffer.append(TEXT_92);
    }
    stringBuffer.append(genFeature.getUpperName());
    stringBuffer.append(TEXT_136);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.common.util.BasicEList"));
    stringBuffer.append(genFeature.getListTemplateArguments(genClass));
    stringBuffer.append(TEXT_130);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.common.util.BasicEList"));
    stringBuffer.append(genFeature.getListTemplateArguments(genClass));
    stringBuffer.append(TEXT_85);
    stringBuffer.append(genFeature.getSafeName());
    stringBuffer.append(TEXT_7);
    }
    stringBuffer.append(TEXT_137);
    stringBuffer.append(genFeature.getListItemType(genClass));
    stringBuffer.append(TEXT_138);
    }
    stringBuffer.append(TEXT_124);
    if (!isImplementation) {
    stringBuffer.append(TEXT_126);
    stringBuffer.append(genFeature.getListItemType(genClass));
    stringBuffer.append(TEXT_139);
    stringBuffer.append(genFeature.getAccessorName());
    stringBuffer.append(TEXT_140);
    } else {
    stringBuffer.append(TEXT_128);
    stringBuffer.append(genFeature.getListItemType(genClass));
    stringBuffer.append(TEXT_139);
    stringBuffer.append(genFeature.getAccessorName());
    stringBuffer.append(TEXT_141);
    if (!genModel.useGenerics()) {
    stringBuffer.append(TEXT_91);
    stringBuffer.append(genFeature.getListItemType(genClass));
    stringBuffer.append(TEXT_85);
    }
    stringBuffer.append(genFeature.getGetAccessor());
    stringBuffer.append(TEXT_142);
    }
    stringBuffer.append(TEXT_124);
    if (!isImplementation) {
    stringBuffer.append(TEXT_143);
    stringBuffer.append(genFeature.getAccessorName());
    stringBuffer.append(TEXT_144);
    } else {
    stringBuffer.append(TEXT_145);
    stringBuffer.append(genFeature.getAccessorName());
    stringBuffer.append(TEXT_146);
    if (genFeature.isVolatile()) {
    stringBuffer.append(TEXT_147);
    stringBuffer.append(genFeature.getGetAccessor());
    stringBuffer.append(TEXT_148);
    } else {
    stringBuffer.append(TEXT_147);
    stringBuffer.append(genFeature.getSafeName());
    stringBuffer.append(TEXT_149);
    stringBuffer.append(genFeature.getSafeName());
    stringBuffer.append(TEXT_150);
    }
    stringBuffer.append(TEXT_151);
    }
    stringBuffer.append(TEXT_124);
    if (!isImplementation) {
    stringBuffer.append(TEXT_152);
    stringBuffer.append(((GAwareGenFeature)genFeature).getSetAccessorName());
    stringBuffer.append(TEXT_91);
    stringBuffer.append(genFeature.getListItemType(genClass));
    stringBuffer.append(TEXT_153);
    stringBuffer.append(genFeature.getCapName());
    stringBuffer.append(TEXT_154);
    } else {
    stringBuffer.append(TEXT_155);
    stringBuffer.append(((GAwareGenFeature)genFeature).getSetAccessorName());
    stringBuffer.append(TEXT_91);
    stringBuffer.append(genFeature.getListItemType(genClass));
    stringBuffer.append(TEXT_153);
    stringBuffer.append(genFeature.getCapName());
    stringBuffer.append(TEXT_156);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.common.util.BasicEList"));
    stringBuffer.append(genFeature.getListTemplateArguments(genClass));
    stringBuffer.append(TEXT_85);
    stringBuffer.append(genFeature.getGetAccessor());
    stringBuffer.append(TEXT_157);
    stringBuffer.append(genFeature.getCapName());
    stringBuffer.append(TEXT_158);
    stringBuffer.append(genFeature.getCapName());
    stringBuffer.append(TEXT_159);
    }
    stringBuffer.append(TEXT_124);
    if (!isImplementation) {
    stringBuffer.append(TEXT_152);
    stringBuffer.append(((GAwareGenFeature)genFeature).getSetAccessorName());
    stringBuffer.append(TEXT_160);
    stringBuffer.append(genFeature.getListItemType(genClass));
    stringBuffer.append(TEXT_161);
    } else {
    stringBuffer.append(TEXT_155);
    stringBuffer.append(((GAwareGenFeature)genFeature).getSetAccessorName());
    stringBuffer.append(TEXT_160);
    stringBuffer.append(genFeature.getListItemType(genClass));
    stringBuffer.append(TEXT_162);
    stringBuffer.append(genFeature.getGetAccessor());
    stringBuffer.append(TEXT_163);
    }
    }
    if (genFeature.isGet() && (isImplementation || !genFeature.isSuppressedGetVisibility())) {
    if (isInterface) {
    stringBuffer.append(TEXT_164);
    stringBuffer.append(genFeature.getFormattedName());
    stringBuffer.append(TEXT_165);
    stringBuffer.append(genFeature.getFeatureKind());
    stringBuffer.append(TEXT_166);
    if (genFeature.isListType()) {
    if (genFeature.isMapType()) { GenFeature keyFeature = genFeature.getMapEntryTypeGenClass().getMapEntryKeyFeature(); GenFeature valueFeature = genFeature.getMapEntryTypeGenClass().getMapEntryValueFeature(); 
    stringBuffer.append(TEXT_167);
    if (keyFeature.isListType()) {
    stringBuffer.append(TEXT_168);
    stringBuffer.append(keyFeature.getQualifiedListItemType(genClass));
    stringBuffer.append(TEXT_169);
    } else {
    stringBuffer.append(TEXT_170);
    stringBuffer.append(keyFeature.getType(genClass));
    stringBuffer.append(TEXT_169);
    }
    stringBuffer.append(TEXT_171);
    if (valueFeature.isListType()) {
    stringBuffer.append(TEXT_168);
    stringBuffer.append(valueFeature.getQualifiedListItemType(genClass));
    stringBuffer.append(TEXT_169);
    } else {
    stringBuffer.append(TEXT_170);
    stringBuffer.append(valueFeature.getType(genClass));
    stringBuffer.append(TEXT_169);
    }
    stringBuffer.append(TEXT_172);
    } else if (!genFeature.isWrappedFeatureMapType() && !(genModel.isSuppressEMFMetaData() && "org.eclipse.emf.ecore.EObject".equals(genFeature.getQualifiedListItemType(genClass)))) {
String typeName = genFeature.getQualifiedListItemType(genClass); String head = typeName; String tail = ""; int index = typeName.indexOf('<'); if (index == -1) { index = typeName.indexOf('['); } 
if (index != -1) { head = typeName.substring(0, index); tail = typeName.substring(index).replaceAll("<", "&lt;"); }

    stringBuffer.append(TEXT_173);
    stringBuffer.append(head);
    stringBuffer.append(TEXT_169);
    stringBuffer.append(tail);
    stringBuffer.append(TEXT_166);
    }
    } else if (genFeature.isSetDefaultValue()) {
    stringBuffer.append(TEXT_174);
    stringBuffer.append(genFeature.getDefaultValue());
    stringBuffer.append(TEXT_175);
    }
    if (genFeature.getTypeGenEnum() != null) {
    stringBuffer.append(TEXT_176);
    stringBuffer.append(genFeature.getTypeGenEnum().getQualifiedName());
    stringBuffer.append(TEXT_177);
    }
    if (genFeature.isBidirectional() && !genFeature.getReverse().getGenClass().isMapEntry()) { GenFeature reverseGenFeature = genFeature.getReverse(); 
    if (!reverseGenFeature.isSuppressedGetVisibility()) {
    stringBuffer.append(TEXT_178);
    stringBuffer.append(reverseGenFeature.getGenClass().getQualifiedInterfaceName());
    stringBuffer.append(TEXT_17);
    stringBuffer.append(reverseGenFeature.getGetAccessor());
    stringBuffer.append(TEXT_18);
    stringBuffer.append(reverseGenFeature.getFormattedName());
    stringBuffer.append(TEXT_179);
    }
    }
    stringBuffer.append(TEXT_180);
    if (!genFeature.hasDocumentation()) {
    stringBuffer.append(TEXT_181);
    stringBuffer.append(genFeature.getFormattedName());
    stringBuffer.append(TEXT_182);
    stringBuffer.append(genFeature.getFeatureKind());
    stringBuffer.append(TEXT_183);
    }
    stringBuffer.append(TEXT_184);
    if (genFeature.hasDocumentation()) {
    stringBuffer.append(TEXT_185);
    stringBuffer.append(genFeature.getDocumentation(genModel.getIndentation(stringBuffer)));
    stringBuffer.append(TEXT_186);
    }
    stringBuffer.append(TEXT_187);
    stringBuffer.append(genFeature.getFormattedName());
    stringBuffer.append(TEXT_182);
    stringBuffer.append(genFeature.getFeatureKind());
    stringBuffer.append(TEXT_166);
    if (genFeature.getTypeGenEnum() != null) {
    stringBuffer.append(TEXT_188);
    stringBuffer.append(genFeature.getTypeGenEnum().getQualifiedName());
    }
    if (genFeature.isUnsettable()) {
    if (!genFeature.isSuppressedIsSetVisibility()) {
    stringBuffer.append(TEXT_189);
    stringBuffer.append(((GAwareGenFeature)genFeature).getIsSetAccessorName());
    stringBuffer.append(TEXT_23);
    }
    if (genFeature.isChangeable() && !genFeature.isSuppressedUnsetVisibility()) {
    stringBuffer.append(TEXT_189);
    stringBuffer.append(((GAwareGenFeature)genFeature).getUnsetAccessorName());
    stringBuffer.append(TEXT_23);
    }
    }
    if (genFeature.isChangeable() && !genFeature.isListType() && !genFeature.isSuppressedSetVisibility()) {
    stringBuffer.append(TEXT_190);
    stringBuffer.append(genFeature.getAccessorName());
    stringBuffer.append(TEXT_91);
    stringBuffer.append(genFeature.getRawImportedBoundType());
    stringBuffer.append(TEXT_85);
    }
    if (!genModel.isSuppressEMFMetaData()) {
    stringBuffer.append(TEXT_188);
    stringBuffer.append(genPackage.getQualifiedPackageInterfaceName());
    stringBuffer.append(TEXT_22);
    stringBuffer.append(genFeature.getFeatureAccessorName());
    stringBuffer.append(TEXT_23);
    }
    if (genFeature.isBidirectional() && !genFeature.getReverse().getGenClass().isMapEntry()) { GenFeature reverseGenFeature = genFeature.getReverse(); 
    if (!reverseGenFeature.isSuppressedGetVisibility()) {
    stringBuffer.append(TEXT_188);
    stringBuffer.append(reverseGenFeature.getGenClass().getQualifiedInterfaceName());
    stringBuffer.append(TEXT_17);
    stringBuffer.append(reverseGenFeature.getGetAccessor());
    }
    }
    if (!genModel.isSuppressEMFModelTags()) { boolean first = true; for (StringTokenizer stringTokenizer = new StringTokenizer(genFeature.getModelInfo(), "\n\r"); stringTokenizer.hasMoreTokens(); ) { String modelInfo = stringTokenizer.nextToken(); if (first) { first = false;
    stringBuffer.append(TEXT_191);
    stringBuffer.append(modelInfo);
    } else {
    stringBuffer.append(TEXT_192);
    stringBuffer.append(modelInfo);
    }} if (first) {
    stringBuffer.append(TEXT_193);
    }}
    stringBuffer.append(TEXT_194);
    //Class/getGenFeature.javadoc.override.javajetinc
    } else {
    stringBuffer.append(TEXT_124);
    if (isJDK50) { //Class/getGenFeature.annotations.insert.javajetinc
    }
    }
    if (!isImplementation) {
    stringBuffer.append(TEXT_126);
    stringBuffer.append(genFeature.getImportedType(genClass));
    stringBuffer.append(TEXT_65);
    stringBuffer.append(genFeature.getGetAccessor());
    stringBuffer.append(TEXT_127);
    } else {
    if (genModel.useGenerics() && ((genFeature.isContainer() || genFeature.isResolveProxies()) && !genFeature.isListType() && !(genModel.isReflectiveDelegation() && genModel.isDynamicDelegation()) && genFeature.isUncheckedCast(genClass) || genFeature.isListType() && !genFeature.isFeatureMapType() && (genModel.isReflectiveDelegation() || genModel.isVirtualDelegation() || genModel.isDynamicDelegation()) || genFeature.isListDataType() && genFeature.hasDelegateFeature() || genFeature.isListType() && genFeature.hasSettingDelegate())) {
    stringBuffer.append(TEXT_68);
    }
    stringBuffer.append(TEXT_128);
    stringBuffer.append(genFeature.getImportedType(genClass));
    stringBuffer.append(TEXT_65);
    stringBuffer.append(genFeature.getGetAccessor());
    if (genClass.hasCollidingGetAccessorOperation(genFeature)) {
    stringBuffer.append(TEXT_195);
    }
    stringBuffer.append(TEXT_129);
    if (genModel.isDynamicDelegation()) {
    stringBuffer.append(TEXT_147);
    if (!isJDK50 && genFeature.isPrimitiveType()) {
    stringBuffer.append(TEXT_91);
    }
    stringBuffer.append(TEXT_91);
    stringBuffer.append(genFeature.getObjectType(genClass));
    stringBuffer.append(TEXT_196);
    stringBuffer.append(genClass.getQualifiedFeatureID(genFeature));
    stringBuffer.append(TEXT_197);
    stringBuffer.append(genFeature.getQualifiedFeatureAccessor());
    stringBuffer.append(TEXT_198);
    stringBuffer.append(!genFeature.isEffectiveSuppressEMFTypes());
    stringBuffer.append(TEXT_85);
    if (!isJDK50 && genFeature.isPrimitiveType()) {
    stringBuffer.append(TEXT_199);
    stringBuffer.append(genFeature.getPrimitiveValueFunction());
    stringBuffer.append(TEXT_23);
    }
    stringBuffer.append(TEXT_7);
    } else if (genModel.isReflectiveDelegation()) {
    stringBuffer.append(TEXT_147);
    if (!isJDK50 && genFeature.isPrimitiveType()) {
    stringBuffer.append(TEXT_91);
    }
    stringBuffer.append(TEXT_91);
    stringBuffer.append(genFeature.getObjectType(genClass));
    stringBuffer.append(TEXT_200);
    stringBuffer.append(genFeature.getQualifiedFeatureAccessor());
    stringBuffer.append(TEXT_201);
    if (!isJDK50 && genFeature.isPrimitiveType()) {
    stringBuffer.append(TEXT_199);
    stringBuffer.append(genFeature.getPrimitiveValueFunction());
    stringBuffer.append(TEXT_23);
    }
    stringBuffer.append(TEXT_7);
    } else if (genFeature.hasSettingDelegate()) {
    stringBuffer.append(TEXT_147);
    if (!isJDK50 && genFeature.isPrimitiveType()) {
    stringBuffer.append(TEXT_91);
    }
    stringBuffer.append(TEXT_91);
    stringBuffer.append(genFeature.getObjectType(genClass));
    stringBuffer.append(TEXT_85);
    stringBuffer.append(genFeature.getUpperName());
    stringBuffer.append(TEXT_202);
    if (!isJDK50 && genFeature.isPrimitiveType()) {
    stringBuffer.append(TEXT_199);
    stringBuffer.append(genFeature.getPrimitiveValueFunction());
    stringBuffer.append(TEXT_23);
    }
    stringBuffer.append(TEXT_7);
    } else if (!genFeature.isVolatile()) {
    if (genFeature.isListType()) {
    if (genModel.isVirtualDelegation()) {
    stringBuffer.append(TEXT_116);
    stringBuffer.append(genFeature.getImportedType(genClass));
    stringBuffer.append(TEXT_65);
    stringBuffer.append(genFeature.getSafeName());
    stringBuffer.append(TEXT_203);
    stringBuffer.append(genFeature.getImportedType(genClass));
    stringBuffer.append(TEXT_204);
    stringBuffer.append(genClass.getQualifiedFeatureID(genFeature));
    stringBuffer.append(positiveOffsetCorrection);
    stringBuffer.append(TEXT_205);
    }
    stringBuffer.append(TEXT_133);
    stringBuffer.append(genFeature.getSafeName());
    stringBuffer.append(TEXT_206);
    if (genModel.isVirtualDelegation()) {
    stringBuffer.append(TEXT_207);
    stringBuffer.append(genClass.getQualifiedFeatureID(genFeature));
    stringBuffer.append(positiveOffsetCorrection);
    stringBuffer.append(TEXT_197);
    stringBuffer.append(genFeature.getSafeName());
    stringBuffer.append(TEXT_208);
    stringBuffer.append(genClass.getListConstructor(genFeature));
    stringBuffer.append(TEXT_205);
    } else {
    stringBuffer.append(TEXT_209);
    stringBuffer.append(genFeature.getSafeName());
    stringBuffer.append(TEXT_208);
    stringBuffer.append(genClass.getListConstructor(genFeature));
    stringBuffer.append(TEXT_7);
    }
    stringBuffer.append(TEXT_210);
    stringBuffer.append(genFeature.getSafeName());
    stringBuffer.append(genFeature.isMapType() && genFeature.isEffectiveSuppressEMFTypes() ? ".map()" : "");
    stringBuffer.append(TEXT_7);
    } else if (genFeature.isContainer()) {
    stringBuffer.append(TEXT_211);
    stringBuffer.append(genClass.getQualifiedFeatureID(genFeature));
    stringBuffer.append(positiveOffsetCorrection);
    stringBuffer.append(TEXT_212);
    stringBuffer.append(genFeature.getImportedType(genClass));
    stringBuffer.append(TEXT_213);
    } else {
    if (genFeature.isResolveProxies()) {
    if (genModel.isVirtualDelegation()) {
    stringBuffer.append(TEXT_116);
    stringBuffer.append(genFeature.getImportedType(genClass));
    stringBuffer.append(TEXT_65);
    stringBuffer.append(genFeature.getSafeName());
    stringBuffer.append(TEXT_203);
    stringBuffer.append(genFeature.getImportedType(genClass));
    stringBuffer.append(TEXT_204);
    stringBuffer.append(genClass.getQualifiedFeatureID(genFeature));
    stringBuffer.append(positiveOffsetCorrection);
    if (genFeature.hasEDefault()) {
    stringBuffer.append(TEXT_197);
    stringBuffer.append(genFeature.getEDefault());
    }
    stringBuffer.append(TEXT_205);
    }
    stringBuffer.append(TEXT_133);
    stringBuffer.append(genFeature.getSafeName());
    stringBuffer.append(TEXT_214);
    stringBuffer.append(genFeature.getSafeNameAsEObject());
    stringBuffer.append(TEXT_215);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.InternalEObject"));
    stringBuffer.append(TEXT_216);
    stringBuffer.append(genFeature.getCapName());
    stringBuffer.append(TEXT_203);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.InternalEObject"));
    stringBuffer.append(TEXT_85);
    stringBuffer.append(genFeature.getSafeName());
    stringBuffer.append(TEXT_217);
    stringBuffer.append(genFeature.getSafeName());
    stringBuffer.append(TEXT_76);
    stringBuffer.append(genFeature.getNonEObjectInternalTypeCast(genClass));
    stringBuffer.append(TEXT_218);
    stringBuffer.append(genFeature.getCapName());
    stringBuffer.append(TEXT_219);
    stringBuffer.append(genFeature.getSafeName());
    stringBuffer.append(TEXT_220);
    stringBuffer.append(genFeature.getCapName());
    stringBuffer.append(TEXT_221);
    if (genFeature.isEffectiveContains()) {
    stringBuffer.append(TEXT_222);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.InternalEObject"));
    stringBuffer.append(TEXT_223);
    stringBuffer.append(genFeature.getCapName());
    stringBuffer.append(TEXT_203);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.InternalEObject"));
    stringBuffer.append(TEXT_85);
    stringBuffer.append(genFeature.getSafeName());
    stringBuffer.append(TEXT_7);
    if (!genFeature.isBidirectional()) {
    stringBuffer.append(TEXT_222);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.common.notify.NotificationChain"));
    stringBuffer.append(TEXT_224);
    stringBuffer.append(genFeature.getCapName());
    stringBuffer.append(TEXT_225);
    stringBuffer.append(genClass.getQualifiedFeatureID(genFeature));
    stringBuffer.append(negativeOffsetCorrection);
    stringBuffer.append(TEXT_226);
    } else { GenFeature reverseFeature = genFeature.getReverse(); GenClass targetClass = reverseFeature.getGenClass(); String reverseOffsetCorrection = targetClass.hasOffsetCorrection() ? " + " + genClass.getOffsetCorrectionField(genFeature) : "";
    stringBuffer.append(TEXT_222);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.common.notify.NotificationChain"));
    stringBuffer.append(TEXT_227);
    stringBuffer.append(genFeature.getCapName());
    stringBuffer.append(TEXT_228);
    stringBuffer.append(targetClass.getQualifiedFeatureID(reverseFeature));
    stringBuffer.append(reverseOffsetCorrection);
    stringBuffer.append(TEXT_197);
    stringBuffer.append(targetClass.getRawImportedInterfaceName());
    stringBuffer.append(TEXT_229);
    }
    stringBuffer.append(TEXT_230);
    stringBuffer.append(genFeature.getCapName());
    stringBuffer.append(TEXT_231);
    if (!genFeature.isBidirectional()) {
    stringBuffer.append(TEXT_232);
    stringBuffer.append(genFeature.getCapName());
    stringBuffer.append(TEXT_233);
    stringBuffer.append(genClass.getQualifiedFeatureID(genFeature));
    stringBuffer.append(negativeOffsetCorrection);
    stringBuffer.append(TEXT_234);
    } else { GenFeature reverseFeature = genFeature.getReverse(); GenClass targetClass = reverseFeature.getGenClass(); String reverseOffsetCorrection = targetClass.hasOffsetCorrection() ? " + " + genClass.getOffsetCorrectionField(genFeature) : "";
    stringBuffer.append(TEXT_235);
    stringBuffer.append(genFeature.getCapName());
    stringBuffer.append(TEXT_236);
    stringBuffer.append(targetClass.getQualifiedFeatureID(reverseFeature));
    stringBuffer.append(reverseOffsetCorrection);
    stringBuffer.append(TEXT_197);
    stringBuffer.append(targetClass.getRawImportedInterfaceName());
    stringBuffer.append(TEXT_237);
    }
    stringBuffer.append(TEXT_238);
    } else if (genModel.isVirtualDelegation()) {
    stringBuffer.append(TEXT_239);
    stringBuffer.append(genClass.getQualifiedFeatureID(genFeature));
    stringBuffer.append(positiveOffsetCorrection);
    stringBuffer.append(TEXT_197);
    stringBuffer.append(genFeature.getSafeName());
    stringBuffer.append(TEXT_205);
    }
    if (!genModel.isSuppressNotification()) {
    stringBuffer.append(TEXT_240);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.impl.ENotificationImpl"));
    stringBuffer.append(TEXT_241);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.common.notify.Notification"));
    stringBuffer.append(TEXT_242);
    stringBuffer.append(genClass.getQualifiedFeatureID(genFeature));
    stringBuffer.append(positiveOffsetCorrection);
    stringBuffer.append(TEXT_243);
    stringBuffer.append(genFeature.getCapName());
    stringBuffer.append(TEXT_197);
    stringBuffer.append(genFeature.getSafeName());
    stringBuffer.append(TEXT_244);
    }
    stringBuffer.append(TEXT_245);
    }
    if (!genFeature.isResolveProxies() && genModel.isVirtualDelegation() && !genFeature.isPrimitiveType()) {
    stringBuffer.append(TEXT_246);
    stringBuffer.append(genFeature.getImportedType(genClass));
    stringBuffer.append(TEXT_204);
    stringBuffer.append(genClass.getQualifiedFeatureID(genFeature));
    stringBuffer.append(positiveOffsetCorrection);
    if (genFeature.hasEDefault()) {
    stringBuffer.append(TEXT_197);
    stringBuffer.append(genFeature.getEDefault());
    }
    stringBuffer.append(TEXT_205);
    } else if (genClass.isFlag(genFeature)) {
    if (genFeature.isBooleanType()) {
    stringBuffer.append(TEXT_246);
    stringBuffer.append(genClass.getFlagsField(genFeature));
    stringBuffer.append(TEXT_247);
    stringBuffer.append(genFeature.getUpperName());
    stringBuffer.append(TEXT_248);
    } else {
    stringBuffer.append(TEXT_147);
    stringBuffer.append(genFeature.getUpperName());
    stringBuffer.append(TEXT_249);
    stringBuffer.append(genClass.getFlagsField(genFeature));
    stringBuffer.append(TEXT_247);
    stringBuffer.append(genFeature.getUpperName());
    stringBuffer.append(TEXT_250);
    stringBuffer.append(genFeature.getUpperName());
    stringBuffer.append(TEXT_251);
    }
    } else {
    stringBuffer.append(TEXT_147);
    stringBuffer.append(genFeature.getSafeName());
    stringBuffer.append(TEXT_7);
    }
    }
    } else {//volatile
    if (genFeature.isResolveProxies() && !genFeature.isListType()) {
    stringBuffer.append(TEXT_116);
    stringBuffer.append(genFeature.getImportedType(genClass));
    stringBuffer.append(TEXT_65);
    stringBuffer.append(genFeature.getSafeName());
    stringBuffer.append(TEXT_252);
    stringBuffer.append(genFeature.getAccessorName());
    stringBuffer.append(TEXT_253);
    stringBuffer.append(genFeature.getSafeName());
    stringBuffer.append(TEXT_214);
    stringBuffer.append(genFeature.getSafeNameAsEObject());
    stringBuffer.append(TEXT_254);
    stringBuffer.append(genFeature.getNonEObjectInternalTypeCast(genClass));
    stringBuffer.append(TEXT_255);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.InternalEObject"));
    stringBuffer.append(TEXT_85);
    stringBuffer.append(genFeature.getSafeName());
    stringBuffer.append(TEXT_256);
    stringBuffer.append(genFeature.getSafeName());
    stringBuffer.append(TEXT_7);
    } else if (genFeature.hasDelegateFeature()) { GenFeature delegateFeature = genFeature.getDelegateFeature();
    if (genFeature.isFeatureMapType()) {
    String featureMapEntryTemplateArgument = isJDK50 ? "<" + genModel.getImportedName("org.eclipse.emf.ecore.util.FeatureMap") + ".Entry>" : "";
    if (delegateFeature.isWrappedFeatureMapType()) {
    stringBuffer.append(TEXT_257);
    stringBuffer.append(genFeature.getImportedEffectiveFeatureMapWrapperClass());
    stringBuffer.append(TEXT_258);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.util.FeatureMap"));
    stringBuffer.append(TEXT_259);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.util.FeatureMap"));
    stringBuffer.append(TEXT_260);
    stringBuffer.append(delegateFeature.getAccessorName());
    stringBuffer.append(TEXT_261);
    stringBuffer.append(featureMapEntryTemplateArgument);
    stringBuffer.append(TEXT_262);
    stringBuffer.append(genFeature.getQualifiedFeatureAccessor());
    stringBuffer.append(TEXT_244);
    } else {
    stringBuffer.append(TEXT_246);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.util.FeatureMap"));
    stringBuffer.append(TEXT_263);
    stringBuffer.append(delegateFeature.getAccessorName());
    stringBuffer.append(TEXT_264);
    stringBuffer.append(featureMapEntryTemplateArgument);
    stringBuffer.append(TEXT_262);
    stringBuffer.append(genFeature.getQualifiedFeatureAccessor());
    stringBuffer.append(TEXT_205);
    }
    } else if (genFeature.isListType()) {
    if (delegateFeature.isWrappedFeatureMapType()) {
    stringBuffer.append(TEXT_265);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.util.FeatureMap"));
    stringBuffer.append(TEXT_260);
    stringBuffer.append(delegateFeature.getAccessorName());
    stringBuffer.append(TEXT_266);
    stringBuffer.append(genFeature.getQualifiedFeatureAccessor());
    stringBuffer.append(TEXT_205);
    } else {
    stringBuffer.append(TEXT_267);
    stringBuffer.append(delegateFeature.getAccessorName());
    stringBuffer.append(TEXT_268);
    stringBuffer.append(genFeature.getQualifiedFeatureAccessor());
    stringBuffer.append(TEXT_205);
    }
    } else {
    if (delegateFeature.isWrappedFeatureMapType()) {
    stringBuffer.append(TEXT_147);
    if (!isJDK50 && genFeature.isPrimitiveType()) {
    stringBuffer.append(TEXT_91);
    }
    if (genFeature.getTypeGenDataType() == null || !genFeature.getTypeGenDataType().isObjectType()) {
    stringBuffer.append(TEXT_91);
    stringBuffer.append(genFeature.getObjectType(genClass));
    stringBuffer.append(TEXT_85);
    }
    stringBuffer.append(TEXT_258);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.util.FeatureMap"));
    stringBuffer.append(TEXT_260);
    stringBuffer.append(delegateFeature.getAccessorName());
    stringBuffer.append(TEXT_269);
    stringBuffer.append(genFeature.getQualifiedFeatureAccessor());
    stringBuffer.append(TEXT_201);
    if (!isJDK50 && genFeature.isPrimitiveType()) {
    stringBuffer.append(TEXT_199);
    stringBuffer.append(genFeature.getPrimitiveValueFunction());
    stringBuffer.append(TEXT_23);
    }
    stringBuffer.append(TEXT_7);
    } else {
    stringBuffer.append(TEXT_147);
    if (!isJDK50 && genFeature.isPrimitiveType()) {
    stringBuffer.append(TEXT_91);
    }
    if (genFeature.getTypeGenDataType() == null || !genFeature.getTypeGenDataType().isObjectType()) {
    stringBuffer.append(TEXT_91);
    stringBuffer.append(genFeature.getObjectType(genClass));
    stringBuffer.append(TEXT_85);
    }
    stringBuffer.append(TEXT_270);
    stringBuffer.append(delegateFeature.getAccessorName());
    stringBuffer.append(TEXT_271);
    stringBuffer.append(genFeature.getQualifiedFeatureAccessor());
    stringBuffer.append(TEXT_201);
    if (!isJDK50 && genFeature.isPrimitiveType()) {
    stringBuffer.append(TEXT_199);
    stringBuffer.append(genFeature.getPrimitiveValueFunction());
    stringBuffer.append(TEXT_23);
    }
    stringBuffer.append(TEXT_7);
    }
    }
    } else if (genClass.getGetAccessorOperation(genFeature) != null) {
    stringBuffer.append(TEXT_116);
    stringBuffer.append(genClass.getGetAccessorOperation(genFeature).getBody(genModel.getIndentation(stringBuffer)));
    } else {
    //GEastadl ###############
    GAwareGenFeature gGenFeature = (GAwareGenFeature)genFeature;
    if(gGenFeature.isGGenFeature()){
    stringBuffer.append(TEXT_126);
     TargetFeaturePath tgtFeaturePath = gGenFeature.getTargetFeaturePath(genClass);
    stringBuffer.append(TEXT_126);
     if(tgtFeaturePath.getClass().equals(NoMappingTargetFeaturePath.class)){
    stringBuffer.append(TEXT_116);
    stringBuffer.append(TEXT_272);
     } else {
    stringBuffer.append(TEXT_116);
     /**** Simple Case: Single-Segmented TargetFeaturePath ****/ 
    stringBuffer.append(TEXT_116);
     if(tgtFeaturePath.getSegmentCount() == 1) { 
    stringBuffer.append(TEXT_209);
     if(gGenFeature.isListType()) { 
    stringBuffer.append(TEXT_222);
     if(tgtFeaturePath.isListType()) { // List => List
    stringBuffer.append(TEXT_273);
    stringBuffer.append(gGenFeature.getRawImportedType());
    stringBuffer.append(TEXT_274);
    stringBuffer.append(tgtFeaturePath.getFirstFeature().getGetAccessor());
    stringBuffer.append(TEXT_275);
     } else if(tgtFeaturePath.isJavaMappedType()) { // List => JavaMappedType 
    stringBuffer.append(TEXT_276);
    stringBuffer.append(TEXT_277);
     } else if(tgtFeaturePath.isEnumType()) { // List => Enum 
    stringBuffer.append(TEXT_276);
    stringBuffer.append(TEXT_278);
     } else { // List => EObject
    stringBuffer.append(TEXT_279);
    stringBuffer.append(tgtFeaturePath.getLastFeature().getImportedType(genClass));
    stringBuffer.append(TEXT_65);
    stringBuffer.append(tgtFeaturePath.getLastFeature().getTypeGenClassifier().getSafeUncapName());
    stringBuffer.append(TEXT_76);
    stringBuffer.append(tgtFeaturePath.getLastFeature().getGetAccessor());
    stringBuffer.append(TEXT_280);
    stringBuffer.append(tgtFeaturePath.getLastFeature().getTypeGenClassifier().getSafeUncapName());
    stringBuffer.append(TEXT_281);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.util.EcoreEList"));
    stringBuffer.append(TEXT_282);
    stringBuffer.append(gGenFeature.getImportedElementType(genClass));
    stringBuffer.append(TEXT_283);
    stringBuffer.append(gGenFeature.getName());
    stringBuffer.append(TEXT_284);
    stringBuffer.append(tgtFeaturePath.getLastFeature().getGetAccessor());
    stringBuffer.append(TEXT_285);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.util.EcoreEList"));
    stringBuffer.append(TEXT_282);
    stringBuffer.append(gGenFeature.getImportedElementType(genClass));
    stringBuffer.append(TEXT_283);
    stringBuffer.append(gGenFeature.getName());
    stringBuffer.append(TEXT_286);
     } 
    stringBuffer.append(TEXT_287);
     } else if(gGenFeature.isJavaMappedType()) { 
    stringBuffer.append(TEXT_222);
     if(tgtFeaturePath.isListType()) { // JavaMappedType => List
    stringBuffer.append(TEXT_276);
    stringBuffer.append(TEXT_288);
     } else if(tgtFeaturePath.isJavaMappedType()) { // JavaMappedType => JavaMappedType 
    stringBuffer.append(TEXT_273);
    stringBuffer.append(genModel.getImportedName(tgtFeaturePath.getImportedType(genClass)));
    stringBuffer.append(TEXT_289);
    stringBuffer.append(tgtFeaturePath.getLastFeature().getGetAccessor());
    stringBuffer.append(TEXT_290);
     if(gGenFeature.isInteger() && tgtFeaturePath.isLong()) { // Integer => Long 
    stringBuffer.append(TEXT_291);
     } else if(gGenFeature.isInteger() && tgtFeaturePath.isString()) { // Integer => String 
    stringBuffer.append(TEXT_292);
     } else if(gGenFeature.isDouble() && tgtFeaturePath.isString()) { // Double => String 
    stringBuffer.append(TEXT_293);
     } else if(gGenFeature.isBigDecimal() && tgtFeaturePath.isString()) { // BigDecimal => String 
    stringBuffer.append(TEXT_294);
    stringBuffer.append(genModel.getImportedName("java.math.BigDecimal"));
    stringBuffer.append(TEXT_295);
     } else if(gGenFeature.isBigDecimal() && tgtFeaturePath.isInteger()) { // BigDecimal => Integer 
    stringBuffer.append(TEXT_294);
    stringBuffer.append(genModel.getImportedName("java.math.BigDecimal"));
    stringBuffer.append(TEXT_296);
     } else { // Any JavaMappedType => Any JavaMappedType 
    stringBuffer.append(TEXT_297);
     } 
    stringBuffer.append(TEXT_298);
     } else if(tgtFeaturePath.isEnumType()) { // JavaMappedType => Enum 
    stringBuffer.append(TEXT_276);
    stringBuffer.append(TEXT_299);
     } else { // JavaMappedType => EObject
    stringBuffer.append(TEXT_276);
    stringBuffer.append(TEXT_300);
     } 
    stringBuffer.append(TEXT_301);
     } else if(gGenFeature.isEnumType()) { 
    stringBuffer.append(TEXT_222);
     if(tgtFeaturePath.isEnumType()) { // Enum => Enum 
    stringBuffer.append(TEXT_302);
    stringBuffer.append(gGenFeature.getImportedType(genClass));
    stringBuffer.append(TEXT_303);
    stringBuffer.append(tgtFeaturePath.getLastFeature().getGetAccessor());
    stringBuffer.append(TEXT_304);
     } else if(tgtFeaturePath.isListType()) { // Enum => List 
    stringBuffer.append(TEXT_276);
    stringBuffer.append(TEXT_305);
     } else if(tgtFeaturePath.isJavaMappedType()) { // Enum => JavaMappedType 
    stringBuffer.append(TEXT_276);
    stringBuffer.append(TEXT_306);
     } else { // Enum => EObject 
    stringBuffer.append(TEXT_276);
    stringBuffer.append(TEXT_307);
     } 
    stringBuffer.append(TEXT_308);
     } else { 
    stringBuffer.append(TEXT_222);
    if(tgtFeaturePath.isListType()) { // EObject => List 
    stringBuffer.append(TEXT_276);
    stringBuffer.append(tgtFeaturePath.getImportedType());
    stringBuffer.append(TEXT_309);
    stringBuffer.append(tgtFeaturePath.getLastFeature().getGetAccessor());
    stringBuffer.append(TEXT_310);
     if(tgtFeaturePath.getLastFeature().getEcoreFeature().getEType() != gGenFeature.getEcoreFeature().getEType()) { 
    stringBuffer.append(TEXT_311);
    stringBuffer.append(gGenFeature.getEcoreFeature().getEType().getName());
    stringBuffer.append(TEXT_312);
     } else { 
    stringBuffer.append(TEXT_313);
     } 
    stringBuffer.append(TEXT_222);
     } else if(tgtFeaturePath.isJavaMappedType()) { // EObject => JavaMappedType 
    stringBuffer.append(TEXT_276);
    stringBuffer.append(TEXT_314);
     } else if(tgtFeaturePath.isEnumType()) { // EObject => Enum 
    stringBuffer.append(TEXT_276);
    stringBuffer.append(TEXT_315);
     } else { // EObject => EObject 
    stringBuffer.append(TEXT_316);
    stringBuffer.append(tgtFeaturePath.getLastFeature().getGetAccessor());
    stringBuffer.append(TEXT_317);
     } 
    stringBuffer.append(TEXT_209);
     } 
    stringBuffer.append(TEXT_116);
     /**** Complex Case: TargetFeaturePath with multiple segments ****/ 
    stringBuffer.append(TEXT_318);
     } else if(tgtFeaturePath.getSegmentCount() > 1) { 
    stringBuffer.append(TEXT_319);
     if(gGenFeature.isListType()) { 
    stringBuffer.append(TEXT_222);
     if(tgtFeaturePath.isListType()) { // List => List
    stringBuffer.append(TEXT_276);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.common.util.EList"));
    stringBuffer.append(TEXT_320);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.common.util.BasicEList"));
    stringBuffer.append(TEXT_321);
    GAwareGenFeature previousFeature = tgtFeaturePath.getFirstFeature();
     if(previousFeature.isListType()) { 
    stringBuffer.append(TEXT_322);
    stringBuffer.append(previousFeature.getGetAccessor());
    stringBuffer.append(TEXT_323);
     } else { 
    stringBuffer.append(TEXT_126);
    stringBuffer.append(previousFeature.getImportedElementType(genClass));
    stringBuffer.append(TEXT_65);
    stringBuffer.append(previousFeature.getName());
    stringBuffer.append(TEXT_76);
    stringBuffer.append(previousFeature.getGetAccessor());
    stringBuffer.append(TEXT_324);
    stringBuffer.append(previousFeature.getName());
    stringBuffer.append(TEXT_325);
    stringBuffer.append(previousFeature.getGetAccessor());
    stringBuffer.append(TEXT_326);
     } 
    stringBuffer.append(TEXT_327);
     if(!tgtFeaturePath.getTailFeatures().isEmpty()) { 
    stringBuffer.append(TEXT_126);
    stringBuffer.append(genModel.getImportedName("java.util.Collection"));
    stringBuffer.append(TEXT_328);
    stringBuffer.append(genModel.getImportedName("java.util.ArrayList"));
    stringBuffer.append(TEXT_329);
     } 
     for(GAwareGenFeature feature : tgtFeaturePath.getTailFeatures()) { 
    stringBuffer.append(TEXT_330);
     if(feature.isListType()) { 
    stringBuffer.append(TEXT_331);
    stringBuffer.append(previousFeature.getImportedElementType(genClass));
    stringBuffer.append(TEXT_332);
    stringBuffer.append(feature.getGetAccessor());
    stringBuffer.append(TEXT_333);
     } else { 
    stringBuffer.append(TEXT_334);
    stringBuffer.append(previousFeature.getImportedElementType(genClass));
    stringBuffer.append(TEXT_332);
    stringBuffer.append(feature.getGetAccessor());
    stringBuffer.append(TEXT_335);
     } 
    stringBuffer.append(TEXT_336);
    previousFeature = feature;
     } 
    stringBuffer.append(TEXT_337);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.util.EcoreEList.UnmodifiableEList"));
    stringBuffer.append(TEXT_338);
    stringBuffer.append(genFeature.getTypeGenClassifier().getName());
    stringBuffer.append(TEXT_283);
    stringBuffer.append(gGenFeature.getName());
    stringBuffer.append(TEXT_339);
     } else if(tgtFeaturePath.isJavaMappedType()) { // List => JavaMappedType 
    stringBuffer.append(TEXT_276);
    stringBuffer.append(TEXT_277);
     } else if(tgtFeaturePath.isEnumType()) { // List => Enum 
    stringBuffer.append(TEXT_276);
    stringBuffer.append(TEXT_340);
     } else { // List => EObject
    stringBuffer.append(TEXT_276);
    stringBuffer.append(TEXT_341);
     } 
    stringBuffer.append(TEXT_287);
     } else if(gGenFeature.isJavaMappedType()) { 
    stringBuffer.append(TEXT_222);
     if(tgtFeaturePath.isListType()) { // JavaMappedType => List
    stringBuffer.append(TEXT_276);
    stringBuffer.append(TEXT_288);
     } else if(tgtFeaturePath.isJavaMappedType()) { // JavaMappedType => JavaMappedType 
    stringBuffer.append(TEXT_342);
     GAwareGenFeature previousFeature = null; 
     for(GAwareGenFeature feature : tgtFeaturePath.getHeadFeatures()) { 
    stringBuffer.append(TEXT_126);
    stringBuffer.append(feature.getImportedType(genClass));
    stringBuffer.append(TEXT_65);
    stringBuffer.append(feature.getName());
    stringBuffer.append(TEXT_76);
     if(previousFeature != null) {
    stringBuffer.append( previousFeature.getName() );
    stringBuffer.append(TEXT_166);
    }
    stringBuffer.append( feature.getGetAccessor() );
    stringBuffer.append(TEXT_324);
    stringBuffer.append(feature.getName());
    stringBuffer.append(TEXT_343);
    stringBuffer.append(feature.getName());
    stringBuffer.append(TEXT_76);
    stringBuffer.append(GenModels.getImportedFactoryName(genModel));
    stringBuffer.append(TEXT_344);
    stringBuffer.append(feature.getImportedType(genClass));
    stringBuffer.append(TEXT_345);
     previousFeature = feature; 
     } 
    stringBuffer.append(TEXT_276);
    stringBuffer.append(genModel.getImportedName(tgtFeaturePath.getImportedType(genClass)));
    stringBuffer.append(TEXT_289);
    stringBuffer.append(previousFeature.getName());
    stringBuffer.append(TEXT_166);
    stringBuffer.append(tgtFeaturePath.getLastFeature().getGetAccessor());
    stringBuffer.append(TEXT_346);
     if(gGenFeature.isInteger() && tgtFeaturePath.isLong()) { // Integer => Long 
    stringBuffer.append(TEXT_291);
     } else if(gGenFeature.isInteger() && tgtFeaturePath.isString()) { // Integer => String 
    stringBuffer.append(TEXT_292);
     } else if(gGenFeature.isDouble() && tgtFeaturePath.isString()) { // Double => String 
    stringBuffer.append(TEXT_293);
     } else if(gGenFeature.isBigDecimal() && tgtFeaturePath.isString()) { // BigDecimal => String 
    stringBuffer.append(TEXT_294);
    stringBuffer.append(genModel.getImportedName("java.math.BigDecimal"));
    stringBuffer.append(TEXT_295);
     } else if(gGenFeature.isBigDecimal() && tgtFeaturePath.isInteger()) { // BigDecimal => Integer 
    stringBuffer.append(TEXT_294);
    stringBuffer.append(genModel.getImportedName("java.math.BigDecimal"));
    stringBuffer.append(TEXT_296);
     } else { // Any JavaMappedType => Any JavaMappedType 
    stringBuffer.append(TEXT_297);
     } 
    stringBuffer.append(TEXT_222);
     } else if(tgtFeaturePath.isEnumType()) { // JavaMappedType => Enum 
    stringBuffer.append(TEXT_276);
    stringBuffer.append(TEXT_347);
     } else { // JavaMappedType => EObject
    stringBuffer.append(TEXT_276);
    stringBuffer.append(TEXT_300);
     } 
    stringBuffer.append(TEXT_301);
     } else if(gGenFeature.isEnumType()) { 
    stringBuffer.append(TEXT_222);
     if(tgtFeaturePath.isEnumType()) { // Enum => Enum 
    stringBuffer.append(TEXT_276);
     GAwareGenFeature previousFeature = null; 
     for(GAwareGenFeature feature : tgtFeaturePath.getHeadFeatures()) { 
    stringBuffer.append(TEXT_126);
    stringBuffer.append(feature.getImportedType(genClass));
    stringBuffer.append(TEXT_65);
    stringBuffer.append(feature.getName());
    stringBuffer.append(TEXT_76);
     if(previousFeature != null) {
    stringBuffer.append( previousFeature.getName() );
    stringBuffer.append(TEXT_166);
    }
    stringBuffer.append( feature.getGetAccessor() );
    stringBuffer.append(TEXT_324);
    stringBuffer.append(feature.getName());
    stringBuffer.append(TEXT_343);
    stringBuffer.append(feature.getName());
    stringBuffer.append(TEXT_76);
    stringBuffer.append(GenModels.getImportedFactoryName(genModel));
    stringBuffer.append(TEXT_344);
    stringBuffer.append(feature.getImportedType(genClass));
    stringBuffer.append(TEXT_345);
     previousFeature = feature; 
     } 
    stringBuffer.append(TEXT_302);
    stringBuffer.append(gGenFeature.getImportedType(genClass));
    stringBuffer.append(TEXT_303);
     if(previousFeature != null) {
    stringBuffer.append( previousFeature.getName() );
    stringBuffer.append(TEXT_166);
    }
    stringBuffer.append(tgtFeaturePath.getLastFeature().getGetAccessor());
    stringBuffer.append(TEXT_304);
     } else if(tgtFeaturePath.isListType()) { // Enum => List 
    stringBuffer.append(TEXT_276);
    stringBuffer.append(TEXT_305);
     } else if(tgtFeaturePath.isJavaMappedType()) { // Enum => JavaMappedType 
    stringBuffer.append(TEXT_276);
    stringBuffer.append(TEXT_306);
     } else { // Enum => EObject 
    stringBuffer.append(TEXT_276);
    stringBuffer.append(TEXT_307);
     } 
    stringBuffer.append(TEXT_308);
     } else { 
    stringBuffer.append(TEXT_222);
    if(tgtFeaturePath.isListType()) { // EObject => List 
    stringBuffer.append(TEXT_276);
    stringBuffer.append(TEXT_348);
     } else if(tgtFeaturePath.isJavaMappedType()) { // EObject => JavaMappedType 
    stringBuffer.append(TEXT_276);
    stringBuffer.append(TEXT_314);
     } else if(tgtFeaturePath.isEnumType()) { // EObject => Enum 
    stringBuffer.append(TEXT_276);
    stringBuffer.append(TEXT_315);
     } else { // EObject => EObject 
    stringBuffer.append(TEXT_276);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.common.util.EList"));
    stringBuffer.append(TEXT_320);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.common.util.BasicEList"));
    stringBuffer.append(TEXT_321);
    GAwareGenFeature previousFeature = tgtFeaturePath.getFirstFeature();
     if(previousFeature.isListType()) { 
    stringBuffer.append(TEXT_322);
    stringBuffer.append(previousFeature.getGetAccessor());
    stringBuffer.append(TEXT_323);
     } else { 
    stringBuffer.append(TEXT_126);
    stringBuffer.append(previousFeature.getImportedElementType(genClass));
    stringBuffer.append(TEXT_65);
    stringBuffer.append(previousFeature.getName());
    stringBuffer.append(TEXT_76);
    stringBuffer.append(previousFeature.getGetAccessor());
    stringBuffer.append(TEXT_324);
    stringBuffer.append(previousFeature.getName());
    stringBuffer.append(TEXT_325);
    stringBuffer.append(previousFeature.getGetAccessor());
    stringBuffer.append(TEXT_326);
     } 
    stringBuffer.append(TEXT_327);
     if(!tgtFeaturePath.getTailFeatures().isEmpty()) { 
    stringBuffer.append(TEXT_126);
    stringBuffer.append(genModel.getImportedName("java.util.Collection"));
    stringBuffer.append(TEXT_328);
    stringBuffer.append(genModel.getImportedName("java.util.ArrayList"));
    stringBuffer.append(TEXT_329);
     } 
     for(GAwareGenFeature feature : tgtFeaturePath.getTailFeatures()) { 
    stringBuffer.append(TEXT_330);
     if(feature.isListType()) { 
    stringBuffer.append(TEXT_331);
    stringBuffer.append(previousFeature.getImportedElementType(genClass));
    stringBuffer.append(TEXT_332);
    stringBuffer.append(feature.getGetAccessor());
    stringBuffer.append(TEXT_333);
     } else { 
    stringBuffer.append(TEXT_334);
    stringBuffer.append(previousFeature.getImportedElementType(genClass));
    stringBuffer.append(TEXT_332);
    stringBuffer.append(feature.getGetAccessor());
    stringBuffer.append(TEXT_335);
     } 
    stringBuffer.append(TEXT_336);
    previousFeature = feature;
     } 
    stringBuffer.append(TEXT_349);
    stringBuffer.append(gGenFeature.getEcoreFeature().getEType().getName());
    stringBuffer.append(TEXT_350);
     } 
    stringBuffer.append(TEXT_209);
     } 
    stringBuffer.append(TEXT_116);
    } else { 
    stringBuffer.append(TEXT_351);
     } 
    stringBuffer.append(TEXT_352);
     } 
    } else{
    stringBuffer.append(TEXT_353);
    stringBuffer.append(gGenFeature.getFormattedName());
    stringBuffer.append(TEXT_354);
    stringBuffer.append(gGenFeature.getFeatureKind());
    stringBuffer.append(TEXT_355);
    if (gGenFeature.isListType()) {
    stringBuffer.append(TEXT_356);
    if (gGenFeature.isMapType()) {
    stringBuffer.append(TEXT_357);
    } else if (gGenFeature.isFeatureMapType()) {
    stringBuffer.append(TEXT_358);
    } else {
    stringBuffer.append(TEXT_359);
    }
    stringBuffer.append(TEXT_360);
    }
    stringBuffer.append(TEXT_361);
    }
    stringBuffer.append(TEXT_362);
    //Class/getGenFeature.todo.override.javajetinc
    }
    }
    stringBuffer.append(TEXT_151);
    }
    //Class/getGenFeature.override.javajetinc
    }
    if (isImplementation && !genModel.isReflectiveDelegation() && genFeature.isBasicGet()) {
    stringBuffer.append(TEXT_124);
    if (isJDK50) { //Class/basicGetGenFeature.annotations.insert.javajetinc
    }
    stringBuffer.append(TEXT_128);
    stringBuffer.append(genFeature.getImportedType(genClass));
    stringBuffer.append(TEXT_363);
    stringBuffer.append(genFeature.getAccessorName());
    stringBuffer.append(TEXT_129);
    if (genModel.isDynamicDelegation()) {
    stringBuffer.append(TEXT_246);
    stringBuffer.append(genFeature.getImportedType(genClass));
    stringBuffer.append(TEXT_196);
    stringBuffer.append(genClass.getQualifiedFeatureID(genFeature));
    stringBuffer.append(TEXT_197);
    stringBuffer.append(genFeature.getQualifiedFeatureAccessor());
    stringBuffer.append(TEXT_364);
    stringBuffer.append(!genFeature.isEffectiveSuppressEMFTypes());
    stringBuffer.append(TEXT_205);
    } else if (genFeature.hasSettingDelegate()) {
    stringBuffer.append(TEXT_147);
    if (!isJDK50 && genFeature.isPrimitiveType()) {
    stringBuffer.append(TEXT_91);
    }
    stringBuffer.append(TEXT_91);
    stringBuffer.append(genFeature.getObjectType(genClass));
    stringBuffer.append(TEXT_85);
    stringBuffer.append(genFeature.getUpperName());
    stringBuffer.append(TEXT_365);
    if (!isJDK50 && genFeature.isPrimitiveType()) {
    stringBuffer.append(TEXT_199);
    stringBuffer.append(genFeature.getPrimitiveValueFunction());
    stringBuffer.append(TEXT_23);
    }
    stringBuffer.append(TEXT_7);
    } else if (genFeature.isContainer()) {
    stringBuffer.append(TEXT_211);
    stringBuffer.append(genClass.getQualifiedFeatureID(genFeature));
    stringBuffer.append(positiveOffsetCorrection);
    stringBuffer.append(TEXT_212);
    stringBuffer.append(genFeature.getImportedType(genClass));
    stringBuffer.append(TEXT_366);
    } else if (!genFeature.isVolatile()) {
    if (genModel.isVirtualDelegation()) {
    stringBuffer.append(TEXT_246);
    stringBuffer.append(genFeature.getImportedType(genClass));
    stringBuffer.append(TEXT_204);
    stringBuffer.append(genClass.getQualifiedFeatureID(genFeature));
    stringBuffer.append(positiveOffsetCorrection);
    stringBuffer.append(TEXT_205);
    } else {
    stringBuffer.append(TEXT_147);
    stringBuffer.append(genFeature.getSafeName());
    stringBuffer.append(TEXT_7);
    }
    } else if (genFeature.hasDelegateFeature()) { GenFeature delegateFeature = genFeature.getDelegateFeature();
    if (delegateFeature.isWrappedFeatureMapType()) {
    stringBuffer.append(TEXT_246);
    stringBuffer.append(genFeature.getImportedType(genClass));
    stringBuffer.append(TEXT_367);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.util.FeatureMap"));
    stringBuffer.append(TEXT_260);
    stringBuffer.append(delegateFeature.getAccessorName());
    stringBuffer.append(TEXT_269);
    stringBuffer.append(genFeature.getQualifiedFeatureAccessor());
    stringBuffer.append(TEXT_368);
    } else {
    stringBuffer.append(TEXT_246);
    stringBuffer.append(genFeature.getImportedType(genClass));
    stringBuffer.append(TEXT_263);
    stringBuffer.append(delegateFeature.getAccessorName());
    stringBuffer.append(TEXT_271);
    stringBuffer.append(genFeature.getQualifiedFeatureAccessor());
    stringBuffer.append(TEXT_368);
    }
    } else {
    stringBuffer.append(TEXT_353);
    stringBuffer.append(genFeature.getFormattedName());
    stringBuffer.append(TEXT_354);
    stringBuffer.append(genFeature.getFeatureKind());
    stringBuffer.append(TEXT_369);
    //Class/basicGetGenFeature.todo.override.javajetinc
    }
    stringBuffer.append(TEXT_151);
    //Class/basicGetGenFeature.override.javajetinc
    }
    if (isImplementation && !genModel.isReflectiveDelegation() && genFeature.isBasicSet()) {
    stringBuffer.append(TEXT_124);
    if (isJDK50) { //Class/basicSetGenFeature.annotations.insert.javajetinc
    }
    stringBuffer.append(TEXT_128);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.common.notify.NotificationChain"));
    stringBuffer.append(TEXT_370);
    stringBuffer.append(genFeature.getAccessorName());
    stringBuffer.append(TEXT_91);
    stringBuffer.append(genFeature.getImportedInternalType(genClass));
    stringBuffer.append(TEXT_223);
    stringBuffer.append(genFeature.getCapName());
    stringBuffer.append(TEXT_197);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.common.notify.NotificationChain"));
    stringBuffer.append(TEXT_371);
    if (genFeature.isContainer()) {
    stringBuffer.append(TEXT_372);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.InternalEObject"));
    stringBuffer.append(TEXT_373);
    stringBuffer.append(genFeature.getCapName());
    stringBuffer.append(TEXT_197);
    stringBuffer.append(genClass.getQualifiedFeatureID(genFeature));
    stringBuffer.append(positiveOffsetCorrection);
    stringBuffer.append(TEXT_374);
    stringBuffer.append(TEXT_375);
    } else if (genModel.isDynamicDelegation()) {
    stringBuffer.append(TEXT_376);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.InternalEObject"));
    stringBuffer.append(TEXT_373);
    stringBuffer.append(genFeature.getCapName());
    stringBuffer.append(TEXT_197);
    stringBuffer.append(genClass.getQualifiedFeatureID(genFeature));
    stringBuffer.append(positiveOffsetCorrection);
    stringBuffer.append(TEXT_374);
    stringBuffer.append(TEXT_375);
    } else if (!genFeature.isVolatile()) {
    if (genModel.isVirtualDelegation()) {
    stringBuffer.append(TEXT_377);
    stringBuffer.append(genFeature.getCapName());
    stringBuffer.append(TEXT_378);
    stringBuffer.append(genClass.getQualifiedFeatureID(genFeature));
    stringBuffer.append(positiveOffsetCorrection);
    stringBuffer.append(TEXT_379);
    stringBuffer.append(genFeature.getCapName());
    stringBuffer.append(TEXT_205);
    } else {
    stringBuffer.append(TEXT_116);
    stringBuffer.append(genFeature.getImportedType(genClass));
    stringBuffer.append(TEXT_216);
    stringBuffer.append(genFeature.getCapName());
    stringBuffer.append(TEXT_76);
    stringBuffer.append(genFeature.getSafeName());
    stringBuffer.append(TEXT_380);
    stringBuffer.append(genFeature.getSafeName());
    stringBuffer.append(TEXT_381);
    stringBuffer.append(genFeature.getCapName());
    stringBuffer.append(TEXT_7);
    }
    if (genFeature.isUnsettable()) {
    if (genModel.isVirtualDelegation()) {
    if (!genModel.isSuppressNotification()) {
    stringBuffer.append(TEXT_382);
    stringBuffer.append(genFeature.getCapName());
    stringBuffer.append(TEXT_383);
    }
    } else if (genClass.isESetFlag(genFeature)) {
    stringBuffer.append(TEXT_384);
    stringBuffer.append(genFeature.getCapName());
    stringBuffer.append(TEXT_385);
    stringBuffer.append(genClass.getESetFlagsField(genFeature));
    stringBuffer.append(TEXT_247);
    stringBuffer.append(genFeature.getUpperName());
    stringBuffer.append(TEXT_386);
    if (!genModel.isSuppressNotification()) {
    stringBuffer.append(TEXT_116);
    stringBuffer.append(genClass.getESetFlagsField(genFeature));
    stringBuffer.append(TEXT_117);
    stringBuffer.append(genFeature.getUpperName());
    stringBuffer.append(TEXT_387);
    }
    } else {
    if (!genModel.isSuppressNotification()) {
    stringBuffer.append(TEXT_384);
    stringBuffer.append(genFeature.getCapName());
    stringBuffer.append(TEXT_388);
    stringBuffer.append(genFeature.getUncapName());
    stringBuffer.append(TEXT_389);
    }
    stringBuffer.append(TEXT_116);
    stringBuffer.append(genFeature.getUncapName());
    stringBuffer.append(TEXT_390);
    }
    }
    if (!genModel.isSuppressNotification()) {
    stringBuffer.append(TEXT_391);
    if (genFeature.isUnsettable()) {
    stringBuffer.append(TEXT_209);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.impl.ENotificationImpl"));
    stringBuffer.append(TEXT_392);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.impl.ENotificationImpl"));
    stringBuffer.append(TEXT_241);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.common.notify.Notification"));
    stringBuffer.append(TEXT_393);
    stringBuffer.append(genClass.getQualifiedFeatureID(genFeature));
    stringBuffer.append(positiveOffsetCorrection);
    stringBuffer.append(TEXT_197);
    if (genModel.isVirtualDelegation()) {
    stringBuffer.append(TEXT_394);
    stringBuffer.append(genFeature.getCapName());
    } else {
    stringBuffer.append(TEXT_395);
    stringBuffer.append(genFeature.getCapName());
    }
    stringBuffer.append(TEXT_379);
    stringBuffer.append(genFeature.getCapName());
    stringBuffer.append(TEXT_197);
    if (genModel.isVirtualDelegation()) {
    stringBuffer.append(TEXT_396);
    } else {
    stringBuffer.append(TEXT_397);
    stringBuffer.append(genFeature.getCapName());
    stringBuffer.append(TEXT_398);
    }
    stringBuffer.append(TEXT_205);
    } else {
    stringBuffer.append(TEXT_209);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.impl.ENotificationImpl"));
    stringBuffer.append(TEXT_392);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.impl.ENotificationImpl"));
    stringBuffer.append(TEXT_241);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.common.notify.Notification"));
    stringBuffer.append(TEXT_393);
    stringBuffer.append(genClass.getQualifiedFeatureID(genFeature));
    stringBuffer.append(positiveOffsetCorrection);
    stringBuffer.append(TEXT_197);
    if (genModel.isVirtualDelegation()) {
    stringBuffer.append(TEXT_395);
    stringBuffer.append(genFeature.getCapName());
    stringBuffer.append(TEXT_399);
    stringBuffer.append(genFeature.getCapName());
    } else {
    stringBuffer.append(TEXT_395);
    stringBuffer.append(genFeature.getCapName());
    }
    stringBuffer.append(TEXT_379);
    stringBuffer.append(genFeature.getCapName());
    stringBuffer.append(TEXT_205);
    }
    stringBuffer.append(TEXT_400);
    }
    stringBuffer.append(TEXT_375);
    } else if (genFeature.hasDelegateFeature()) { GenFeature delegateFeature = genFeature.getDelegateFeature();
    if (delegateFeature.isWrappedFeatureMapType()) {
    stringBuffer.append(TEXT_265);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.util.FeatureMap"));
    stringBuffer.append(TEXT_259);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.util.FeatureMap"));
    stringBuffer.append(TEXT_260);
    stringBuffer.append(delegateFeature.getAccessorName());
    stringBuffer.append(TEXT_401);
    stringBuffer.append(genFeature.getQualifiedFeatureAccessor());
    stringBuffer.append(TEXT_379);
    stringBuffer.append(genFeature.getCapName());
    stringBuffer.append(TEXT_374);
    } else {
    stringBuffer.append(TEXT_265);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.util.FeatureMap"));
    stringBuffer.append(TEXT_402);
    stringBuffer.append(delegateFeature.getAccessorName());
    stringBuffer.append(TEXT_403);
    stringBuffer.append(genFeature.getQualifiedFeatureAccessor());
    stringBuffer.append(TEXT_379);
    stringBuffer.append(genFeature.getCapName());
    stringBuffer.append(TEXT_374);
    }
    } else {
    stringBuffer.append(TEXT_404);
    stringBuffer.append(genFeature.getFormattedName());
    stringBuffer.append(TEXT_354);
    stringBuffer.append(genFeature.getFeatureKind());
    stringBuffer.append(TEXT_405);
    //Class/basicSetGenFeature.todo.override.javajetinc
    }
    stringBuffer.append(TEXT_151);
    //Class/basicSetGenFeature.override.javajetinc
    }
    if (genFeature.isSet() && (isImplementation || !genFeature.isSuppressedSetVisibility())) {
    if (isInterface) { 
    stringBuffer.append(TEXT_406);
    stringBuffer.append(genClass.getQualifiedInterfaceName());
    stringBuffer.append(TEXT_17);
    stringBuffer.append(genFeature.getGetAccessor());
    stringBuffer.append(TEXT_18);
    stringBuffer.append(genFeature.getFormattedName());
    stringBuffer.append(TEXT_56);
    stringBuffer.append(genFeature.getFeatureKind());
    stringBuffer.append(TEXT_166);
    stringBuffer.append(TEXT_407);
    stringBuffer.append(genFeature.getFormattedName());
    stringBuffer.append(TEXT_182);
    stringBuffer.append(genFeature.getFeatureKind());
    stringBuffer.append(TEXT_166);
    if (genFeature.isEnumType()) {
    stringBuffer.append(TEXT_188);
    stringBuffer.append(genFeature.getTypeGenEnum().getQualifiedName());
    }
    if (genFeature.isUnsettable()) {
    if (!genFeature.isSuppressedIsSetVisibility()) {
    stringBuffer.append(TEXT_189);
    stringBuffer.append(((GAwareGenFeature)genFeature).getIsSetAccessorName());
    stringBuffer.append(TEXT_23);
    }
    if (!genFeature.isSuppressedUnsetVisibility()) {
    stringBuffer.append(TEXT_189);
    stringBuffer.append(((GAwareGenFeature)genFeature).getAccessorName());
    stringBuffer.append(TEXT_23);
    }
    }
    stringBuffer.append(TEXT_189);
    stringBuffer.append(genFeature.getGetAccessor());
    stringBuffer.append(TEXT_408);
    //Class/setGenFeature.javadoc.override.javajetinc
    } else {
    stringBuffer.append(TEXT_124);
    if (isJDK50) { //Class/setGenFeature.annotations.insert.javajetinc
    }
    }
    if (!isImplementation) { 
    stringBuffer.append(TEXT_152);
    stringBuffer.append(((GAwareGenFeature)genFeature).getSetAccessorName());
    stringBuffer.append(TEXT_91);
    stringBuffer.append(genFeature.getImportedType(genClass));
    stringBuffer.append(TEXT_409);
    } else { GenOperation setAccessorOperation = genClass.getSetAccessorOperation(genFeature);
    stringBuffer.append(TEXT_155);
    stringBuffer.append(((GAwareGenFeature)genFeature).getSetAccessorName());
    if (genClass.hasCollidingSetAccessorOperation(genFeature)) {
    stringBuffer.append(TEXT_195);
    }
    stringBuffer.append(TEXT_91);
    stringBuffer.append(genFeature.getImportedType(genClass));
    stringBuffer.append(TEXT_65);
    stringBuffer.append(setAccessorOperation == null ? "new" + genFeature.getCapName() : setAccessorOperation.getGenParameters().get(0).getName());
    stringBuffer.append(TEXT_410);
    if (genModel.isDynamicDelegation()) {
    stringBuffer.append(TEXT_411);
    stringBuffer.append(genClass.getQualifiedFeatureID(genFeature));
    stringBuffer.append(TEXT_197);
    stringBuffer.append(genFeature.getQualifiedFeatureAccessor());
    stringBuffer.append(TEXT_197);
    if (!isJDK50 && genFeature.isPrimitiveType()) {
    stringBuffer.append(TEXT_412);
    stringBuffer.append(genFeature.getObjectType(genClass));
    stringBuffer.append(TEXT_91);
    }
    stringBuffer.append(TEXT_413);
    stringBuffer.append(genFeature.getCapName());
    if (!isJDK50 && genFeature.isPrimitiveType()) {
    stringBuffer.append(TEXT_85);
    }
    stringBuffer.append(TEXT_205);
    } else if (genModel.isReflectiveDelegation()) {
    stringBuffer.append(TEXT_414);
    stringBuffer.append(genFeature.getQualifiedFeatureAccessor());
    stringBuffer.append(TEXT_197);
    if (!isJDK50 && genFeature.isPrimitiveType()) {
    stringBuffer.append(TEXT_412);
    stringBuffer.append(genFeature.getObjectType(genClass));
    stringBuffer.append(TEXT_91);
    }
    stringBuffer.append(TEXT_413);
    stringBuffer.append(genFeature.getCapName());
    if (!isJDK50 && genFeature.isPrimitiveType()) {
    stringBuffer.append(TEXT_85);
    }
    stringBuffer.append(TEXT_205);
    } else if (genFeature.hasSettingDelegate()) {
    stringBuffer.append(TEXT_116);
    stringBuffer.append(genFeature.getUpperName());
    stringBuffer.append(TEXT_415);
    if (!isJDK50 && genFeature.isPrimitiveType()) {
    stringBuffer.append(TEXT_412);
    stringBuffer.append(genFeature.getObjectType(genClass));
    stringBuffer.append(TEXT_91);
    }
    stringBuffer.append(TEXT_413);
    stringBuffer.append(genFeature.getCapName());
    if (!isJDK50 && genFeature.isPrimitiveType()) {
    stringBuffer.append(TEXT_85);
    }
    stringBuffer.append(TEXT_205);
    } else if (!genFeature.isVolatile()) {
    if (genFeature.isContainer()) { GenFeature reverseFeature = genFeature.getReverse(); GenClass targetClass = reverseFeature.getGenClass(); String reverseOffsetCorrection = targetClass.hasOffsetCorrection() ? " + " + genClass.getOffsetCorrectionField(genFeature) : "";
    stringBuffer.append(TEXT_416);
    stringBuffer.append(genFeature.getCapName());
    stringBuffer.append(TEXT_417);
    stringBuffer.append(genClass.getQualifiedFeatureID(genFeature));
    stringBuffer.append(positiveOffsetCorrection);
    stringBuffer.append(TEXT_418);
    stringBuffer.append(genFeature.getCapName());
    stringBuffer.append(TEXT_419);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.util.EcoreUtil"));
    stringBuffer.append(TEXT_420);
    stringBuffer.append(genFeature.getEObjectCast());
    stringBuffer.append(TEXT_413);
    stringBuffer.append(genFeature.getCapName());
    stringBuffer.append(TEXT_421);
    stringBuffer.append(genModel.getImportedName("java.lang.IllegalArgumentException"));
    stringBuffer.append(TEXT_422);
    stringBuffer.append(genModel.getNonNLS());
    stringBuffer.append(TEXT_209);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.common.notify.NotificationChain"));
    stringBuffer.append(TEXT_423);
    stringBuffer.append(genFeature.getCapName());
    stringBuffer.append(TEXT_424);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.InternalEObject"));
    stringBuffer.append(TEXT_373);
    stringBuffer.append(genFeature.getCapName());
    stringBuffer.append(TEXT_425);
    stringBuffer.append(targetClass.getQualifiedFeatureID(reverseFeature));
    stringBuffer.append(reverseOffsetCorrection);
    stringBuffer.append(TEXT_197);
    stringBuffer.append(targetClass.getRawImportedInterfaceName());
    stringBuffer.append(TEXT_426);
    stringBuffer.append(genFeature.getAccessorName());
    stringBuffer.append(TEXT_91);
    stringBuffer.append(genFeature.getInternalTypeCast());
    stringBuffer.append(TEXT_413);
    stringBuffer.append(genFeature.getCapName());
    stringBuffer.append(TEXT_427);
    if (!genModel.isSuppressNotification()) {
    stringBuffer.append(TEXT_428);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.impl.ENotificationImpl"));
    stringBuffer.append(TEXT_241);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.common.notify.Notification"));
    stringBuffer.append(TEXT_393);
    stringBuffer.append(genClass.getQualifiedFeatureID(genFeature));
    stringBuffer.append(positiveOffsetCorrection);
    stringBuffer.append(TEXT_379);
    stringBuffer.append(genFeature.getCapName());
    stringBuffer.append(TEXT_379);
    stringBuffer.append(genFeature.getCapName());
    stringBuffer.append(TEXT_244);
    }
    } else if (genFeature.isBidirectional() || genFeature.isEffectiveContains()) {
    if (genModel.isVirtualDelegation()) {
    stringBuffer.append(TEXT_116);
    stringBuffer.append(genFeature.getImportedType(genClass));
    stringBuffer.append(TEXT_65);
    stringBuffer.append(genFeature.getSafeName());
    stringBuffer.append(TEXT_203);
    stringBuffer.append(genFeature.getImportedType(genClass));
    stringBuffer.append(TEXT_204);
    stringBuffer.append(genClass.getQualifiedFeatureID(genFeature));
    stringBuffer.append(positiveOffsetCorrection);
    stringBuffer.append(TEXT_205);
    }
    stringBuffer.append(TEXT_416);
    stringBuffer.append(genFeature.getCapName());
    stringBuffer.append(TEXT_429);
    stringBuffer.append(genFeature.getSafeName());
    stringBuffer.append(TEXT_430);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.common.notify.NotificationChain"));
    stringBuffer.append(TEXT_431);
    stringBuffer.append(genFeature.getSafeName());
    stringBuffer.append(TEXT_432);
    if (!genFeature.isBidirectional()) {
    stringBuffer.append(TEXT_433);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.InternalEObject"));
    stringBuffer.append(TEXT_85);
    stringBuffer.append(genFeature.getSafeName());
    stringBuffer.append(TEXT_434);
    stringBuffer.append(genClass.getQualifiedFeatureID(genFeature));
    stringBuffer.append(negativeOffsetCorrection);
    stringBuffer.append(TEXT_435);
    stringBuffer.append(genFeature.getCapName());
    stringBuffer.append(TEXT_424);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.InternalEObject"));
    stringBuffer.append(TEXT_373);
    stringBuffer.append(genFeature.getCapName());
    stringBuffer.append(TEXT_436);
    stringBuffer.append(genClass.getQualifiedFeatureID(genFeature));
    stringBuffer.append(negativeOffsetCorrection);
    stringBuffer.append(TEXT_234);
    } else { GenFeature reverseFeature = genFeature.getReverse(); GenClass targetClass = reverseFeature.getGenClass(); String reverseOffsetCorrection = targetClass.hasOffsetCorrection() ? " + " + genClass.getOffsetCorrectionField(genFeature) : "";
    stringBuffer.append(TEXT_433);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.InternalEObject"));
    stringBuffer.append(TEXT_85);
    stringBuffer.append(genFeature.getSafeName());
    stringBuffer.append(TEXT_437);
    stringBuffer.append(targetClass.getQualifiedFeatureID(reverseFeature));
    stringBuffer.append(reverseOffsetCorrection);
    stringBuffer.append(TEXT_197);
    stringBuffer.append(targetClass.getRawImportedInterfaceName());
    stringBuffer.append(TEXT_438);
    stringBuffer.append(genFeature.getCapName());
    stringBuffer.append(TEXT_424);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.InternalEObject"));
    stringBuffer.append(TEXT_373);
    stringBuffer.append(genFeature.getCapName());
    stringBuffer.append(TEXT_425);
    stringBuffer.append(targetClass.getQualifiedFeatureID(reverseFeature));
    stringBuffer.append(reverseOffsetCorrection);
    stringBuffer.append(TEXT_197);
    stringBuffer.append(targetClass.getRawImportedInterfaceName());
    stringBuffer.append(TEXT_237);
    }
    stringBuffer.append(TEXT_439);
    stringBuffer.append(genFeature.getAccessorName());
    stringBuffer.append(TEXT_91);
    stringBuffer.append(genFeature.getInternalTypeCast());
    stringBuffer.append(TEXT_413);
    stringBuffer.append(genFeature.getCapName());
    stringBuffer.append(TEXT_427);
    if (genFeature.isUnsettable()) {
    stringBuffer.append(TEXT_440);
    if (genModel.isVirtualDelegation()) {
    stringBuffer.append(TEXT_441);
    stringBuffer.append(genFeature.getCapName());
    stringBuffer.append(TEXT_442);
    stringBuffer.append(genClass.getQualifiedFeatureID(genFeature));
    stringBuffer.append(positiveOffsetCorrection);
    stringBuffer.append(TEXT_205);
    } else if (genClass.isESetFlag(genFeature)) {
    if (!genModel.isSuppressNotification()) {
    stringBuffer.append(TEXT_441);
    stringBuffer.append(genFeature.getCapName());
    stringBuffer.append(TEXT_385);
    stringBuffer.append(genClass.getESetFlagsField(genFeature));
    stringBuffer.append(TEXT_247);
    stringBuffer.append(genFeature.getUpperName());
    stringBuffer.append(TEXT_386);
    }
    stringBuffer.append(TEXT_209);
    stringBuffer.append(genClass.getESetFlagsField(genFeature));
    stringBuffer.append(TEXT_117);
    stringBuffer.append(genFeature.getUpperName());
    stringBuffer.append(TEXT_387);
    } else {
    if (!genModel.isSuppressNotification()) {
    stringBuffer.append(TEXT_441);
    stringBuffer.append(genFeature.getCapName());
    stringBuffer.append(TEXT_388);
    stringBuffer.append(genFeature.getUncapName());
    stringBuffer.append(TEXT_389);
    }
    stringBuffer.append(TEXT_209);
    stringBuffer.append(genFeature.getUncapName());
    stringBuffer.append(TEXT_390);
    }
    if (!genModel.isSuppressNotification()) {
    stringBuffer.append(TEXT_443);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.impl.ENotificationImpl"));
    stringBuffer.append(TEXT_241);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.common.notify.Notification"));
    stringBuffer.append(TEXT_393);
    stringBuffer.append(genClass.getQualifiedFeatureID(genFeature));
    stringBuffer.append(positiveOffsetCorrection);
    stringBuffer.append(TEXT_379);
    stringBuffer.append(genFeature.getCapName());
    stringBuffer.append(TEXT_379);
    stringBuffer.append(genFeature.getCapName());
    stringBuffer.append(TEXT_444);
    stringBuffer.append(genFeature.getCapName());
    stringBuffer.append(TEXT_445);
    }
    stringBuffer.append(TEXT_446);
    } else {
    if (!genModel.isSuppressNotification()) {
    stringBuffer.append(TEXT_428);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.impl.ENotificationImpl"));
    stringBuffer.append(TEXT_241);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.common.notify.Notification"));
    stringBuffer.append(TEXT_393);
    stringBuffer.append(genClass.getQualifiedFeatureID(genFeature));
    stringBuffer.append(positiveOffsetCorrection);
    stringBuffer.append(TEXT_379);
    stringBuffer.append(genFeature.getCapName());
    stringBuffer.append(TEXT_379);
    stringBuffer.append(genFeature.getCapName());
    stringBuffer.append(TEXT_244);
    }
    }
    } else {
    if (genClass.isFlag(genFeature)) {
    if (!genModel.isSuppressNotification()) {
    if (genFeature.isBooleanType()) {
    stringBuffer.append(TEXT_116);
    stringBuffer.append(genFeature.getImportedType(genClass));
    stringBuffer.append(TEXT_216);
    stringBuffer.append(genFeature.getCapName());
    stringBuffer.append(TEXT_203);
    stringBuffer.append(genClass.getFlagsField(genFeature));
    stringBuffer.append(TEXT_247);
    stringBuffer.append(genFeature.getUpperName());
    stringBuffer.append(TEXT_248);
    } else {
    stringBuffer.append(TEXT_116);
    stringBuffer.append(genFeature.getImportedType(genClass));
    stringBuffer.append(TEXT_216);
    stringBuffer.append(genFeature.getCapName());
    stringBuffer.append(TEXT_76);
    stringBuffer.append(genFeature.getUpperName());
    stringBuffer.append(TEXT_249);
    stringBuffer.append(genClass.getFlagsField(genFeature));
    stringBuffer.append(TEXT_247);
    stringBuffer.append(genFeature.getUpperName());
    stringBuffer.append(TEXT_250);
    stringBuffer.append(genFeature.getUpperName());
    stringBuffer.append(TEXT_251);
    }
    }
    if (genFeature.isBooleanType()) {
    stringBuffer.append(TEXT_416);
    stringBuffer.append(genFeature.getCapName());
    stringBuffer.append(TEXT_447);
    stringBuffer.append(genClass.getFlagsField(genFeature));
    stringBuffer.append(TEXT_117);
    stringBuffer.append(genFeature.getUpperName());
    stringBuffer.append(TEXT_448);
    stringBuffer.append(genClass.getFlagsField(genFeature));
    stringBuffer.append(TEXT_449);
    stringBuffer.append(genFeature.getUpperName());
    stringBuffer.append(TEXT_450);
    } else {
    stringBuffer.append(TEXT_416);
    stringBuffer.append(genFeature.getCapName());
    stringBuffer.append(TEXT_451);
    stringBuffer.append(genFeature.getCapName());
    stringBuffer.append(TEXT_76);
    stringBuffer.append(genFeature.getUpperName());
    stringBuffer.append(TEXT_452);
    stringBuffer.append(genClass.getFlagsField(genFeature));
    stringBuffer.append(TEXT_76);
    stringBuffer.append(genClass.getFlagsField(genFeature));
    stringBuffer.append(TEXT_453);
    stringBuffer.append(genFeature.getUpperName());
    stringBuffer.append(TEXT_454);
    if (isJDK50) {
    stringBuffer.append(TEXT_413);
    stringBuffer.append(genFeature.getCapName());
    stringBuffer.append(TEXT_83);
    } else {
    stringBuffer.append(genFeature.getImportedType(genClass));
    stringBuffer.append(TEXT_455);
    stringBuffer.append(genFeature.getCapName());
    stringBuffer.append(TEXT_85);
    }
    stringBuffer.append(TEXT_86);
    stringBuffer.append(genFeature.getUpperName());
    stringBuffer.append(TEXT_456);
    }
    } else {
    if (!genModel.isVirtualDelegation() || genFeature.isPrimitiveType()) {
    if (!genModel.isSuppressNotification()) {
    stringBuffer.append(TEXT_116);
    stringBuffer.append(genFeature.getImportedType(genClass));
    stringBuffer.append(TEXT_216);
    stringBuffer.append(genFeature.getCapName());
    stringBuffer.append(TEXT_76);
    stringBuffer.append(genFeature.getSafeName());
    stringBuffer.append(TEXT_7);
    }
    }
    if (genFeature.isEnumType()) {
    if (genModel.isVirtualDelegation()) {
    stringBuffer.append(TEXT_116);
    stringBuffer.append(genFeature.getImportedType(genClass));
    stringBuffer.append(TEXT_65);
    stringBuffer.append(genFeature.getSafeName());
    stringBuffer.append(TEXT_381);
    stringBuffer.append(genFeature.getCapName());
    stringBuffer.append(TEXT_457);
    stringBuffer.append(genFeature.getEDefault());
    stringBuffer.append(TEXT_458);
    stringBuffer.append(genFeature.getCapName());
    stringBuffer.append(TEXT_7);
    } else {
    stringBuffer.append(TEXT_116);
    stringBuffer.append(genFeature.getSafeName());
    stringBuffer.append(TEXT_381);
    stringBuffer.append(genFeature.getCapName());
    stringBuffer.append(TEXT_457);
    stringBuffer.append(genFeature.getEDefault());
    stringBuffer.append(TEXT_458);
    stringBuffer.append(genFeature.getCapName());
    stringBuffer.append(TEXT_7);
    }
    } else {
    if (genModel.isVirtualDelegation() && !genFeature.isPrimitiveType()) {
    stringBuffer.append(TEXT_116);
    stringBuffer.append(genFeature.getImportedType(genClass));
    stringBuffer.append(TEXT_65);
    stringBuffer.append(genFeature.getSafeName());
    stringBuffer.append(TEXT_76);
    stringBuffer.append(genFeature.getInternalTypeCast());
    stringBuffer.append(TEXT_413);
    stringBuffer.append(genFeature.getCapName());
    stringBuffer.append(TEXT_7);
    } else {
    stringBuffer.append(TEXT_116);
    stringBuffer.append(genFeature.getSafeName());
    stringBuffer.append(TEXT_76);
    stringBuffer.append(genFeature.getInternalTypeCast());
    stringBuffer.append(TEXT_413);
    stringBuffer.append(genFeature.getCapName());
    stringBuffer.append(TEXT_7);
    }
    }
    if (genModel.isVirtualDelegation() && !genFeature.isPrimitiveType()) {
    stringBuffer.append(TEXT_377);
    stringBuffer.append(genFeature.getCapName());
    stringBuffer.append(TEXT_378);
    stringBuffer.append(genClass.getQualifiedFeatureID(genFeature));
    stringBuffer.append(positiveOffsetCorrection);
    stringBuffer.append(TEXT_197);
    stringBuffer.append(genFeature.getSafeName());
    stringBuffer.append(TEXT_205);
    }
    }
    if (genFeature.isUnsettable()) {
    if (genModel.isVirtualDelegation() && !genFeature.isPrimitiveType()) {
    stringBuffer.append(TEXT_382);
    stringBuffer.append(genFeature.getCapName());
    stringBuffer.append(TEXT_383);
    } else if (genClass.isESetFlag(genFeature)) {
    if (!genModel.isSuppressNotification()) {
    stringBuffer.append(TEXT_384);
    stringBuffer.append(genFeature.getCapName());
    stringBuffer.append(TEXT_385);
    stringBuffer.append(genClass.getESetFlagsField(genFeature));
    stringBuffer.append(TEXT_247);
    stringBuffer.append(genFeature.getUpperName());
    stringBuffer.append(TEXT_386);
    }
    stringBuffer.append(TEXT_116);
    stringBuffer.append(genClass.getESetFlagsField(genFeature));
    stringBuffer.append(TEXT_117);
    stringBuffer.append(genFeature.getUpperName());
    stringBuffer.append(TEXT_387);
    } else {
    if (!genModel.isSuppressNotification()) {
    stringBuffer.append(TEXT_384);
    stringBuffer.append(genFeature.getCapName());
    stringBuffer.append(TEXT_388);
    stringBuffer.append(genFeature.getUncapName());
    stringBuffer.append(TEXT_389);
    }
    stringBuffer.append(TEXT_116);
    stringBuffer.append(genFeature.getUncapName());
    stringBuffer.append(TEXT_390);
    }
    if (!genModel.isSuppressNotification()) {
    stringBuffer.append(TEXT_459);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.impl.ENotificationImpl"));
    stringBuffer.append(TEXT_241);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.common.notify.Notification"));
    stringBuffer.append(TEXT_393);
    stringBuffer.append(genClass.getQualifiedFeatureID(genFeature));
    stringBuffer.append(positiveOffsetCorrection);
    stringBuffer.append(TEXT_197);
    if (genModel.isVirtualDelegation() && !genFeature.isPrimitiveType()) {
    stringBuffer.append(TEXT_460);
    stringBuffer.append(genFeature.getEDefault());
    stringBuffer.append(TEXT_461);
    stringBuffer.append(genFeature.getCapName());
    } else {
    stringBuffer.append(TEXT_395);
    stringBuffer.append(genFeature.getCapName());
    }
    stringBuffer.append(TEXT_197);
    if (genClass.isFlag(genFeature)) {
    stringBuffer.append(TEXT_413);
    stringBuffer.append(genFeature.getCapName());
    } else {
    stringBuffer.append(genFeature.getSafeName());
    }
    stringBuffer.append(TEXT_197);
    if (genModel.isVirtualDelegation() && !genFeature.isPrimitiveType()) {
    stringBuffer.append(TEXT_396);
    } else {
    stringBuffer.append(TEXT_397);
    stringBuffer.append(genFeature.getCapName());
    stringBuffer.append(TEXT_398);
    }
    stringBuffer.append(TEXT_244);
    }
    } else {
    if (!genModel.isSuppressNotification()) {
    stringBuffer.append(TEXT_459);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.impl.ENotificationImpl"));
    stringBuffer.append(TEXT_241);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.common.notify.Notification"));
    stringBuffer.append(TEXT_393);
    stringBuffer.append(genClass.getQualifiedFeatureID(genFeature));
    stringBuffer.append(positiveOffsetCorrection);
    stringBuffer.append(TEXT_197);
    if (genModel.isVirtualDelegation() && !genFeature.isPrimitiveType()) {
    stringBuffer.append(TEXT_395);
    stringBuffer.append(genFeature.getCapName());
    stringBuffer.append(TEXT_462);
    stringBuffer.append(genFeature.getEDefault());
    stringBuffer.append(TEXT_461);
    stringBuffer.append(genFeature.getCapName());
    } else {
    stringBuffer.append(TEXT_395);
    stringBuffer.append(genFeature.getCapName());
    }
    stringBuffer.append(TEXT_197);
    if (genClass.isFlag(genFeature)) {
    stringBuffer.append(TEXT_413);
    stringBuffer.append(genFeature.getCapName());
    } else {
    stringBuffer.append(genFeature.getSafeName());
    }
    stringBuffer.append(TEXT_244);
    }
    }
    }
    } else if (genFeature.hasDelegateFeature()) { GenFeature delegateFeature = genFeature.getDelegateFeature();
    if (delegateFeature.isWrappedFeatureMapType()) {
    stringBuffer.append(TEXT_463);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.util.FeatureMap"));
    stringBuffer.append(TEXT_259);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.util.FeatureMap"));
    stringBuffer.append(TEXT_260);
    stringBuffer.append(delegateFeature.getAccessorName());
    stringBuffer.append(TEXT_464);
    stringBuffer.append(genFeature.getQualifiedFeatureAccessor());
    stringBuffer.append(TEXT_197);
    if (!isJDK50 && genFeature.isPrimitiveType()) {
    stringBuffer.append(TEXT_412);
    stringBuffer.append(genFeature.getObjectType(genClass));
    stringBuffer.append(TEXT_91);
    }
    stringBuffer.append(TEXT_413);
    stringBuffer.append(genFeature.getCapName());
    if (!isJDK50 && genFeature.isPrimitiveType()) {
    stringBuffer.append(TEXT_85);
    }
    stringBuffer.append(TEXT_205);
    } else {
    stringBuffer.append(TEXT_463);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.util.FeatureMap"));
    stringBuffer.append(TEXT_402);
    stringBuffer.append(delegateFeature.getAccessorName());
    stringBuffer.append(TEXT_465);
    stringBuffer.append(genFeature.getQualifiedFeatureAccessor());
    stringBuffer.append(TEXT_197);
    if (!isJDK50 && genFeature.isPrimitiveType()) {
    stringBuffer.append(TEXT_412);
    stringBuffer.append(genFeature.getObjectType(genClass));
    stringBuffer.append(TEXT_91);
    }
    stringBuffer.append(TEXT_413);
    stringBuffer.append(genFeature.getCapName());
    if (!isJDK50 && genFeature.isPrimitiveType()) {
    stringBuffer.append(TEXT_85);
    }
    stringBuffer.append(TEXT_205);
    }
    } else if (setAccessorOperation != null) {
    stringBuffer.append(TEXT_116);
    stringBuffer.append(setAccessorOperation.getBody(genModel.getIndentation(stringBuffer)));
    } else {
    //GEastadl ###############
    GAwareGenFeature gGenFeature = (GAwareGenFeature)genFeature;
    if(gGenFeature.isGGenFeature()){
    stringBuffer.append(TEXT_126);
    TargetFeaturePath tgtFeaturePath = ((GAwareGenFeature)genFeature).getTargetFeaturePath(genClass);
    stringBuffer.append(TEXT_126);
     if(tgtFeaturePath.getClass().equals(NoMappingTargetFeaturePath.class)){
    stringBuffer.append(TEXT_116);
    stringBuffer.append(TEXT_272);
     } else { 
    stringBuffer.append(TEXT_466);
     /**** Simple Case: Single-Segmented TargetFeaturePath ****/ 
    stringBuffer.append(TEXT_116);
     GAwareGenFeature tgtFeature = tgtFeaturePath.getLastFeature(); 
    stringBuffer.append(TEXT_116);
     if(tgtFeaturePath.getSegmentCount() == 1) { 
    stringBuffer.append(TEXT_467);
     if(gGenFeature.isJavaMappedType()) { 
    stringBuffer.append(TEXT_222);
     if(tgtFeature.isListType()) { // JavaMappedType => List
    stringBuffer.append(TEXT_276);
    stringBuffer.append(TEXT_288);
     } else if(tgtFeature.isJavaMappedType()) { // JavaMappedType => JavaMappedType 
    stringBuffer.append(TEXT_276);
     GAwareGenFeature previousFeature = tgtFeature;
    stringBuffer.append(TEXT_468);
     if(gGenFeature.isInteger() && tgtFeaturePath.isLong()) { // Integer => Long 
    stringBuffer.append(TEXT_126);
    if(tgtFeaturePath.length() > 1){
    stringBuffer.append(previousFeature.getName());
    stringBuffer.append(TEXT_166);
    }
    stringBuffer.append(TEXT_469);
    stringBuffer.append(tgtFeature.getAccessorName());
    if (genClass.hasCollidingSetAccessorOperation(tgtFeature)) {
    stringBuffer.append(TEXT_195);
    }
    stringBuffer.append(TEXT_91);
    stringBuffer.append(setAccessorOperation == null ? "new" + gGenFeature.getCapName() : setAccessorOperation.getGenParameters().get(0).getName());
    stringBuffer.append(TEXT_470);
     } else if(gGenFeature.isInteger() && tgtFeaturePath.isString()) { // Integer => String 
    stringBuffer.append(TEXT_126);
    if(tgtFeaturePath.length() > 1){
    stringBuffer.append(previousFeature.getName());
    stringBuffer.append(TEXT_166);
    }
    stringBuffer.append(TEXT_469);
    stringBuffer.append(tgtFeature.getAccessorName());
    if (genClass.hasCollidingSetAccessorOperation(tgtFeature)) {
    stringBuffer.append(TEXT_195);
    }
    stringBuffer.append(TEXT_471);
    stringBuffer.append(setAccessorOperation == null ? "new" + gGenFeature.getCapName() : setAccessorOperation.getGenParameters().get(0).getName());
    stringBuffer.append(TEXT_244);
     } else if(gGenFeature.isDouble() && tgtFeaturePath.isString()) { // Double => String 
    stringBuffer.append(TEXT_126);
    if(tgtFeaturePath.length() > 1){
    stringBuffer.append(previousFeature.getName());
    stringBuffer.append(TEXT_166);
    }
    stringBuffer.append(TEXT_469);
    stringBuffer.append(tgtFeature.getAccessorName());
    if (genClass.hasCollidingSetAccessorOperation(tgtFeature)) {
    stringBuffer.append(TEXT_195);
    }
    stringBuffer.append(TEXT_91);
    stringBuffer.append(setAccessorOperation == null ? "new" + gGenFeature.getCapName() : setAccessorOperation.getGenParameters().get(0).getName());
    stringBuffer.append(TEXT_472);
     } else if(gGenFeature.isBigDecimal() && tgtFeaturePath.isString()) { // BigDecimal => String 
    stringBuffer.append(TEXT_126);
    if(tgtFeaturePath.length() > 1){
    stringBuffer.append(previousFeature.getName());
    stringBuffer.append(TEXT_166);
    }
    stringBuffer.append(TEXT_469);
    stringBuffer.append(tgtFeature.getAccessorName());
    if (genClass.hasCollidingSetAccessorOperation(tgtFeature)) {
    stringBuffer.append(TEXT_195);
    }
    stringBuffer.append(TEXT_91);
    stringBuffer.append(setAccessorOperation == null ? "new" + gGenFeature.getCapName() : setAccessorOperation.getGenParameters().get(0).getName());
    stringBuffer.append(TEXT_473);
     } else if(gGenFeature.isBigDecimal() && tgtFeaturePath.isInteger()) { // BigDecimal => Integer 
    stringBuffer.append(TEXT_126);
    if(tgtFeaturePath.length() > 1){
    stringBuffer.append(previousFeature.getName());
    stringBuffer.append(TEXT_166);
    }
    stringBuffer.append(TEXT_469);
    stringBuffer.append(tgtFeature.getAccessorName());
    if (genClass.hasCollidingSetAccessorOperation(tgtFeature)) {
    stringBuffer.append(TEXT_195);
    }
    stringBuffer.append(TEXT_91);
    stringBuffer.append(setAccessorOperation == null ? "new" + gGenFeature.getCapName() : setAccessorOperation.getGenParameters().get(0).getName());
    stringBuffer.append(TEXT_474);
     } else { // Any JavaMappedType => Any JavaMappedType 
    stringBuffer.append(TEXT_475);
    if(tgtFeaturePath.length() > 1){
    stringBuffer.append(previousFeature.getName());
    stringBuffer.append(TEXT_166);
    }
    stringBuffer.append(TEXT_469);
    stringBuffer.append(tgtFeature.getAccessorName());
    if (genClass.hasCollidingSetAccessorOperation(tgtFeature)) {
    stringBuffer.append(TEXT_195);
    }
    stringBuffer.append(TEXT_91);
    stringBuffer.append(setAccessorOperation == null ? "new" + gGenFeature.getCapName() : setAccessorOperation.getGenParameters().get(0).getName());
    stringBuffer.append(TEXT_205);
     } 
    stringBuffer.append(TEXT_222);
     } else if(tgtFeature.isEnumType()) { // JavaMappedType => Enum 
    stringBuffer.append(TEXT_276);
    stringBuffer.append(TEXT_299);
     } else { // JavaMappedType => EObject
    stringBuffer.append(TEXT_276);
    stringBuffer.append(TEXT_300);
     } 
    stringBuffer.append(TEXT_301);
     } else if(gGenFeature.isEnumType()) { 
    stringBuffer.append(TEXT_222);
     if(tgtFeature.isEnumType()) { // Enum => Enum 
    stringBuffer.append(TEXT_476);
    stringBuffer.append(tgtFeature.getAccessorName());
    if (genClass.hasCollidingSetAccessorOperation(tgtFeature)) {
    stringBuffer.append(TEXT_195);
    }
    stringBuffer.append(TEXT_91);
    stringBuffer.append(tgtFeature.getImportedType());
    stringBuffer.append(TEXT_303);
    stringBuffer.append(setAccessorOperation == null ? "new" + gGenFeature.getCapName() : setAccessorOperation.getGenParameters().get(0).getName());
    stringBuffer.append(TEXT_477);
     } else if(tgtFeature.isListType()) { // Enum => List 
    stringBuffer.append(TEXT_276);
    stringBuffer.append(TEXT_305);
     } else if(tgtFeature.isJavaMappedType()) { // Enum => JavaMappedType 
    stringBuffer.append(TEXT_276);
    stringBuffer.append(TEXT_306);
     } else { // Enum => EObject 
    stringBuffer.append(TEXT_276);
    stringBuffer.append(TEXT_307);
     } 
    stringBuffer.append(TEXT_308);
     } else { 
    stringBuffer.append(TEXT_222);
    if(tgtFeature.isListType()) { // EObject => List 
    stringBuffer.append(TEXT_276);
    stringBuffer.append(tgtFeature.getImportedType());
    stringBuffer.append(TEXT_309);
    stringBuffer.append(tgtFeature.getGetAccessor());
    stringBuffer.append(TEXT_478);
    stringBuffer.append(tgtFeature.getImportedElementType(genClass));
    stringBuffer.append(TEXT_85);
    if (genClass.hasCollidingSetAccessorOperation(tgtFeature)) {
    stringBuffer.append(TEXT_195);
    }
    stringBuffer.append(setAccessorOperation == null ? "new" + gGenFeature.getCapName() : setAccessorOperation.getGenParameters().get(0).getName());
    stringBuffer.append(TEXT_479);
     } else if(tgtFeaturePath.isJavaMappedType()) { // EObject => JavaMappedType 
    stringBuffer.append(TEXT_276);
    stringBuffer.append(TEXT_314);
     } else if(tgtFeature.isEnumType()) { // EObject => Enum 
    stringBuffer.append(TEXT_276);
    stringBuffer.append(TEXT_315);
     } else { // EObject => EObject 
    stringBuffer.append(TEXT_480);
    stringBuffer.append(tgtFeature.getAccessorName());
    if (genClass.hasCollidingSetAccessorOperation(tgtFeature)) {
    stringBuffer.append(TEXT_195);
    }
    stringBuffer.append(TEXT_258);
    stringBuffer.append(tgtFeature.getImportedType());
    stringBuffer.append(TEXT_447);
    stringBuffer.append(setAccessorOperation == null ? "new" + gGenFeature.getCapName() : setAccessorOperation.getGenParameters().get(0).getName());
    stringBuffer.append(TEXT_481);
     } 
    stringBuffer.append(TEXT_209);
     } 
    stringBuffer.append(TEXT_318);
     /**** Complex Case: TargetFeaturePath with multiple segments ****/ 
    stringBuffer.append(TEXT_318);
     } else if(tgtFeaturePath.getSegmentCount() > 1) { 
    stringBuffer.append(TEXT_209);
     if(gGenFeature.isJavaMappedType()) { 
    stringBuffer.append(TEXT_222);
     if(tgtFeature.isListType()) { // JavaMappedType => List
    stringBuffer.append(TEXT_276);
    stringBuffer.append(TEXT_288);
     } else if(tgtFeature.isJavaMappedType()) { // JavaMappedType => JavaMappedType 
    stringBuffer.append(TEXT_276);
     GAwareGenFeature previousFeature = null; 
     for(GAwareGenFeature feature : tgtFeaturePath.getHeadFeatures()) { 
    stringBuffer.append(TEXT_126);
    stringBuffer.append(feature.getImportedType(genClass));
    stringBuffer.append(TEXT_65);
    stringBuffer.append(feature.getName());
    stringBuffer.append(TEXT_76);
     if(previousFeature != null) {
    stringBuffer.append( previousFeature.getName() );
    stringBuffer.append(TEXT_166);
    }
    stringBuffer.append( feature.getGetAccessor() );
    stringBuffer.append(TEXT_324);
    stringBuffer.append(feature.getName());
    stringBuffer.append(TEXT_343);
    stringBuffer.append(feature.getName());
    stringBuffer.append(TEXT_76);
    stringBuffer.append(GenModels.getImportedFactoryName(genModel));
    stringBuffer.append(TEXT_344);
    stringBuffer.append(feature.getImportedType(genClass));
    stringBuffer.append(TEXT_482);
     if(previousFeature != null) {
    stringBuffer.append( previousFeature.getName() );
    stringBuffer.append(TEXT_166);
    }
    stringBuffer.append( feature.getSetAccessorName() );
    stringBuffer.append(TEXT_91);
    stringBuffer.append(feature.getName());
    stringBuffer.append(TEXT_483);
     previousFeature = feature; 
     } 
    stringBuffer.append(TEXT_484);
     if(gGenFeature.isInteger() && tgtFeaturePath.isLong()) { // Integer => Long 
    stringBuffer.append(TEXT_126);
    if(tgtFeaturePath.length() > 1){
    stringBuffer.append(previousFeature.getName());
    stringBuffer.append(TEXT_166);
    }
    stringBuffer.append(TEXT_469);
    stringBuffer.append(tgtFeature.getAccessorName());
    if (genClass.hasCollidingSetAccessorOperation(tgtFeature)) {
    stringBuffer.append(TEXT_195);
    }
    stringBuffer.append(TEXT_91);
    stringBuffer.append(setAccessorOperation == null ? "new" + gGenFeature.getCapName() : setAccessorOperation.getGenParameters().get(0).getName());
    stringBuffer.append(TEXT_470);
     } else if(gGenFeature.isInteger() && tgtFeaturePath.isString()) { // Integer => String 
    stringBuffer.append(TEXT_126);
    if(tgtFeaturePath.length() > 1){
    stringBuffer.append(previousFeature.getName());
    stringBuffer.append(TEXT_166);
    }
    stringBuffer.append(TEXT_469);
    stringBuffer.append(tgtFeature.getAccessorName());
    if (genClass.hasCollidingSetAccessorOperation(tgtFeature)) {
    stringBuffer.append(TEXT_195);
    }
    stringBuffer.append(TEXT_471);
    stringBuffer.append(setAccessorOperation == null ? "new" + gGenFeature.getCapName() : setAccessorOperation.getGenParameters().get(0).getName());
    stringBuffer.append(TEXT_244);
     } else if(gGenFeature.isDouble() && tgtFeaturePath.isString()) { // Double => String 
    stringBuffer.append(TEXT_126);
    if(tgtFeaturePath.length() > 1){
    stringBuffer.append(previousFeature.getName());
    stringBuffer.append(TEXT_166);
    }
    stringBuffer.append(TEXT_469);
    stringBuffer.append(tgtFeature.getAccessorName());
    if (genClass.hasCollidingSetAccessorOperation(tgtFeature)) {
    stringBuffer.append(TEXT_195);
    }
    stringBuffer.append(TEXT_91);
    stringBuffer.append(setAccessorOperation == null ? "new" + gGenFeature.getCapName() : setAccessorOperation.getGenParameters().get(0).getName());
    stringBuffer.append(TEXT_472);
     } else if(gGenFeature.isBigDecimal() && tgtFeaturePath.isString()) { // BigDecimal => String 
    stringBuffer.append(TEXT_126);
    if(tgtFeaturePath.length() > 1){
    stringBuffer.append(previousFeature.getName());
    stringBuffer.append(TEXT_166);
    }
    stringBuffer.append(TEXT_469);
    stringBuffer.append(tgtFeature.getAccessorName());
    if (genClass.hasCollidingSetAccessorOperation(tgtFeature)) {
    stringBuffer.append(TEXT_195);
    }
    stringBuffer.append(TEXT_91);
    stringBuffer.append(setAccessorOperation == null ? "new" + gGenFeature.getCapName() : setAccessorOperation.getGenParameters().get(0).getName());
    stringBuffer.append(TEXT_473);
     } else if(gGenFeature.isBigDecimal() && tgtFeaturePath.isInteger()) { // BigDecimal => Integer 
    stringBuffer.append(TEXT_126);
    if(tgtFeaturePath.length() > 1){
    stringBuffer.append(previousFeature.getName());
    stringBuffer.append(TEXT_166);
    }
    stringBuffer.append(TEXT_469);
    stringBuffer.append(tgtFeature.getAccessorName());
    if (genClass.hasCollidingSetAccessorOperation(tgtFeature)) {
    stringBuffer.append(TEXT_195);
    }
    stringBuffer.append(TEXT_91);
    stringBuffer.append(setAccessorOperation == null ? "new" + gGenFeature.getCapName() : setAccessorOperation.getGenParameters().get(0).getName());
    stringBuffer.append(TEXT_474);
     } else { // Any JavaMappedType => Any JavaMappedType 
    stringBuffer.append(TEXT_475);
    if(tgtFeaturePath.length() > 1){
    stringBuffer.append(previousFeature.getName());
    stringBuffer.append(TEXT_166);
    }
    stringBuffer.append(TEXT_469);
    stringBuffer.append(tgtFeature.getAccessorName());
    if (genClass.hasCollidingSetAccessorOperation(tgtFeature)) {
    stringBuffer.append(TEXT_195);
    }
    stringBuffer.append(TEXT_91);
    stringBuffer.append(setAccessorOperation == null ? "new" + gGenFeature.getCapName() : setAccessorOperation.getGenParameters().get(0).getName());
    stringBuffer.append(TEXT_205);
     } 
    stringBuffer.append(TEXT_222);
     } else if(tgtFeature.isEnumType()) { // JavaMappedType => Enum 
    stringBuffer.append(TEXT_276);
    stringBuffer.append(TEXT_299);
     } else { // JavaMappedType => EObject
    stringBuffer.append(TEXT_276);
    stringBuffer.append(TEXT_300);
     } 
    stringBuffer.append(TEXT_301);
     } else if(gGenFeature.isEnumType()) { 
    stringBuffer.append(TEXT_222);
     if(tgtFeature.isEnumType()) { // Enum => Enum 
    stringBuffer.append(TEXT_276);
     GAwareGenFeature previousFeature = null; 
     for(GAwareGenFeature feature : tgtFeaturePath.getHeadFeatures()) { 
    stringBuffer.append(TEXT_126);
    stringBuffer.append(feature.getImportedType(genClass));
    stringBuffer.append(TEXT_65);
    stringBuffer.append(feature.getName());
    stringBuffer.append(TEXT_76);
     if(previousFeature != null) {
    stringBuffer.append( previousFeature.getName() );
    stringBuffer.append(TEXT_166);
    }
    stringBuffer.append( feature.getGetAccessor() );
    stringBuffer.append(TEXT_324);
    stringBuffer.append(feature.getName());
    stringBuffer.append(TEXT_343);
    stringBuffer.append(feature.getName());
    stringBuffer.append(TEXT_76);
    stringBuffer.append(GenModels.getImportedFactoryName(genModel));
    stringBuffer.append(TEXT_344);
    stringBuffer.append(feature.getImportedType(genClass));
    stringBuffer.append(TEXT_482);
     if(previousFeature != null) {
    stringBuffer.append( previousFeature.getName() );
    stringBuffer.append(TEXT_166);
    }
    stringBuffer.append( feature.getSetAccessorName() );
    stringBuffer.append(TEXT_91);
    stringBuffer.append(feature.getName());
    stringBuffer.append(TEXT_483);
     previousFeature = feature; 
     } 
    stringBuffer.append(TEXT_485);
    stringBuffer.append( previousFeature.getName());
    stringBuffer.append(TEXT_486);
    stringBuffer.append(tgtFeature.getAccessorName());
    if (genClass.hasCollidingSetAccessorOperation(tgtFeature)) {
    stringBuffer.append(TEXT_195);
    }
    stringBuffer.append(TEXT_91);
    stringBuffer.append(tgtFeature.getImportedType());
    stringBuffer.append(TEXT_303);
    stringBuffer.append(setAccessorOperation == null ? "new" + gGenFeature.getCapName() : setAccessorOperation.getGenParameters().get(0).getName());
    stringBuffer.append(TEXT_487);
     } else if(tgtFeature.isListType()) { // Enum => List 
    stringBuffer.append(TEXT_276);
    stringBuffer.append(TEXT_305);
     } else if(tgtFeature.isJavaMappedType()) { // Enum => JavaMappedType 
    stringBuffer.append(TEXT_276);
    stringBuffer.append(TEXT_306);
     } else { // Enum => EObject 
    stringBuffer.append(TEXT_276);
    stringBuffer.append(TEXT_307);
     } 
    stringBuffer.append(TEXT_308);
     } else { 
    stringBuffer.append(TEXT_222);
    if(tgtFeature.isListType()) { // EObject => List 
    stringBuffer.append(TEXT_276);
    stringBuffer.append(TEXT_488);
     } else if(tgtFeaturePath.isJavaMappedType()) { // EObject => JavaMappedType 
    stringBuffer.append(TEXT_276);
    stringBuffer.append(TEXT_314);
     } else if(tgtFeature.isEnumType()) { // EObject => Enum 
    stringBuffer.append(TEXT_276);
    stringBuffer.append(TEXT_315);
     } else { // EObject => EObject 
    stringBuffer.append(TEXT_484);
    GAwareGenFeature previousFeature = null;
    stringBuffer.append(TEXT_276);
    for(GAwareGenFeature feature : tgtFeaturePath.getHeadFeatures()) {
    stringBuffer.append(TEXT_489);
    stringBuffer.append(feature.getImportedType(genClass));
    stringBuffer.append(TEXT_65);
    stringBuffer.append(feature.getName());
    stringBuffer.append(TEXT_76);
    stringBuffer.append( previousFeature != null ? previousFeature.getName() + "." : "");
    stringBuffer.append(feature.getGetAccessor());
    stringBuffer.append(TEXT_490);
    stringBuffer.append(feature.getName());
    stringBuffer.append(TEXT_491);
    stringBuffer.append(feature.getName());
    stringBuffer.append(TEXT_76);
    stringBuffer.append(GenModels.getImportedFactoryName(genModel));
    stringBuffer.append(TEXT_344);
    stringBuffer.append(feature.getImportedType(genClass));
    stringBuffer.append(TEXT_492);
    stringBuffer.append( previousFeature != null ? previousFeature.getName() + "." : "");
    stringBuffer.append(TEXT_469);
    stringBuffer.append(feature.getAccessorName());
    stringBuffer.append(TEXT_91);
    stringBuffer.append(feature.getName());
    stringBuffer.append(TEXT_493);
    previousFeature = feature;
    stringBuffer.append(TEXT_276);
    }
    stringBuffer.append(TEXT_485);
     GAwareGenFeature lastFeature = tgtFeaturePath.getLastFeature(); 
    stringBuffer.append(TEXT_276);
    stringBuffer.append( previousFeature.getName() );
    stringBuffer.append(TEXT_486);
    stringBuffer.append(lastFeature.getAccessorName());
    if (genClass.hasCollidingSetAccessorOperation(lastFeature)) {
    stringBuffer.append(TEXT_195);
    }
    stringBuffer.append(TEXT_258);
    stringBuffer.append(lastFeature.getImportedType());
    stringBuffer.append(TEXT_447);
    stringBuffer.append(setAccessorOperation == null ? "new" + gGenFeature.getCapName() : setAccessorOperation.getGenParameters().get(0).getName());
    stringBuffer.append(TEXT_481);
     } 
    stringBuffer.append(TEXT_209);
     } 
    stringBuffer.append(TEXT_494);
    } else { 
    stringBuffer.append(TEXT_351);
     } 
    stringBuffer.append(TEXT_352);
    }
    } else{
    stringBuffer.append(TEXT_353);
    stringBuffer.append(gGenFeature.getFormattedName());
    stringBuffer.append(TEXT_354);
    stringBuffer.append(gGenFeature.getFeatureKind());
    stringBuffer.append(TEXT_495);
    }
    //GEastadl End ############
    stringBuffer.append(TEXT_362);
    //Class/setGenFeature.todo.override.javajetinc
    }
    stringBuffer.append(TEXT_151);
    }
    //Class/setGenFeature.override.javajetinc
    }
    if (isImplementation && !genModel.isReflectiveDelegation() && genFeature.isBasicUnset()) {
    stringBuffer.append(TEXT_124);
    if (isJDK50) { //Class/basicUnsetGenFeature.annotations.insert.javajetinc
    }
    stringBuffer.append(TEXT_128);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.common.notify.NotificationChain"));
    stringBuffer.append(TEXT_496);
    stringBuffer.append(genFeature.getAccessorName());
    stringBuffer.append(TEXT_91);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.common.notify.NotificationChain"));
    stringBuffer.append(TEXT_371);
    if (genModel.isDynamicDelegation()) {
    stringBuffer.append(TEXT_497);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.InternalEObject"));
    stringBuffer.append(TEXT_85);
    if (genFeature.isResolveProxies()) {
    stringBuffer.append(TEXT_498);
    stringBuffer.append(genFeature.getAccessorName());
    } else {
    stringBuffer.append(genFeature.getGetAccessor());
    }
    stringBuffer.append(TEXT_499);
    stringBuffer.append(genClass.getQualifiedFeatureID(genFeature));
    stringBuffer.append(positiveOffsetCorrection);
    stringBuffer.append(TEXT_374);
    } else if (!genFeature.isVolatile()) {
    if (genModel.isVirtualDelegation()) {
    if (!genModel.isSuppressNotification()) {
    stringBuffer.append(TEXT_500);
    stringBuffer.append(genFeature.getCapName());
    stringBuffer.append(TEXT_76);
    }
    stringBuffer.append(TEXT_501);
    stringBuffer.append(genClass.getQualifiedFeatureID(genFeature));
    stringBuffer.append(positiveOffsetCorrection);
    stringBuffer.append(TEXT_205);
    } else {
    if (!genModel.isSuppressNotification()) {
    stringBuffer.append(TEXT_116);
    stringBuffer.append(genFeature.getImportedType(genClass));
    stringBuffer.append(TEXT_216);
    stringBuffer.append(genFeature.getCapName());
    stringBuffer.append(TEXT_76);
    stringBuffer.append(genFeature.getSafeName());
    stringBuffer.append(TEXT_7);
    }
    stringBuffer.append(TEXT_116);
    stringBuffer.append(genFeature.getSafeName());
    stringBuffer.append(TEXT_502);
    }
    if (genModel.isVirtualDelegation()) {
    if (!genModel.isSuppressNotification()) {
    stringBuffer.append(TEXT_382);
    stringBuffer.append(genFeature.getCapName());
    stringBuffer.append(TEXT_503);
    }
    } else if (genClass.isESetFlag(genFeature)) {
    if (!genModel.isSuppressNotification()) {
    stringBuffer.append(TEXT_384);
    stringBuffer.append(genFeature.getCapName());
    stringBuffer.append(TEXT_385);
    stringBuffer.append(genClass.getESetFlagsField(genFeature));
    stringBuffer.append(TEXT_247);
    stringBuffer.append(genFeature.getUpperName());
    stringBuffer.append(TEXT_386);
    }
    stringBuffer.append(TEXT_116);
    stringBuffer.append(genClass.getESetFlagsField(genFeature));
    stringBuffer.append(TEXT_449);
    stringBuffer.append(genFeature.getUpperName());
    stringBuffer.append(TEXT_387);
    } else {
    if (!genModel.isSuppressNotification()) {
    stringBuffer.append(TEXT_384);
    stringBuffer.append(genFeature.getCapName());
    stringBuffer.append(TEXT_388);
    stringBuffer.append(genFeature.getUncapName());
    stringBuffer.append(TEXT_389);
    }
    stringBuffer.append(TEXT_116);
    stringBuffer.append(genFeature.getUncapName());
    stringBuffer.append(TEXT_504);
    }
    if (!genModel.isSuppressNotification()) {
    stringBuffer.append(TEXT_505);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.impl.ENotificationImpl"));
    stringBuffer.append(TEXT_392);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.impl.ENotificationImpl"));
    stringBuffer.append(TEXT_241);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.common.notify.Notification"));
    stringBuffer.append(TEXT_506);
    stringBuffer.append(genClass.getQualifiedFeatureID(genFeature));
    stringBuffer.append(positiveOffsetCorrection);
    stringBuffer.append(TEXT_197);
    if (genModel.isVirtualDelegation()) {
    stringBuffer.append(TEXT_507);
    stringBuffer.append(genFeature.getCapName());
    stringBuffer.append(TEXT_508);
    } else {
    stringBuffer.append(TEXT_395);
    stringBuffer.append(genFeature.getCapName());
    }
    stringBuffer.append(TEXT_509);
    if (genModel.isVirtualDelegation()) {
    stringBuffer.append(TEXT_396);
    } else {
    stringBuffer.append(TEXT_395);
    stringBuffer.append(genFeature.getCapName());
    stringBuffer.append(TEXT_398);
    }
    stringBuffer.append(TEXT_510);
    }
    } else {
    stringBuffer.append(TEXT_511);
    stringBuffer.append(genFeature.getFormattedName());
    stringBuffer.append(TEXT_354);
    stringBuffer.append(genFeature.getFeatureKind());
    stringBuffer.append(TEXT_405);
    //Class/basicUnsetGenFeature.todo.override.javajetinc
    }
    stringBuffer.append(TEXT_151);
    //Class.basicUnsetGenFeature.override.javajetinc
    }
    if (genFeature.isUnset() && (isImplementation || !genFeature.isSuppressedUnsetVisibility())) {
    if (isInterface) {
    stringBuffer.append(TEXT_512);
    stringBuffer.append(genClass.getQualifiedInterfaceName());
    stringBuffer.append(TEXT_17);
    stringBuffer.append(genFeature.getGetAccessor());
    stringBuffer.append(TEXT_18);
    stringBuffer.append(genFeature.getFormattedName());
    stringBuffer.append(TEXT_56);
    stringBuffer.append(genFeature.getFeatureKind());
    stringBuffer.append(TEXT_166);
    stringBuffer.append(TEXT_513);
    if (!genFeature.isSuppressedIsSetVisibility()) {
    stringBuffer.append(TEXT_189);
    stringBuffer.append(((GAwareGenFeature)genFeature).getIsSetAccessorName());
    stringBuffer.append(TEXT_23);
    }
    stringBuffer.append(TEXT_189);
    stringBuffer.append(genFeature.getGetAccessor());
    stringBuffer.append(TEXT_23);
    if (!genFeature.isListType() && !genFeature.isSuppressedSetVisibility()) {
    stringBuffer.append(TEXT_190);
    stringBuffer.append(genFeature.getAccessorName());
    stringBuffer.append(TEXT_91);
    stringBuffer.append(genFeature.getRawImportedBoundType());
    stringBuffer.append(TEXT_85);
    }
    stringBuffer.append(TEXT_194);
    //Class/unsetGenFeature.javadoc.override.javajetinc
    } else {
    stringBuffer.append(TEXT_124);
    if (isJDK50) { //Class/unsetGenFeature.annotations.insert.javajetinc
    }
    }
    if (!isImplementation) {
    stringBuffer.append(TEXT_152);
    stringBuffer.append(((GAwareGenFeature)genFeature).getUnsetAccessorName());
    stringBuffer.append(TEXT_127);
    } else {
    stringBuffer.append(TEXT_155);
    stringBuffer.append(((GAwareGenFeature)genFeature).getUnsetAccessorName());
    if (genClass.hasCollidingUnsetAccessorOperation(genFeature)) {
    stringBuffer.append(TEXT_195);
    }
    stringBuffer.append(TEXT_129);
    if (genModel.isDynamicDelegation()) {
    stringBuffer.append(TEXT_514);
    stringBuffer.append(genClass.getQualifiedFeatureID(genFeature));
    stringBuffer.append(TEXT_197);
    stringBuffer.append(genFeature.getQualifiedFeatureAccessor());
    stringBuffer.append(TEXT_205);
    } else if (genModel.isReflectiveDelegation()) {
    stringBuffer.append(TEXT_515);
    stringBuffer.append(genFeature.getQualifiedFeatureAccessor());
    stringBuffer.append(TEXT_205);
    } else if (genFeature.hasSettingDelegate()) {
    stringBuffer.append(TEXT_116);
    stringBuffer.append(genFeature.getUpperName());
    stringBuffer.append(TEXT_516);
    } else if (!genFeature.isVolatile()) {
    if (genFeature.isListType()) {
    if (genModel.isVirtualDelegation()) {
    stringBuffer.append(TEXT_116);
    stringBuffer.append(genFeature.getImportedType(genClass));
    stringBuffer.append(TEXT_65);
    stringBuffer.append(genFeature.getSafeName());
    stringBuffer.append(TEXT_203);
    stringBuffer.append(genFeature.getImportedType(genClass));
    stringBuffer.append(TEXT_204);
    stringBuffer.append(genClass.getQualifiedFeatureID(genFeature));
    stringBuffer.append(positiveOffsetCorrection);
    stringBuffer.append(TEXT_205);
    }
    stringBuffer.append(TEXT_133);
    stringBuffer.append(genFeature.getSafeName());
    stringBuffer.append(TEXT_517);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.util.InternalEList"));
    stringBuffer.append(TEXT_518);
    stringBuffer.append(singleWildcard);
    stringBuffer.append(TEXT_85);
    stringBuffer.append(genFeature.getSafeName());
    stringBuffer.append(TEXT_519);
    } else if (genFeature.isBidirectional() || genFeature.isEffectiveContains()) {
    if (genModel.isVirtualDelegation()) {
    stringBuffer.append(TEXT_116);
    stringBuffer.append(genFeature.getImportedType(genClass));
    stringBuffer.append(TEXT_65);
    stringBuffer.append(genFeature.getSafeName());
    stringBuffer.append(TEXT_203);
    stringBuffer.append(genFeature.getImportedType(genClass));
    stringBuffer.append(TEXT_204);
    stringBuffer.append(genClass.getQualifiedFeatureID(genFeature));
    stringBuffer.append(positiveOffsetCorrection);
    stringBuffer.append(TEXT_205);
    }
    stringBuffer.append(TEXT_133);
    stringBuffer.append(genFeature.getSafeName());
    stringBuffer.append(TEXT_520);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.common.notify.NotificationChain"));
    stringBuffer.append(TEXT_521);
    if (!genFeature.isBidirectional()) {
    stringBuffer.append(TEXT_522);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.InternalEObject"));
    stringBuffer.append(TEXT_85);
    stringBuffer.append(genFeature.getSafeName());
    stringBuffer.append(TEXT_434);
    stringBuffer.append(genClass.getQualifiedFeatureID(genFeature));
    stringBuffer.append(negativeOffsetCorrection);
    stringBuffer.append(TEXT_234);
    } else { GenFeature reverseFeature = genFeature.getReverse(); GenClass targetClass = reverseFeature.getGenClass(); String reverseOffsetCorrection = targetClass.hasOffsetCorrection() ? " + " + genClass.getOffsetCorrectionField(genFeature) : "";
    stringBuffer.append(TEXT_522);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.InternalEObject"));
    stringBuffer.append(TEXT_85);
    stringBuffer.append(genFeature.getSafeName());
    stringBuffer.append(TEXT_437);
    stringBuffer.append(targetClass.getQualifiedFeatureID(reverseFeature));
    stringBuffer.append(reverseOffsetCorrection);
    stringBuffer.append(TEXT_197);
    stringBuffer.append(targetClass.getRawImportedInterfaceName());
    stringBuffer.append(TEXT_237);
    }
    stringBuffer.append(TEXT_523);
    stringBuffer.append(genFeature.getAccessorName());
    stringBuffer.append(TEXT_524);
    if (genModel.isVirtualDelegation()) {
    stringBuffer.append(TEXT_441);
    stringBuffer.append(genFeature.getCapName());
    stringBuffer.append(TEXT_442);
    stringBuffer.append(genClass.getQualifiedFeatureID(genFeature));
    stringBuffer.append(positiveOffsetCorrection);
    stringBuffer.append(TEXT_205);
    } else if (genClass.isESetFlag(genFeature)) {
    if (!genModel.isSuppressNotification()) {
    stringBuffer.append(TEXT_441);
    stringBuffer.append(genFeature.getCapName());
    stringBuffer.append(TEXT_385);
    stringBuffer.append(genClass.getESetFlagsField(genFeature));
    stringBuffer.append(TEXT_247);
    stringBuffer.append(genFeature.getUpperName());
    stringBuffer.append(TEXT_386);
    }
    stringBuffer.append(TEXT_209);
    stringBuffer.append(genClass.getESetFlagsField(genFeature));
    stringBuffer.append(TEXT_449);
    stringBuffer.append(genFeature.getUpperName());
    stringBuffer.append(TEXT_387);
    } else {
    if (!genModel.isSuppressNotification()) {
    stringBuffer.append(TEXT_441);
    stringBuffer.append(genFeature.getCapName());
    stringBuffer.append(TEXT_388);
    stringBuffer.append(genFeature.getUncapName());
    stringBuffer.append(TEXT_389);
    }
    stringBuffer.append(TEXT_209);
    stringBuffer.append(genFeature.getUncapName());
    stringBuffer.append(TEXT_504);
    }
    if (!genModel.isSuppressNotification()) {
    stringBuffer.append(TEXT_443);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.impl.ENotificationImpl"));
    stringBuffer.append(TEXT_241);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.common.notify.Notification"));
    stringBuffer.append(TEXT_506);
    stringBuffer.append(genClass.getQualifiedFeatureID(genFeature));
    stringBuffer.append(positiveOffsetCorrection);
    stringBuffer.append(TEXT_525);
    stringBuffer.append(genFeature.getCapName());
    stringBuffer.append(TEXT_445);
    }
    stringBuffer.append(TEXT_446);
    } else {
    if (genClass.isFlag(genFeature)) {
    if (!genModel.isSuppressNotification()) {
    if (genFeature.isBooleanType()) {
    stringBuffer.append(TEXT_116);
    stringBuffer.append(genFeature.getImportedType(genClass));
    stringBuffer.append(TEXT_216);
    stringBuffer.append(genFeature.getCapName());
    stringBuffer.append(TEXT_203);
    stringBuffer.append(genClass.getFlagsField(genFeature));
    stringBuffer.append(TEXT_247);
    stringBuffer.append(genFeature.getUpperName());
    stringBuffer.append(TEXT_248);
    } else {
    stringBuffer.append(TEXT_116);
    stringBuffer.append(genFeature.getImportedType(genClass));
    stringBuffer.append(TEXT_216);
    stringBuffer.append(genFeature.getCapName());
    stringBuffer.append(TEXT_76);
    stringBuffer.append(genFeature.getUpperName());
    stringBuffer.append(TEXT_249);
    stringBuffer.append(genClass.getFlagsField(genFeature));
    stringBuffer.append(TEXT_247);
    stringBuffer.append(genFeature.getUpperName());
    stringBuffer.append(TEXT_250);
    stringBuffer.append(genFeature.getUpperName());
    stringBuffer.append(TEXT_251);
    }
    }
    } else if (genModel.isVirtualDelegation() && !genFeature.isPrimitiveType()) {
    stringBuffer.append(TEXT_377);
    stringBuffer.append(genFeature.getCapName());
    stringBuffer.append(TEXT_526);
    stringBuffer.append(genClass.getQualifiedFeatureID(genFeature));
    stringBuffer.append(positiveOffsetCorrection);
    stringBuffer.append(TEXT_205);
    } else {
    if (!genModel.isSuppressNotification()) {
    stringBuffer.append(TEXT_116);
    stringBuffer.append(genFeature.getImportedType(genClass));
    stringBuffer.append(TEXT_216);
    stringBuffer.append(genFeature.getCapName());
    stringBuffer.append(TEXT_76);
    stringBuffer.append(genFeature.getSafeName());
    stringBuffer.append(TEXT_7);
    }
    }
    if (!genModel.isSuppressNotification()) {
    if (genModel.isVirtualDelegation() && !genFeature.isPrimitiveType()) {
    stringBuffer.append(TEXT_382);
    stringBuffer.append(genFeature.getCapName());
    stringBuffer.append(TEXT_503);
    } else if (genClass.isESetFlag(genFeature)) {
    stringBuffer.append(TEXT_384);
    stringBuffer.append(genFeature.getCapName());
    stringBuffer.append(TEXT_385);
    stringBuffer.append(genClass.getESetFlagsField(genFeature));
    stringBuffer.append(TEXT_247);
    stringBuffer.append(genFeature.getUpperName());
    stringBuffer.append(TEXT_386);
    } else {
    stringBuffer.append(TEXT_384);
    stringBuffer.append(genFeature.getCapName());
    stringBuffer.append(TEXT_388);
    stringBuffer.append(genFeature.getUncapName());
    stringBuffer.append(TEXT_389);
    }
    }
    if (genFeature.isReferenceType()) {
    stringBuffer.append(TEXT_116);
    stringBuffer.append(genFeature.getSafeName());
    stringBuffer.append(TEXT_502);
    if (!genModel.isVirtualDelegation()) {
    if (genClass.isESetFlag(genFeature)) {
    stringBuffer.append(TEXT_116);
    stringBuffer.append(genClass.getESetFlagsField(genFeature));
    stringBuffer.append(TEXT_449);
    stringBuffer.append(genFeature.getUpperName());
    stringBuffer.append(TEXT_387);
    } else {
    stringBuffer.append(TEXT_116);
    stringBuffer.append(genFeature.getUncapName());
    stringBuffer.append(TEXT_504);
    }
    }
    if (!genModel.isSuppressNotification()) {
    stringBuffer.append(TEXT_459);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.impl.ENotificationImpl"));
    stringBuffer.append(TEXT_241);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.common.notify.Notification"));
    stringBuffer.append(TEXT_506);
    stringBuffer.append(genClass.getQualifiedFeatureID(genFeature));
    stringBuffer.append(positiveOffsetCorrection);
    stringBuffer.append(TEXT_197);
    if (genModel.isVirtualDelegation()) {
    stringBuffer.append(TEXT_507);
    stringBuffer.append(genFeature.getCapName());
    stringBuffer.append(TEXT_508);
    } else {
    stringBuffer.append(TEXT_395);
    stringBuffer.append(genFeature.getCapName());
    }
    stringBuffer.append(TEXT_509);
    if (genModel.isVirtualDelegation()) {
    stringBuffer.append(TEXT_396);
    } else {
    stringBuffer.append(TEXT_395);
    stringBuffer.append(genFeature.getCapName());
    stringBuffer.append(TEXT_398);
    }
    stringBuffer.append(TEXT_244);
    }
    } else {
    if (genClass.isFlag(genFeature)) {
    if (genFeature.isBooleanType()) {
    stringBuffer.append(TEXT_133);
    stringBuffer.append(genFeature.getEDefault());
    stringBuffer.append(TEXT_447);
    stringBuffer.append(genClass.getFlagsField(genFeature));
    stringBuffer.append(TEXT_117);
    stringBuffer.append(genFeature.getUpperName());
    stringBuffer.append(TEXT_448);
    stringBuffer.append(genClass.getFlagsField(genFeature));
    stringBuffer.append(TEXT_449);
    stringBuffer.append(genFeature.getUpperName());
    stringBuffer.append(TEXT_450);
    } else {
    stringBuffer.append(TEXT_116);
    stringBuffer.append(genClass.getFlagsField(genFeature));
    stringBuffer.append(TEXT_76);
    stringBuffer.append(genClass.getFlagsField(genFeature));
    stringBuffer.append(TEXT_453);
    stringBuffer.append(genFeature.getUpperName());
    stringBuffer.append(TEXT_454);
    stringBuffer.append(genFeature.getUpperName());
    stringBuffer.append(TEXT_527);
    }
    } else if (!genModel.isVirtualDelegation() || genFeature.isPrimitiveType()) {
    stringBuffer.append(TEXT_116);
    stringBuffer.append(genFeature.getSafeName());
    stringBuffer.append(TEXT_76);
    stringBuffer.append(genFeature.getEDefault());
    stringBuffer.append(TEXT_7);
    }
    if (!genModel.isVirtualDelegation() || genFeature.isPrimitiveType()) {
    if (genClass.isESetFlag(genFeature)) {
    stringBuffer.append(TEXT_116);
    stringBuffer.append(genClass.getESetFlagsField(genFeature));
    stringBuffer.append(TEXT_449);
    stringBuffer.append(genFeature.getUpperName());
    stringBuffer.append(TEXT_387);
    } else {
    stringBuffer.append(TEXT_116);
    stringBuffer.append(genFeature.getUncapName());
    stringBuffer.append(TEXT_504);
    }
    }
    if (!genModel.isSuppressNotification()) {
    stringBuffer.append(TEXT_459);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.impl.ENotificationImpl"));
    stringBuffer.append(TEXT_241);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.common.notify.Notification"));
    stringBuffer.append(TEXT_506);
    stringBuffer.append(genClass.getQualifiedFeatureID(genFeature));
    stringBuffer.append(positiveOffsetCorrection);
    stringBuffer.append(TEXT_197);
    if (genModel.isVirtualDelegation() && !genFeature.isPrimitiveType()) {
    stringBuffer.append(TEXT_507);
    stringBuffer.append(genFeature.getCapName());
    stringBuffer.append(TEXT_528);
    stringBuffer.append(genFeature.getEDefault());
    } else {
    stringBuffer.append(TEXT_395);
    stringBuffer.append(genFeature.getCapName());
    }
    stringBuffer.append(TEXT_197);
    stringBuffer.append(genFeature.getEDefault());
    stringBuffer.append(TEXT_197);
    if (genModel.isVirtualDelegation() && !genFeature.isPrimitiveType()) {
    stringBuffer.append(TEXT_396);
    } else {
    stringBuffer.append(TEXT_395);
    stringBuffer.append(genFeature.getCapName());
    stringBuffer.append(TEXT_398);
    }
    stringBuffer.append(TEXT_244);
    }
    }
    }
    } else if (genFeature.hasDelegateFeature()) { GenFeature delegateFeature = genFeature.getDelegateFeature();
    if (delegateFeature.isWrappedFeatureMapType()) {
    stringBuffer.append(TEXT_463);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.util.FeatureMap"));
    stringBuffer.append(TEXT_259);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.util.FeatureMap"));
    stringBuffer.append(TEXT_260);
    stringBuffer.append(delegateFeature.getAccessorName());
    stringBuffer.append(TEXT_529);
    stringBuffer.append(genFeature.getQualifiedFeatureAccessor());
    stringBuffer.append(TEXT_205);
    } else {
    stringBuffer.append(TEXT_463);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.util.FeatureMap"));
    stringBuffer.append(TEXT_402);
    stringBuffer.append(delegateFeature.getAccessorName());
    stringBuffer.append(TEXT_530);
    stringBuffer.append(genFeature.getQualifiedFeatureAccessor());
    stringBuffer.append(TEXT_205);
    }
    } else if (genClass.getUnsetAccessorOperation(genFeature) != null) {
    stringBuffer.append(TEXT_116);
    stringBuffer.append(genClass.getUnsetAccessorOperation(genFeature).getBody(genModel.getIndentation(stringBuffer)));
    } else {
    stringBuffer.append(TEXT_8);
    stringBuffer.append(TEXT_531);
     GAwareGenFeature gGenFeature = (GAwareGenFeature)genFeature; 
     TargetFeaturePath tgtFeaturePath = gGenFeature.getTargetFeaturePath(genClass); 
     if(tgtFeaturePath.getClass().equals(NoMappingTargetFeaturePath.class)){
    stringBuffer.append(TEXT_126);
    stringBuffer.append(TEXT_532);
     } else { 
    stringBuffer.append(TEXT_126);
     if(tgtFeaturePath.getSegmentCount() == 1) { 
    stringBuffer.append(TEXT_116);
    stringBuffer.append( tgtFeaturePath.getLastFeature().getUnsetAccessorName() );
    stringBuffer.append(TEXT_533);
     } else if(tgtFeaturePath.getSegmentCount() > 1) { 
    stringBuffer.append(TEXT_116);
     GAwareGenFeature previousFeature = null; 
     for(GAwareGenFeature feature : tgtFeaturePath.getHeadFeatures()) { 
    stringBuffer.append(TEXT_126);
    stringBuffer.append(feature.getImportedType(genClass));
    stringBuffer.append(TEXT_65);
    stringBuffer.append(feature.getName());
    stringBuffer.append(TEXT_76);
     if(previousFeature != null) {
    stringBuffer.append( previousFeature.getName() );
    stringBuffer.append(TEXT_166);
    }
    stringBuffer.append( feature.getGetAccessor() );
    stringBuffer.append(TEXT_324);
    stringBuffer.append(feature.getName());
    stringBuffer.append(TEXT_343);
    stringBuffer.append(feature.getName());
    stringBuffer.append(TEXT_76);
    stringBuffer.append(GenModels.getImportedFactoryName(genModel));
    stringBuffer.append(TEXT_344);
    stringBuffer.append(feature.getImportedType(genClass));
    stringBuffer.append(TEXT_345);
     previousFeature = feature; 
     } 
    stringBuffer.append(TEXT_116);
    stringBuffer.append( previousFeature.getName() );
    stringBuffer.append(TEXT_166);
    stringBuffer.append( tgtFeaturePath.getLastFeature().getUnsetAccessorName() );
    stringBuffer.append(TEXT_533);
    } else { 
    stringBuffer.append(TEXT_534);
     } 
     } 
    stringBuffer.append(TEXT_362);
    //GEastadl/gUnsetImplementation.insert.javajetinc
    //Class/unsetGenFeature.todo.override.javajetinc
    }
    stringBuffer.append(TEXT_151);
    }
    //Class/unsetGenFeature.override.javajetinc
    }
    if (genFeature.isIsSet() && (isImplementation || !genFeature.isSuppressedIsSetVisibility())) {
    if (isInterface) {
    stringBuffer.append(TEXT_535);
    stringBuffer.append(genClass.getQualifiedInterfaceName());
    stringBuffer.append(TEXT_17);
    stringBuffer.append(genFeature.getGetAccessor());
    stringBuffer.append(TEXT_18);
    stringBuffer.append(genFeature.getFormattedName());
    stringBuffer.append(TEXT_56);
    stringBuffer.append(genFeature.getFeatureKind());
    stringBuffer.append(TEXT_536);
    stringBuffer.append(TEXT_537);
    stringBuffer.append(genFeature.getFormattedName());
    stringBuffer.append(TEXT_182);
    stringBuffer.append(genFeature.getFeatureKind());
    stringBuffer.append(TEXT_536);
    if (genFeature.isChangeable() && !genFeature.isSuppressedUnsetVisibility()) {
    stringBuffer.append(TEXT_189);
    stringBuffer.append(((GAwareGenFeature)genFeature).getAccessorName());
    stringBuffer.append(TEXT_23);
    }
    stringBuffer.append(TEXT_189);
    stringBuffer.append(genFeature.getGetAccessor());
    stringBuffer.append(TEXT_23);
    if (!genFeature.isListType() && genFeature.isChangeable() && !genFeature.isSuppressedSetVisibility()) {
    stringBuffer.append(TEXT_190);
    stringBuffer.append(genFeature.getAccessorName());
    stringBuffer.append(TEXT_91);
    stringBuffer.append(genFeature.getRawImportedBoundType());
    stringBuffer.append(TEXT_85);
    }
    stringBuffer.append(TEXT_194);
    //Class/isSetGenFeature.javadoc.override.javajetinc
    } else {
    stringBuffer.append(TEXT_124);
    if (isJDK50) { //Class/isSetGenFeature.annotations.insert.javajetinc
    }
    }
    if (!isImplementation) {
    stringBuffer.append(TEXT_538);
    stringBuffer.append(((GAwareGenFeature)genFeature).getIsSetAccessorName());
    stringBuffer.append(TEXT_127);
    } else {
    stringBuffer.append(TEXT_539);
    stringBuffer.append(((GAwareGenFeature)genFeature).getIsSetAccessorName());
    if (genClass.hasCollidingIsSetAccessorOperation(genFeature)) {
    stringBuffer.append(TEXT_195);
    }
    stringBuffer.append(TEXT_129);
    if (genModel.isDynamicDelegation()) {
    stringBuffer.append(TEXT_540);
    stringBuffer.append(genClass.getQualifiedFeatureID(genFeature));
    stringBuffer.append(TEXT_197);
    stringBuffer.append(genFeature.getQualifiedFeatureAccessor());
    stringBuffer.append(TEXT_205);
    } else if (genModel.isReflectiveDelegation()) {
    stringBuffer.append(TEXT_541);
    stringBuffer.append(genFeature.getQualifiedFeatureAccessor());
    stringBuffer.append(TEXT_205);
    } else if (genFeature.hasSettingDelegate()) {
    stringBuffer.append(TEXT_147);
    stringBuffer.append(genFeature.getUpperName());
    stringBuffer.append(TEXT_542);
    } else if (!genFeature.isVolatile()) {
    if (genFeature.isListType()) {
    if (genModel.isVirtualDelegation()) {
    stringBuffer.append(TEXT_116);
    stringBuffer.append(genFeature.getImportedType(genClass));
    stringBuffer.append(TEXT_65);
    stringBuffer.append(genFeature.getSafeName());
    stringBuffer.append(TEXT_203);
    stringBuffer.append(genFeature.getImportedType(genClass));
    stringBuffer.append(TEXT_204);
    stringBuffer.append(genClass.getQualifiedFeatureID(genFeature));
    stringBuffer.append(positiveOffsetCorrection);
    stringBuffer.append(TEXT_205);
    }
    stringBuffer.append(TEXT_147);
    stringBuffer.append(genFeature.getSafeName());
    stringBuffer.append(TEXT_543);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.util.InternalEList"));
    stringBuffer.append(TEXT_518);
    stringBuffer.append(singleWildcard);
    stringBuffer.append(TEXT_85);
    stringBuffer.append(genFeature.getSafeName());
    stringBuffer.append(TEXT_544);
    } else {
    if (genModel.isVirtualDelegation() && !genFeature.isPrimitiveType()) {
    stringBuffer.append(TEXT_545);
    stringBuffer.append(genClass.getQualifiedFeatureID(genFeature));
    stringBuffer.append(positiveOffsetCorrection);
    stringBuffer.append(TEXT_205);
    } else if (genClass.isESetFlag(genFeature)) {
    stringBuffer.append(TEXT_246);
    stringBuffer.append(genClass.getESetFlagsField(genFeature));
    stringBuffer.append(TEXT_247);
    stringBuffer.append(genFeature.getUpperName());
    stringBuffer.append(TEXT_386);
    } else {
    stringBuffer.append(TEXT_147);
    stringBuffer.append(genFeature.getUncapName());
    stringBuffer.append(TEXT_389);
    }
    }
    } else if (genFeature.hasDelegateFeature()) { GenFeature delegateFeature = genFeature.getDelegateFeature();
    if (delegateFeature.isWrappedFeatureMapType()) {
    stringBuffer.append(TEXT_546);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.util.FeatureMap"));
    stringBuffer.append(TEXT_259);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.util.FeatureMap"));
    stringBuffer.append(TEXT_260);
    stringBuffer.append(delegateFeature.getAccessorName());
    stringBuffer.append(TEXT_547);
    stringBuffer.append(genFeature.getQualifiedFeatureAccessor());
    stringBuffer.append(TEXT_205);
    } else {
    stringBuffer.append(TEXT_546);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.util.FeatureMap"));
    stringBuffer.append(TEXT_402);
    stringBuffer.append(delegateFeature.getAccessorName());
    stringBuffer.append(TEXT_548);
    stringBuffer.append(genFeature.getQualifiedFeatureAccessor());
    stringBuffer.append(TEXT_205);
    }
    } else if (genClass.getIsSetAccessorOperation(genFeature) != null) {
    stringBuffer.append(TEXT_116);
    stringBuffer.append(genClass.getIsSetAccessorOperation(genFeature).getBody(genModel.getIndentation(stringBuffer)));
    } else {
    stringBuffer.append(TEXT_8);
    stringBuffer.append(TEXT_549);
     GAwareGenFeature gGenFeature = (GAwareGenFeature)genFeature; 
     TargetFeaturePath tgtFeaturePath = gGenFeature.getTargetFeaturePath(genClass); 
     if(tgtFeaturePath.getClass().equals(NoMappingTargetFeaturePath.class)){
    stringBuffer.append(TEXT_126);
    stringBuffer.append(TEXT_532);
     } else { 
    stringBuffer.append(TEXT_126);
     if(tgtFeaturePath.getSegmentCount() == 1) { 
    stringBuffer.append(TEXT_147);
    stringBuffer.append( tgtFeaturePath.getLastFeature().getIsSetAccessorName() );
    stringBuffer.append(TEXT_550);
     } else if(tgtFeaturePath.getSegmentCount() > 1) { 
    stringBuffer.append(TEXT_116);
     GAwareGenFeature previousFeature = null; 
     for(GAwareGenFeature feature : tgtFeaturePath.getHeadFeatures()) { 
    stringBuffer.append(TEXT_126);
    stringBuffer.append(feature.getImportedType(genClass));
    stringBuffer.append(TEXT_65);
    stringBuffer.append(feature.getName());
    stringBuffer.append(TEXT_76);
     if(previousFeature != null) {
    stringBuffer.append( previousFeature.getName() );
    stringBuffer.append(TEXT_166);
    }
    stringBuffer.append( feature.getGetAccessor() );
    stringBuffer.append(TEXT_324);
    stringBuffer.append(feature.getName());
    stringBuffer.append(TEXT_343);
    stringBuffer.append(feature.getName());
    stringBuffer.append(TEXT_76);
    stringBuffer.append(GenModels.getImportedFactoryName(genModel));
    stringBuffer.append(TEXT_344);
    stringBuffer.append(feature.getImportedType(genClass));
    stringBuffer.append(TEXT_345);
     previousFeature = feature; 
     } 
    stringBuffer.append(TEXT_147);
    stringBuffer.append( previousFeature.getName() );
    stringBuffer.append(TEXT_166);
    stringBuffer.append( tgtFeaturePath.getLastFeature().getIsSetAccessorName() );
    stringBuffer.append(TEXT_533);
    } else { 
    stringBuffer.append(TEXT_534);
     } 
     } 
    stringBuffer.append(TEXT_362);
    //GEastadl/gIsSetImplementation.insert.javajetinc
    //Class/isSetGenFeature.todo.override.javajetinc
    }
    stringBuffer.append(TEXT_151);
    }
    //Class/isSetGenFeature.override.javajetinc
    }
    //Class/genFeature.override.javajetinc
    }//for
    }}.run();
    for (GenOperation genOperation : (isImplementation ? genClass.getImplementedGenOperations() : genClass.getDeclaredGenOperations())) {
    if (isImplementation) {
    if (genOperation.isInvariant() && genOperation.hasInvariantExpression()) {
    stringBuffer.append(TEXT_551);
    stringBuffer.append(genOperation.getName());
    stringBuffer.append(TEXT_91);
    stringBuffer.append(genOperation.getParameterTypes(", "));
    stringBuffer.append(TEXT_552);
    stringBuffer.append(genOperation.getFormattedName());
    stringBuffer.append(TEXT_553);
    stringBuffer.append(genOperation.getName());
    stringBuffer.append(TEXT_91);
    stringBuffer.append(genOperation.getParameterTypes(", "));
    stringBuffer.append(TEXT_554);
    stringBuffer.append(genModel.getImportedName("java.lang.String"));
    stringBuffer.append(TEXT_65);
    stringBuffer.append(CodeGenUtil.upperName(genClass.getUniqueName(genOperation), genModel.getLocale()));
    stringBuffer.append(TEXT_555);
    stringBuffer.append(genOperation.getInvariantExpression("\t\t"));
    stringBuffer.append(TEXT_44);
    stringBuffer.append(genModel.getNonNLS());
    stringBuffer.append(TEXT_8);
    } else if (genOperation.hasInvocationDelegate()) {
    stringBuffer.append(TEXT_556);
    stringBuffer.append(genOperation.getName());
    stringBuffer.append(TEXT_91);
    stringBuffer.append(genOperation.getParameterTypes(", "));
    stringBuffer.append(TEXT_552);
    stringBuffer.append(genOperation.getFormattedName());
    stringBuffer.append(TEXT_557);
    stringBuffer.append(genOperation.getName());
    stringBuffer.append(TEXT_91);
    stringBuffer.append(genOperation.getParameterTypes(", "));
    stringBuffer.append(TEXT_554);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.EOperation"));
    stringBuffer.append(TEXT_558);
    stringBuffer.append(CodeGenUtil.upperName(genClass.getUniqueName(genOperation), genModel.getLocale()));
    stringBuffer.append(TEXT_559);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.EOperation"));
    stringBuffer.append(TEXT_62);
    stringBuffer.append(genOperation.getQualifiedOperationAccessor());
    stringBuffer.append(TEXT_560);
    }
    }
    if (isInterface) {
    stringBuffer.append(TEXT_561);
    stringBuffer.append(TEXT_513);
    if (genOperation.hasDocumentation() || genOperation.hasParameterDocumentation()) {
    stringBuffer.append(TEXT_562);
    if (genOperation.hasDocumentation()) {
    stringBuffer.append(TEXT_563);
    stringBuffer.append(genOperation.getDocumentation(genModel.getIndentation(stringBuffer)));
    }
    for (GenParameter genParameter : genOperation.getGenParameters()) {
    if (genParameter.hasDocumentation()) { String documentation = genParameter.getDocumentation("");
    if (documentation.contains("\n") || documentation.contains("\r")) {
    stringBuffer.append(TEXT_564);
    stringBuffer.append(genParameter.getName());
    stringBuffer.append(TEXT_565);
    stringBuffer.append(genParameter.getDocumentation(genModel.getIndentation(stringBuffer)));
    } else {
    stringBuffer.append(TEXT_564);
    stringBuffer.append(genParameter.getName());
    stringBuffer.append(TEXT_65);
    stringBuffer.append(genParameter.getDocumentation(genModel.getIndentation(stringBuffer)));
    }
    }
    }
    stringBuffer.append(TEXT_186);
    }
    if (!genModel.isSuppressEMFModelTags()) { boolean first = true; for (StringTokenizer stringTokenizer = new StringTokenizer(genOperation.getModelInfo(), "\n\r"); stringTokenizer.hasMoreTokens(); ) { String modelInfo = stringTokenizer.nextToken(); if (first) { first = false;
    stringBuffer.append(TEXT_191);
    stringBuffer.append(modelInfo);
    } else {
    stringBuffer.append(TEXT_192);
    stringBuffer.append(modelInfo);
    }} if (first) {
    stringBuffer.append(TEXT_193);
    }}
    stringBuffer.append(TEXT_194);
    //Class/genOperation.javadoc.override.javajetinc
    } else {
    stringBuffer.append(TEXT_124);
    if (isJDK50) { //Class/genOperation.annotations.insert.javajetinc
    }
    }
    if (!isImplementation) {
    stringBuffer.append(TEXT_126);
    stringBuffer.append(genOperation.getTypeParameters(genClass));
    stringBuffer.append(genOperation.getImportedType(genClass));
    stringBuffer.append(TEXT_65);
    stringBuffer.append(genOperation.getName());
    stringBuffer.append(TEXT_91);
    stringBuffer.append(genOperation.getParameters(genClass));
    stringBuffer.append(TEXT_85);
    stringBuffer.append(genOperation.getThrows(genClass));
    stringBuffer.append(TEXT_49);
    } else {
    stringBuffer.append(TEXT_128);
    stringBuffer.append(genOperation.getTypeParameters(genClass));
    stringBuffer.append(genOperation.getImportedType(genClass));
    stringBuffer.append(TEXT_65);
    stringBuffer.append(genOperation.getName());
    stringBuffer.append(TEXT_91);
    stringBuffer.append(genOperation.getParameters(genClass));
    stringBuffer.append(TEXT_85);
    stringBuffer.append(genOperation.getThrows(genClass));
    stringBuffer.append(TEXT_566);
    if (genOperation.hasBody()) {
    stringBuffer.append(TEXT_116);
    stringBuffer.append(genOperation.getBody(genModel.getIndentation(stringBuffer)));
    } else if (genOperation.isInvariant()) {GenClass opClass = genOperation.getGenClass(); String diagnostics = genOperation.getGenParameters().get(0).getName(); String context = genOperation.getGenParameters().get(1).getName();
    if (genOperation.hasInvariantExpression()) {
    stringBuffer.append(TEXT_567);
    stringBuffer.append(opClass.getGenPackage().getImportedValidatorClassName());
    stringBuffer.append(TEXT_568);
    stringBuffer.append(genClass.getQualifiedClassifierAccessor());
    stringBuffer.append(TEXT_569);
    stringBuffer.append(diagnostics);
    stringBuffer.append(TEXT_570);
    stringBuffer.append(context);
    stringBuffer.append(TEXT_571);
    stringBuffer.append(genOperation.getValidationDelegate());
    stringBuffer.append(TEXT_572);
    stringBuffer.append(genModel.getNonNLS());
    stringBuffer.append(TEXT_573);
    stringBuffer.append(genOperation.getQualifiedOperationAccessor());
    stringBuffer.append(TEXT_570);
    stringBuffer.append(CodeGenUtil.upperName(genClass.getUniqueName(genOperation), genModel.getLocale()));
    stringBuffer.append(TEXT_574);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.common.util.Diagnostic"));
    stringBuffer.append(TEXT_575);
    stringBuffer.append(opClass.getGenPackage().getImportedValidatorClassName());
    stringBuffer.append(TEXT_576);
    stringBuffer.append(opClass.getGenPackage().getImportedValidatorClassName());
    stringBuffer.append(TEXT_166);
    stringBuffer.append(opClass.getOperationID(genOperation));
    stringBuffer.append(TEXT_205);
    } else {
    stringBuffer.append(TEXT_577);
    stringBuffer.append(diagnostics);
    stringBuffer.append(TEXT_578);
    stringBuffer.append(diagnostics);
    stringBuffer.append(TEXT_579);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.common.util.BasicDiagnostic"));
    stringBuffer.append(TEXT_580);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.common.util.Diagnostic"));
    stringBuffer.append(TEXT_581);
    stringBuffer.append(opClass.getGenPackage().getImportedValidatorClassName());
    stringBuffer.append(TEXT_582);
    stringBuffer.append(opClass.getGenPackage().getImportedValidatorClassName());
    stringBuffer.append(TEXT_166);
    stringBuffer.append(opClass.getOperationID(genOperation));
    stringBuffer.append(TEXT_583);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.plugin.EcorePlugin"));
    stringBuffer.append(TEXT_584);
    stringBuffer.append(genOperation.getName());
    stringBuffer.append(TEXT_585);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.util.EObjectValidator"));
    stringBuffer.append(TEXT_586);
    stringBuffer.append(context);
    stringBuffer.append(TEXT_587);
    stringBuffer.append(genModel.getNonNLS());
    stringBuffer.append(genModel.getNonNLS(2));
    stringBuffer.append(TEXT_588);
    }
    } else if (genOperation.hasInvocationDelegate()) { int size = genOperation.getGenParameters().size();
    stringBuffer.append(TEXT_589);
    if (genOperation.isVoid()) {
    stringBuffer.append(TEXT_209);
    stringBuffer.append(CodeGenUtil.upperName(genClass.getUniqueName(genOperation), genModel.getLocale()));
    stringBuffer.append(TEXT_590);
    if (size > 0) {
    stringBuffer.append(TEXT_412);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.common.util.BasicEList"));
    stringBuffer.append(TEXT_591);
    stringBuffer.append(size);
    stringBuffer.append(TEXT_197);
    stringBuffer.append(genOperation.getParametersArray(genClass));
    stringBuffer.append(TEXT_85);
    } else {
    stringBuffer.append(TEXT_592);
    }
    stringBuffer.append(TEXT_205);
    } else {
    stringBuffer.append(TEXT_593);
    if (!isJDK50 && genOperation.isPrimitiveType()) {
    stringBuffer.append(TEXT_91);
    }
    stringBuffer.append(TEXT_91);
    stringBuffer.append(genOperation.getObjectType(genClass));
    stringBuffer.append(TEXT_85);
    stringBuffer.append(CodeGenUtil.upperName(genClass.getUniqueName(genOperation), genModel.getLocale()));
    stringBuffer.append(TEXT_590);
    if (size > 0) {
    stringBuffer.append(TEXT_412);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.common.util.BasicEList"));
    stringBuffer.append(TEXT_591);
    stringBuffer.append(size);
    stringBuffer.append(TEXT_197);
    stringBuffer.append(genOperation.getParametersArray(genClass));
    stringBuffer.append(TEXT_85);
    } else {
    stringBuffer.append(TEXT_592);
    }
    stringBuffer.append(TEXT_85);
    if (!isJDK50 && genOperation.isPrimitiveType()) {
    stringBuffer.append(TEXT_199);
    stringBuffer.append(genOperation.getPrimitiveValueFunction());
    stringBuffer.append(TEXT_23);
    }
    stringBuffer.append(TEXT_7);
    }
    stringBuffer.append(TEXT_594);
    stringBuffer.append(genModel.getImportedName(isGWT ? "org.eclipse.emf.common.util.InvocationTargetException" : "java.lang.reflect.InvocationTargetException"));
    stringBuffer.append(TEXT_595);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.common.util.WrappedException"));
    stringBuffer.append(TEXT_596);
    } else {
     GOperation gOp = GOperationFactory.INSTANCE.createGOperation(genOperation);
     if(gOp != null) { 
    stringBuffer.append(TEXT_126);
     GenFeature tgtFeature = gOp.getTargetFeature(genClass);
    	if(gOp.getOperationType() == GOperation.GOpType.GET) { 
    stringBuffer.append(TEXT_597);
     if(tgtFeature.isListType() && !tgtFeature.isFeatureMapType()) { 
    stringBuffer.append(TEXT_116);
    stringBuffer.append(genOperation.getRawImportedType());
    stringBuffer.append(TEXT_274);
    stringBuffer.append(tgtFeature.getGetAccessor());
    stringBuffer.append(TEXT_598);
     } else { 
    stringBuffer.append(TEXT_147);
    stringBuffer.append(tgtFeature.getGetAccessor());
    stringBuffer.append(TEXT_599);
     } 
    stringBuffer.append(TEXT_362);
    	} else if(gOp.getOperationType() == GOperation.GOpType.SET) { 
    stringBuffer.append(TEXT_597);
     boolean isEastadlType = tgtFeature.getEcoreFeature().getEGenericType().getERawType().getInstanceClass() == null; 
    stringBuffer.append(TEXT_65);
     String paramName = genOperation.getParameterNames("~"); 
     if(isEastadlType) { 
    stringBuffer.append(TEXT_600);
    stringBuffer.append(tgtFeature.getAccessorName());
    stringBuffer.append(TEXT_258);
    stringBuffer.append(tgtFeature.getImportedType());
    stringBuffer.append(TEXT_85);
    stringBuffer.append(paramName);
    stringBuffer.append(TEXT_205);
     } else if(!tgtFeature.isListType()) { 
    stringBuffer.append(TEXT_600);
    stringBuffer.append(tgtFeature.getAccessorName());
    stringBuffer.append(TEXT_91);
    stringBuffer.append(paramName);
    stringBuffer.append(TEXT_601);
     } 
    stringBuffer.append(TEXT_602);
    	} else { 
    stringBuffer.append(TEXT_603);
    	}
    stringBuffer.append(TEXT_604);
     } else { 
    stringBuffer.append(TEXT_605);
     } 
    //Class/implementedGenOperation.todo.override.javajetinc
    }
    stringBuffer.append(TEXT_151);
    }
    //Class/implementedGenOperation.override.javajetinc
    }//for
    if (isImplementation && !genModel.isReflectiveDelegation() && genClass.implementsAny(GenClasses.getEInverseAddGenFeatures(genClass))) {
    stringBuffer.append(TEXT_124);
    if (genModel.useGenerics()) {
    for (GenFeature genFeature : GenClasses.getEInverseAddGenFeatures(genClass)) {
    if (genFeature.isUncheckedCast(genClass)) {
    stringBuffer.append(TEXT_68);
    break; }
    }
    }
    if (genModel.useClassOverrideAnnotation()) {
    stringBuffer.append(TEXT_121);
    }
    stringBuffer.append(TEXT_128);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.common.notify.NotificationChain"));
    stringBuffer.append(TEXT_606);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.InternalEObject"));
    stringBuffer.append(TEXT_607);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.common.notify.NotificationChain"));
    stringBuffer.append(TEXT_608);
    stringBuffer.append(negativeOffsetCorrection);
    stringBuffer.append(TEXT_609);
    for (GenFeature genFeature : GenClasses.getEInverseAddGenFeatures(genClass)) {
    stringBuffer.append(TEXT_610);
    stringBuffer.append(genClass.getQualifiedFeatureID(genFeature));
    stringBuffer.append(TEXT_611);
    if (genFeature.isListType()) { String cast = "("  + genModel.getImportedName("org.eclipse.emf.ecore.util.InternalEList") + (!genModel.useGenerics() ? ")" : "<" + genModel.getImportedName("org.eclipse.emf.ecore.InternalEObject") + ">)(" + genModel.getImportedName("org.eclipse.emf.ecore.util.InternalEList") + "<?>)");
    if (genFeature.isMapType() && genFeature.isEffectiveSuppressEMFTypes()) {
    stringBuffer.append(TEXT_612);
    stringBuffer.append(cast);
    stringBuffer.append(TEXT_91);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.common.util.EMap"));
    stringBuffer.append(TEXT_613);
    stringBuffer.append(genFeature.getImportedMapTemplateArguments(genClass));
    stringBuffer.append(TEXT_85);
    stringBuffer.append(genFeature.getGetAccessor());
    stringBuffer.append(TEXT_614);
    } else {
    stringBuffer.append(TEXT_615);
    stringBuffer.append(cast);
    stringBuffer.append(genFeature.getGetAccessor());
    stringBuffer.append(TEXT_616);
    }
    } else if (genFeature.isContainer()) {
    stringBuffer.append(TEXT_617);
    if (genFeature.isBasicSet()) {
    stringBuffer.append(TEXT_618);
    stringBuffer.append(genFeature.getAccessorName());
    stringBuffer.append(TEXT_258);
    stringBuffer.append(genFeature.getImportedType(genClass));
    stringBuffer.append(TEXT_619);
    } else {
    stringBuffer.append(TEXT_620);
    stringBuffer.append(genClass.getQualifiedFeatureID(genFeature));
    stringBuffer.append(positiveOffsetCorrection);
    stringBuffer.append(TEXT_374);
    }
    } else {
    if (genClass.getImplementingGenModel(genFeature).isVirtualDelegation()) {
    stringBuffer.append(TEXT_222);
    stringBuffer.append(genFeature.getImportedType(genClass));
    stringBuffer.append(TEXT_65);
    stringBuffer.append(genFeature.getSafeName());
    stringBuffer.append(TEXT_203);
    stringBuffer.append(genFeature.getImportedType(genClass));
    stringBuffer.append(TEXT_204);
    stringBuffer.append(genClass.getQualifiedFeatureID(genFeature));
    stringBuffer.append(positiveOffsetCorrection);
    stringBuffer.append(TEXT_205);
    } else if (genFeature.isVolatile() || genClass.getImplementingGenModel(genFeature).isDynamicDelegation()) {
    stringBuffer.append(TEXT_222);
    stringBuffer.append(genFeature.getImportedType(genClass));
    stringBuffer.append(TEXT_65);
    stringBuffer.append(genFeature.getSafeName());
    stringBuffer.append(TEXT_76);
    if (genFeature.isResolveProxies()) {
    stringBuffer.append(TEXT_498);
    stringBuffer.append(genFeature.getAccessorName());
    } else {
    stringBuffer.append(genFeature.getGetAccessor());
    }
    stringBuffer.append(TEXT_599);
    }
    stringBuffer.append(TEXT_621);
    stringBuffer.append(genFeature.getSafeName());
    stringBuffer.append(TEXT_432);
    if (genFeature.isEffectiveContains()) {
    stringBuffer.append(TEXT_622);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.InternalEObject"));
    stringBuffer.append(TEXT_85);
    stringBuffer.append(genFeature.getSafeName());
    stringBuffer.append(TEXT_434);
    stringBuffer.append(genClass.getQualifiedFeatureID(genFeature));
    stringBuffer.append(negativeOffsetCorrection);
    stringBuffer.append(TEXT_234);
    } else { GenFeature reverseFeature = genFeature.getReverse(); GenClass targetClass = reverseFeature.getGenClass(); String reverseOffsetCorrection = targetClass.hasOffsetCorrection() ? " + " + genClass.getOffsetCorrectionField(genFeature) : "";
    stringBuffer.append(TEXT_622);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.InternalEObject"));
    stringBuffer.append(TEXT_85);
    stringBuffer.append(genFeature.getSafeName());
    stringBuffer.append(TEXT_437);
    stringBuffer.append(targetClass.getQualifiedFeatureID(reverseFeature));
    stringBuffer.append(reverseOffsetCorrection);
    stringBuffer.append(TEXT_197);
    stringBuffer.append(targetClass.getRawImportedInterfaceName());
    stringBuffer.append(TEXT_237);
    }
    stringBuffer.append(TEXT_618);
    stringBuffer.append(genFeature.getAccessorName());
    stringBuffer.append(TEXT_258);
    stringBuffer.append(genFeature.getImportedType(genClass));
    stringBuffer.append(TEXT_619);
    }
    }
    stringBuffer.append(TEXT_446);
    if (genModel.isMinimalReflectiveMethods()) {
    stringBuffer.append(TEXT_623);
    } else {
    stringBuffer.append(TEXT_624);
    }
    stringBuffer.append(TEXT_151);
    }
    if (isImplementation && !genModel.isReflectiveDelegation() && genClass.implementsAny(genClass.getEInverseRemoveGenFeatures())) {
    stringBuffer.append(TEXT_124);
    if (genModel.useClassOverrideAnnotation()) {
    stringBuffer.append(TEXT_121);
    }
    stringBuffer.append(TEXT_128);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.common.notify.NotificationChain"));
    stringBuffer.append(TEXT_625);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.InternalEObject"));
    stringBuffer.append(TEXT_607);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.common.notify.NotificationChain"));
    stringBuffer.append(TEXT_608);
    stringBuffer.append(negativeOffsetCorrection);
    stringBuffer.append(TEXT_609);
    for (GenFeature genFeature : genClass.getEInverseRemoveGenFeatures()) {
    stringBuffer.append(TEXT_610);
    stringBuffer.append(genClass.getQualifiedFeatureID(genFeature));
    stringBuffer.append(TEXT_611);
    if (genFeature.isListType()) {
    if (genFeature.isMapType() && genFeature.isEffectiveSuppressEMFTypes()) {
    stringBuffer.append(TEXT_612);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.util.InternalEList"));
    stringBuffer.append(singleWildcard);
    stringBuffer.append(TEXT_367);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.common.util.EMap"));
    stringBuffer.append(TEXT_613);
    stringBuffer.append(genFeature.getImportedMapTemplateArguments(genClass));
    stringBuffer.append(TEXT_85);
    stringBuffer.append(genFeature.getGetAccessor());
    stringBuffer.append(TEXT_626);
    } else if (genFeature.isWrappedFeatureMapType()) {
    stringBuffer.append(TEXT_612);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.util.InternalEList"));
    stringBuffer.append(singleWildcard);
    stringBuffer.append(TEXT_367);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.util.FeatureMap"));
    stringBuffer.append(TEXT_627);
    stringBuffer.append(genFeature.getGetAccessor());
    stringBuffer.append(TEXT_628);
    } else {
    stringBuffer.append(TEXT_612);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.util.InternalEList"));
    stringBuffer.append(singleWildcard);
    stringBuffer.append(TEXT_85);
    stringBuffer.append(genFeature.getGetAccessor());
    stringBuffer.append(TEXT_629);
    }
    } else if (genFeature.isContainer() && !genFeature.isBasicSet()) {
    stringBuffer.append(TEXT_630);
    stringBuffer.append(genClass.getQualifiedFeatureID(genFeature));
    stringBuffer.append(positiveOffsetCorrection);
    stringBuffer.append(TEXT_374);
    } else if (genFeature.isUnsettable()) {
    stringBuffer.append(TEXT_631);
    stringBuffer.append(genFeature.getAccessorName());
    stringBuffer.append(TEXT_632);
    } else {
    stringBuffer.append(TEXT_618);
    stringBuffer.append(genFeature.getAccessorName());
    stringBuffer.append(TEXT_633);
    }
    }
    stringBuffer.append(TEXT_446);
    if (genModel.isMinimalReflectiveMethods()) {
    stringBuffer.append(TEXT_634);
    } else {
    stringBuffer.append(TEXT_635);
    }
    stringBuffer.append(TEXT_151);
    }
    if (isImplementation && !genModel.isReflectiveDelegation() && genClass.implementsAny(genClass.getEBasicRemoveFromContainerGenFeatures())) {
    stringBuffer.append(TEXT_124);
    if (genModel.useClassOverrideAnnotation()) {
    stringBuffer.append(TEXT_121);
    }
    stringBuffer.append(TEXT_128);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.common.notify.NotificationChain"));
    stringBuffer.append(TEXT_636);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.common.notify.NotificationChain"));
    stringBuffer.append(TEXT_637);
    stringBuffer.append(negativeOffsetCorrection);
    stringBuffer.append(TEXT_609);
    for (GenFeature genFeature : genClass.getEBasicRemoveFromContainerGenFeatures()) {
    GenFeature reverseFeature = genFeature.getReverse(); GenClass targetClass = reverseFeature.getGenClass(); String reverseOffsetCorrection = targetClass.hasOffsetCorrection() ? " + " + genClass.getOffsetCorrectionField(genFeature) : "";
    stringBuffer.append(TEXT_610);
    stringBuffer.append(genClass.getQualifiedFeatureID(genFeature));
    stringBuffer.append(TEXT_638);
    stringBuffer.append(targetClass.getQualifiedFeatureID(reverseFeature));
    stringBuffer.append(reverseOffsetCorrection);
    stringBuffer.append(TEXT_197);
    stringBuffer.append(targetClass.getRawImportedInterfaceName());
    stringBuffer.append(TEXT_237);
    }
    stringBuffer.append(TEXT_446);
    if (genModel.isMinimalReflectiveMethods()) {
    stringBuffer.append(TEXT_639);
    } else {
    stringBuffer.append(TEXT_640);
    }
    stringBuffer.append(TEXT_151);
    }
    if (isImplementation && !genModel.isReflectiveDelegation() && genClass.implementsAny(genClass.getEGetGenFeatures())) {
    stringBuffer.append(TEXT_124);
    if (genModel.useClassOverrideAnnotation()) {
    stringBuffer.append(TEXT_121);
    }
    stringBuffer.append(TEXT_641);
    stringBuffer.append(negativeOffsetCorrection);
    stringBuffer.append(TEXT_609);
    for (GenFeature genFeature : genClass.getEGetGenFeatures()) {
    stringBuffer.append(TEXT_610);
    stringBuffer.append(genClass.getQualifiedFeatureID(genFeature));
    stringBuffer.append(TEXT_611);
    if (genFeature.isPrimitiveType()) {
    if (isJDK50) {
    stringBuffer.append(TEXT_642);
    stringBuffer.append(genFeature.getGetAccessor());
    stringBuffer.append(TEXT_599);
    } else if (genFeature.isBooleanType()) {
    stringBuffer.append(TEXT_642);
    stringBuffer.append(genFeature.getGetAccessor());
    stringBuffer.append(TEXT_643);
    } else {
    stringBuffer.append(TEXT_644);
    stringBuffer.append(genFeature.getObjectType(genClass));
    stringBuffer.append(TEXT_91);
    stringBuffer.append(genFeature.getGetAccessor());
    stringBuffer.append(TEXT_323);
    }
    } else if (genFeature.isResolveProxies() && !genFeature.isListType()) {
    stringBuffer.append(TEXT_645);
    stringBuffer.append(genFeature.getGetAccessor());
    stringBuffer.append(TEXT_646);
    stringBuffer.append(genFeature.getAccessorName());
    stringBuffer.append(TEXT_599);
    } else if (genFeature.isMapType()) {
    if (genFeature.isEffectiveSuppressEMFTypes()) {
    stringBuffer.append(TEXT_647);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.common.util.EMap"));
    stringBuffer.append(TEXT_613);
    stringBuffer.append(genFeature.getImportedMapTemplateArguments(genClass));
    stringBuffer.append(TEXT_85);
    stringBuffer.append(genFeature.getGetAccessor());
    stringBuffer.append(TEXT_648);
    stringBuffer.append(genFeature.getGetAccessor());
    stringBuffer.append(TEXT_599);
    } else {
    stringBuffer.append(TEXT_649);
    stringBuffer.append(genFeature.getGetAccessor());
    stringBuffer.append(TEXT_650);
    stringBuffer.append(genFeature.getGetAccessor());
    stringBuffer.append(TEXT_651);
    }
    } else if (genFeature.isWrappedFeatureMapType()) {
    stringBuffer.append(TEXT_647);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.util.FeatureMap"));
    stringBuffer.append(TEXT_627);
    stringBuffer.append(genFeature.getGetAccessor());
    stringBuffer.append(TEXT_652);
    stringBuffer.append(genFeature.getGetAccessor());
    stringBuffer.append(TEXT_599);
    } else if (genFeature.isFeatureMapType()) {
    stringBuffer.append(TEXT_649);
    stringBuffer.append(genFeature.getGetAccessor());
    stringBuffer.append(TEXT_653);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.util.FeatureMap"));
    stringBuffer.append(TEXT_62);
    stringBuffer.append(genFeature.getGetAccessor());
    stringBuffer.append(TEXT_654);
    } else {
    stringBuffer.append(TEXT_642);
    stringBuffer.append(genFeature.getGetAccessor());
    stringBuffer.append(TEXT_599);
    }
    }
    stringBuffer.append(TEXT_446);
    if (genModel.isMinimalReflectiveMethods()) {
    stringBuffer.append(TEXT_655);
    } else {
    stringBuffer.append(TEXT_656);
    }
    stringBuffer.append(TEXT_151);
    }
    if (isImplementation && !genModel.isReflectiveDelegation() && genClass.implementsAny(genClass.getESetGenFeatures())) {
    stringBuffer.append(TEXT_124);
    if (genModel.useGenerics()) {
    for (GenFeature genFeature : genClass.getESetGenFeatures()) {
    if (genFeature.isUncheckedCast(genClass) && !genFeature.isFeatureMapType() && !genFeature.isMapType()) {
    stringBuffer.append(TEXT_68);
    break; }
    }
    }
    if (genModel.useClassOverrideAnnotation()) {
    stringBuffer.append(TEXT_121);
    }
    stringBuffer.append(TEXT_657);
    stringBuffer.append(negativeOffsetCorrection);
    stringBuffer.append(TEXT_609);
    for (GenFeature genFeature : genClass.getESetGenFeatures()) {
    stringBuffer.append(TEXT_610);
    stringBuffer.append(genClass.getQualifiedFeatureID(genFeature));
    stringBuffer.append(TEXT_611);
    if (genFeature.isListType()) {
    if (genFeature.isWrappedFeatureMapType()) {
    stringBuffer.append(TEXT_658);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.util.FeatureMap"));
    stringBuffer.append(TEXT_259);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.util.FeatureMap"));
    stringBuffer.append(TEXT_627);
    stringBuffer.append(genFeature.getGetAccessor());
    stringBuffer.append(TEXT_659);
    } else if (genFeature.isFeatureMapType()) {
    stringBuffer.append(TEXT_658);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.util.FeatureMap"));
    stringBuffer.append(TEXT_62);
    stringBuffer.append(genFeature.getGetAccessor());
    stringBuffer.append(TEXT_660);
    } else if (genFeature.isMapType()) {
    if (genFeature.isEffectiveSuppressEMFTypes()) {
    stringBuffer.append(TEXT_658);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.EStructuralFeature"));
    stringBuffer.append(TEXT_661);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.common.util.EMap"));
    stringBuffer.append(TEXT_613);
    stringBuffer.append(genFeature.getImportedMapTemplateArguments(genClass));
    stringBuffer.append(TEXT_85);
    stringBuffer.append(genFeature.getGetAccessor());
    stringBuffer.append(TEXT_662);
    } else {
    stringBuffer.append(TEXT_658);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.EStructuralFeature"));
    stringBuffer.append(TEXT_663);
    stringBuffer.append(genFeature.getGetAccessor());
    stringBuffer.append(TEXT_660);
    }
    } else {
    stringBuffer.append(TEXT_222);
    stringBuffer.append(genFeature.getGetAccessor());
    stringBuffer.append(TEXT_664);
    stringBuffer.append(genFeature.getGetAccessor());
    stringBuffer.append(TEXT_665);
    stringBuffer.append(genModel.getImportedName("java.util.Collection"));
    if (isJDK50) {
    stringBuffer.append(TEXT_666);
    stringBuffer.append(genFeature.getListItemType(genClass));
    stringBuffer.append(TEXT_667);
    }
    stringBuffer.append(TEXT_668);
    }
    } else if (!isJDK50 && genFeature.isPrimitiveType()) {
    stringBuffer.append(TEXT_669);
    stringBuffer.append(((GAwareGenFeature)genFeature).getSetAccessorName());
    stringBuffer.append(TEXT_670);
    stringBuffer.append(genFeature.getObjectType(genClass));
    stringBuffer.append(TEXT_671);
    stringBuffer.append(genFeature.getPrimitiveValueFunction());
    stringBuffer.append(TEXT_323);
    } else {
    stringBuffer.append(TEXT_672);
    stringBuffer.append(((GAwareGenFeature)genFeature).getSetAccessorName());
    stringBuffer.append(TEXT_91);
    if (genFeature.getTypeGenDataType() == null || !genFeature.getTypeGenDataType().isObjectType() || !genFeature.getRawType().equals(genFeature.getType(genClass))) {
    stringBuffer.append(TEXT_91);
    stringBuffer.append(genFeature.getObjectType(genClass));
    stringBuffer.append(TEXT_85);
    }
    stringBuffer.append(TEXT_673);
    }
    stringBuffer.append(TEXT_674);
    }
    stringBuffer.append(TEXT_446);
    if (genModel.isMinimalReflectiveMethods()) {
    stringBuffer.append(TEXT_675);
    } else {
    stringBuffer.append(TEXT_676);
    }
    stringBuffer.append(TEXT_151);
    }
    if (isImplementation && !genModel.isReflectiveDelegation() && genClass.implementsAny(genClass.getEUnsetGenFeatures())) {
    stringBuffer.append(TEXT_124);
    if (genModel.useClassOverrideAnnotation()) {
    stringBuffer.append(TEXT_121);
    }
    stringBuffer.append(TEXT_677);
    stringBuffer.append(negativeOffsetCorrection);
    stringBuffer.append(TEXT_609);
    for (GenFeature genFeature : genClass.getEUnsetGenFeatures()) {
    stringBuffer.append(TEXT_610);
    stringBuffer.append(genClass.getQualifiedFeatureID(genFeature));
    stringBuffer.append(TEXT_611);
    if (genFeature.isListType() && !genFeature.isUnsettable()) {
    if (genFeature.isWrappedFeatureMapType()) {
    stringBuffer.append(TEXT_658);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.util.FeatureMap"));
    stringBuffer.append(TEXT_627);
    stringBuffer.append(genFeature.getGetAccessor());
    stringBuffer.append(TEXT_678);
    } else {
    stringBuffer.append(TEXT_222);
    stringBuffer.append(genFeature.getGetAccessor());
    stringBuffer.append(TEXT_679);
    }
    } else if (genFeature.isUnsettable()) {
    stringBuffer.append(TEXT_222);
    stringBuffer.append(((GAwareGenFeature)genFeature).getUnsetAccessorName());
    stringBuffer.append(TEXT_599);
    } else if (!genFeature.hasEDefault()) {
    stringBuffer.append(TEXT_680);
    stringBuffer.append(((GAwareGenFeature)genFeature).getSetAccessorName());
    stringBuffer.append(TEXT_258);
    stringBuffer.append(genFeature.getImportedType(genClass));
    stringBuffer.append(TEXT_681);
    } else {
    stringBuffer.append(TEXT_682);
    stringBuffer.append(((GAwareGenFeature)genFeature).getSetAccessorName());
    stringBuffer.append(TEXT_91);
    stringBuffer.append(genFeature.getEDefault());
    stringBuffer.append(TEXT_205);
    }
    stringBuffer.append(TEXT_674);
    }
    stringBuffer.append(TEXT_446);
    if (genModel.isMinimalReflectiveMethods()) {
    stringBuffer.append(TEXT_683);
    } else {
    stringBuffer.append(TEXT_684);
    }
    stringBuffer.append(TEXT_151);
    //Class/eUnset.override.javajetinc
    }
    if (isImplementation && !genModel.isReflectiveDelegation() && genClass.implementsAny(genClass.getEIsSetGenFeatures())) {
    stringBuffer.append(TEXT_124);
    if (genModel.useGenerics()) {
    for (GenFeature genFeature : genClass.getEIsSetGenFeatures()) {
    if (genFeature.isListType() && !genFeature.isUnsettable() && !genFeature.isWrappedFeatureMapType() && !genClass.isField(genFeature) && genFeature.isField() && genClass.getImplementingGenModel(genFeature).isVirtualDelegation()) {
    stringBuffer.append(TEXT_68);
    break; }
    }
    }
    if (genModel.useClassOverrideAnnotation()) {
    stringBuffer.append(TEXT_121);
    }
    stringBuffer.append(TEXT_685);
    stringBuffer.append(negativeOffsetCorrection);
    stringBuffer.append(TEXT_609);
    for (GenFeature genFeature : genClass.getEIsSetGenFeatures()) { String safeNameAccessor = genFeature.getSafeName(); if ("featureID".equals(safeNameAccessor)) { safeNameAccessor = "this." + safeNameAccessor; }
    stringBuffer.append(TEXT_610);
    stringBuffer.append(genClass.getQualifiedFeatureID(genFeature));
    stringBuffer.append(TEXT_611);
    if (genFeature.hasSettingDelegate()) {
    if (genFeature.isUnsettable()) {
    stringBuffer.append(TEXT_686);
    stringBuffer.append(genFeature.getAccessorName());
    stringBuffer.append(TEXT_599);
    } else {
    stringBuffer.append(TEXT_642);
    stringBuffer.append(genFeature.getUpperName());
    stringBuffer.append(TEXT_542);
    }
    } else if (genFeature.isListType() && !genFeature.isUnsettable()) {
    if (genFeature.isWrappedFeatureMapType()) {
    if (genFeature.isVolatile()) {
    stringBuffer.append(TEXT_687);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.util.FeatureMap"));
    stringBuffer.append(TEXT_627);
    stringBuffer.append(genFeature.getGetAccessor());
    stringBuffer.append(TEXT_688);
    } else {
    stringBuffer.append(TEXT_642);
    stringBuffer.append(safeNameAccessor);
    stringBuffer.append(TEXT_689);
    stringBuffer.append(safeNameAccessor);
    stringBuffer.append(TEXT_690);
    }
    } else {
    if (genClass.isField(genFeature)) {
    stringBuffer.append(TEXT_642);
    stringBuffer.append(safeNameAccessor);
    stringBuffer.append(TEXT_689);
    stringBuffer.append(safeNameAccessor);
    stringBuffer.append(TEXT_691);
    } else {
    if (genFeature.isField() && genClass.getImplementingGenModel(genFeature).isVirtualDelegation()) {
    stringBuffer.append(TEXT_222);
    stringBuffer.append(genFeature.getImportedType(genClass));
    stringBuffer.append(TEXT_65);
    stringBuffer.append(safeNameAccessor);
    stringBuffer.append(TEXT_203);
    stringBuffer.append(genFeature.getImportedType(genClass));
    stringBuffer.append(TEXT_204);
    stringBuffer.append(genClass.getQualifiedFeatureID(genFeature));
    stringBuffer.append(positiveOffsetCorrection);
    stringBuffer.append(TEXT_692);
    stringBuffer.append(safeNameAccessor);
    stringBuffer.append(TEXT_689);
    stringBuffer.append(safeNameAccessor);
    stringBuffer.append(TEXT_691);
    } else {
    stringBuffer.append(TEXT_693);
    stringBuffer.append(genFeature.getGetAccessor());
    stringBuffer.append(TEXT_694);
    }
    }
    }
    } else if (genFeature.isUnsettable()) {
    stringBuffer.append(TEXT_642);
    stringBuffer.append(((GAwareGenFeature)genFeature).getIsSetAccessorName());
    stringBuffer.append(TEXT_599);
    } else if (genFeature.isResolveProxies()) {
    if (genClass.isField(genFeature)) {
    stringBuffer.append(TEXT_642);
    stringBuffer.append(safeNameAccessor);
    stringBuffer.append(TEXT_695);
    } else {
    if (genFeature.isField() && genClass.getImplementingGenModel(genFeature).isVirtualDelegation()) {
    stringBuffer.append(TEXT_696);
    stringBuffer.append(genClass.getQualifiedFeatureID(genFeature));
    stringBuffer.append(positiveOffsetCorrection);
    stringBuffer.append(TEXT_697);
    } else {
    stringBuffer.append(TEXT_698);
    stringBuffer.append(genFeature.getAccessorName());
    stringBuffer.append(TEXT_699);
    }
    }
    } else if (!genFeature.hasEDefault()) {
    if (genClass.isField(genFeature)) {
    stringBuffer.append(TEXT_642);
    stringBuffer.append(safeNameAccessor);
    stringBuffer.append(TEXT_695);
    } else {
    if (genFeature.isField() && genClass.getImplementingGenModel(genFeature).isVirtualDelegation()) {
    stringBuffer.append(TEXT_696);
    stringBuffer.append(genClass.getQualifiedFeatureID(genFeature));
    stringBuffer.append(positiveOffsetCorrection);
    stringBuffer.append(TEXT_697);
    } else {
    stringBuffer.append(TEXT_642);
    stringBuffer.append(genFeature.getGetAccessor());
    stringBuffer.append(TEXT_699);
    }
    }
    } else if (genFeature.isPrimitiveType() || genFeature.isEnumType()) {
    if (genClass.isField(genFeature)) {
    if (genClass.isFlag(genFeature)) {
    if (genFeature.isBooleanType()) {
    stringBuffer.append(TEXT_612);
    stringBuffer.append(genClass.getFlagsField(genFeature));
    stringBuffer.append(TEXT_247);
    stringBuffer.append(genFeature.getUpperName());
    stringBuffer.append(TEXT_700);
    stringBuffer.append(genFeature.getEDefault());
    stringBuffer.append(TEXT_7);
    } else {
    stringBuffer.append(TEXT_615);
    stringBuffer.append(genClass.getFlagsField(genFeature));
    stringBuffer.append(TEXT_247);
    stringBuffer.append(genFeature.getUpperName());
    stringBuffer.append(TEXT_701);
    stringBuffer.append(genFeature.getUpperName());
    stringBuffer.append(TEXT_527);
    }
    } else {
    stringBuffer.append(TEXT_642);
    stringBuffer.append(safeNameAccessor);
    stringBuffer.append(TEXT_429);
    stringBuffer.append(genFeature.getEDefault());
    stringBuffer.append(TEXT_7);
    }
    } else {
    if (genFeature.isEnumType() && genFeature.isField() && genClass.getImplementingGenModel(genFeature).isVirtualDelegation()) {
    stringBuffer.append(TEXT_696);
    stringBuffer.append(genClass.getQualifiedFeatureID(genFeature));
    stringBuffer.append(positiveOffsetCorrection);
    stringBuffer.append(TEXT_197);
    stringBuffer.append(genFeature.getEDefault());
    stringBuffer.append(TEXT_702);
    stringBuffer.append(genFeature.getEDefault());
    stringBuffer.append(TEXT_7);
    } else {
    stringBuffer.append(TEXT_642);
    stringBuffer.append(genFeature.getGetAccessor());
    stringBuffer.append(TEXT_703);
    stringBuffer.append(genFeature.getEDefault());
    stringBuffer.append(TEXT_7);
    }
    }
    } else {//datatype
    if (genClass.isField(genFeature)) {
    stringBuffer.append(TEXT_642);
    stringBuffer.append(genFeature.getEDefault());
    stringBuffer.append(TEXT_457);
    stringBuffer.append(safeNameAccessor);
    stringBuffer.append(TEXT_704);
    stringBuffer.append(genFeature.getEDefault());
    stringBuffer.append(TEXT_705);
    stringBuffer.append(safeNameAccessor);
    stringBuffer.append(TEXT_205);
    } else {
    if (genFeature.isField() && genClass.getImplementingGenModel(genFeature).isVirtualDelegation()) {
    stringBuffer.append(TEXT_222);
    stringBuffer.append(genFeature.getImportedType(genClass));
    stringBuffer.append(TEXT_65);
    stringBuffer.append(safeNameAccessor);
    stringBuffer.append(TEXT_203);
    stringBuffer.append(genFeature.getImportedType(genClass));
    stringBuffer.append(TEXT_204);
    stringBuffer.append(genClass.getQualifiedFeatureID(genFeature));
    stringBuffer.append(positiveOffsetCorrection);
    stringBuffer.append(TEXT_197);
    stringBuffer.append(genFeature.getEDefault());
    stringBuffer.append(TEXT_692);
    stringBuffer.append(genFeature.getEDefault());
    stringBuffer.append(TEXT_457);
    stringBuffer.append(safeNameAccessor);
    stringBuffer.append(TEXT_704);
    stringBuffer.append(genFeature.getEDefault());
    stringBuffer.append(TEXT_705);
    stringBuffer.append(safeNameAccessor);
    stringBuffer.append(TEXT_205);
    } else {
    stringBuffer.append(TEXT_642);
    stringBuffer.append(genFeature.getEDefault());
    stringBuffer.append(TEXT_457);
    stringBuffer.append(genFeature.getGetAccessor());
    stringBuffer.append(TEXT_706);
    stringBuffer.append(genFeature.getEDefault());
    stringBuffer.append(TEXT_705);
    stringBuffer.append(genFeature.getGetAccessor());
    stringBuffer.append(TEXT_323);
    }
    }
    }
    }
    stringBuffer.append(TEXT_446);
    if (genModel.isMinimalReflectiveMethods()) {
    stringBuffer.append(TEXT_707);
    } else {
    stringBuffer.append(TEXT_708);
    }
    stringBuffer.append(TEXT_151);
    //Class/eIsSet.override.javajetinc
    }
    if (isImplementation && (!genClass.getMixinGenFeatures().isEmpty() || genClass.hasOffsetCorrection() && !genClass.getGenFeatures().isEmpty())) {
    if (!genClass.getMixinGenFeatures().isEmpty()) {
    stringBuffer.append(TEXT_124);
    if (genModel.useClassOverrideAnnotation()) {
    stringBuffer.append(TEXT_121);
    }
    stringBuffer.append(TEXT_709);
    stringBuffer.append(singleWildcard);
    stringBuffer.append(TEXT_710);
    for (GenClass mixinGenClass : genClass.getMixinGenClasses()) {
    stringBuffer.append(TEXT_711);
    stringBuffer.append(mixinGenClass.getRawImportedInterfaceName());
    stringBuffer.append(TEXT_712);
    stringBuffer.append(negativeOffsetCorrection);
    stringBuffer.append(TEXT_221);
    for (GenFeature genFeature : mixinGenClass.getGenFeatures()) {
    stringBuffer.append(TEXT_713);
    stringBuffer.append(genClass.getQualifiedFeatureID(genFeature));
    stringBuffer.append(TEXT_714);
    stringBuffer.append(mixinGenClass.getQualifiedFeatureID(genFeature));
    stringBuffer.append(TEXT_7);
    }
    stringBuffer.append(TEXT_715);
    }
    stringBuffer.append(TEXT_716);
    }
    stringBuffer.append(TEXT_717);
    if (genModel.useClassOverrideAnnotation()) {
    stringBuffer.append(TEXT_121);
    }
    stringBuffer.append(TEXT_718);
    stringBuffer.append(singleWildcard);
    stringBuffer.append(TEXT_710);
    for (GenClass mixinGenClass : genClass.getMixinGenClasses()) {
    stringBuffer.append(TEXT_711);
    stringBuffer.append(mixinGenClass.getRawImportedInterfaceName());
    stringBuffer.append(TEXT_719);
    for (GenFeature genFeature : mixinGenClass.getGenFeatures()) {
    stringBuffer.append(TEXT_713);
    stringBuffer.append(mixinGenClass.getQualifiedFeatureID(genFeature));
    stringBuffer.append(TEXT_714);
    stringBuffer.append(genClass.getQualifiedFeatureID(genFeature));
    stringBuffer.append(positiveOffsetCorrection);
    stringBuffer.append(TEXT_7);
    }
    stringBuffer.append(TEXT_715);
    }
    if (genClass.hasOffsetCorrection() && !genClass.getGenFeatures().isEmpty()) {
    stringBuffer.append(TEXT_711);
    stringBuffer.append(genClass.getRawImportedInterfaceName());
    stringBuffer.append(TEXT_720);
    stringBuffer.append(negativeOffsetCorrection);
    stringBuffer.append(TEXT_221);
    for (GenFeature genFeature : genClass.getGenFeatures()) {
    stringBuffer.append(TEXT_713);
    stringBuffer.append(genClass.getQualifiedFeatureID(genFeature));
    stringBuffer.append(TEXT_714);
    stringBuffer.append(genClass.getQualifiedFeatureID(genFeature));
    stringBuffer.append(positiveOffsetCorrection);
    stringBuffer.append(TEXT_7);
    }
    stringBuffer.append(TEXT_715);
    }
    stringBuffer.append(TEXT_721);
    }
    if (genModel.isOperationReflection() && isImplementation && (!genClass.getMixinGenOperations().isEmpty() || !genClass.getOverrideGenOperations(genClass.getExtendedGenOperations(), genClass.getImplementedGenOperations()).isEmpty() || genClass.hasOffsetCorrection() && !genClass.getGenOperations().isEmpty())) {
    stringBuffer.append(TEXT_124);
    if (genModel.useClassOverrideAnnotation()) {
    stringBuffer.append(TEXT_121);
    }
    stringBuffer.append(TEXT_722);
    stringBuffer.append(singleWildcard);
    stringBuffer.append(TEXT_710);
    for (GenClass extendedGenClass : genClass.getExtendedGenClasses()) { List<GenOperation> extendedImplementedGenOperations = extendedGenClass.getImplementedGenOperations(); List<GenOperation> implementedGenOperations = genClass.getImplementedGenOperations();
    if (!genClass.getOverrideGenOperations(extendedImplementedGenOperations, implementedGenOperations).isEmpty()) {
    stringBuffer.append(TEXT_711);
    stringBuffer.append(extendedGenClass.getRawImportedInterfaceName());
    stringBuffer.append(TEXT_723);
    for (GenOperation genOperation : extendedImplementedGenOperations) { GenOperation overrideGenOperation = genClass.getOverrideGenOperation(genOperation);
    if (implementedGenOperations.contains(overrideGenOperation)) {
    stringBuffer.append(TEXT_713);
    stringBuffer.append(extendedGenClass.getQualifiedOperationID(genOperation));
    stringBuffer.append(TEXT_714);
    stringBuffer.append(genClass.getQualifiedOperationID(overrideGenOperation));
    stringBuffer.append(positiveOperationOffsetCorrection);
    stringBuffer.append(TEXT_7);
    }
    }
    stringBuffer.append(TEXT_724);
    }
    }
    for (GenClass mixinGenClass : genClass.getMixinGenClasses()) {
    stringBuffer.append(TEXT_711);
    stringBuffer.append(mixinGenClass.getRawImportedInterfaceName());
    stringBuffer.append(TEXT_723);
    for (GenOperation genOperation : mixinGenClass.getGenOperations()) { GenOperation overrideGenOperation = genClass.getOverrideGenOperation(genOperation);
    stringBuffer.append(TEXT_713);
    stringBuffer.append(mixinGenClass.getQualifiedOperationID(genOperation));
    stringBuffer.append(TEXT_714);
    stringBuffer.append(genClass.getQualifiedOperationID(overrideGenOperation != null ? overrideGenOperation : genOperation));
    stringBuffer.append(positiveOperationOffsetCorrection);
    stringBuffer.append(TEXT_7);
    }
    stringBuffer.append(TEXT_715);
    }
    if (genClass.hasOffsetCorrection() && !genClass.getGenOperations().isEmpty()) {
    stringBuffer.append(TEXT_711);
    stringBuffer.append(genClass.getRawImportedInterfaceName());
    stringBuffer.append(TEXT_725);
    stringBuffer.append(negativeOperationOffsetCorrection);
    stringBuffer.append(TEXT_221);
    for (GenOperation genOperation : genClass.getGenOperations()) {
    stringBuffer.append(TEXT_713);
    stringBuffer.append(genClass.getQualifiedOperationID(genOperation));
    stringBuffer.append(TEXT_714);
    stringBuffer.append(genClass.getQualifiedOperationID(genOperation));
    stringBuffer.append(positiveOperationOffsetCorrection);
    stringBuffer.append(TEXT_7);
    }
    stringBuffer.append(TEXT_715);
    }
    stringBuffer.append(TEXT_726);
    }
    if (isImplementation && genModel.isVirtualDelegation()) { String eVirtualValuesField = genClass.getEVirtualValuesField();
    if (eVirtualValuesField != null) {
    stringBuffer.append(TEXT_124);
    if (genModel.useClassOverrideAnnotation()) {
    stringBuffer.append(TEXT_121);
    }
    stringBuffer.append(TEXT_727);
    stringBuffer.append(eVirtualValuesField);
    stringBuffer.append(TEXT_728);
    if (genModel.useClassOverrideAnnotation()) {
    stringBuffer.append(TEXT_121);
    }
    stringBuffer.append(TEXT_729);
    stringBuffer.append(eVirtualValuesField);
    stringBuffer.append(TEXT_730);
    }
    { List<String> eVirtualIndexBitFields = genClass.getEVirtualIndexBitFields(new ArrayList<String>());
    if (!eVirtualIndexBitFields.isEmpty()) { List<String> allEVirtualIndexBitFields = genClass.getAllEVirtualIndexBitFields(new ArrayList<String>());
    stringBuffer.append(TEXT_124);
    if (genModel.useClassOverrideAnnotation()) {
    stringBuffer.append(TEXT_121);
    }
    stringBuffer.append(TEXT_731);
    for (int i = 0; i < allEVirtualIndexBitFields.size(); i++) {
    stringBuffer.append(TEXT_610);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_732);
    stringBuffer.append(allEVirtualIndexBitFields.get(i));
    stringBuffer.append(TEXT_7);
    }
    stringBuffer.append(TEXT_733);
    if (genModel.useClassOverrideAnnotation()) {
    stringBuffer.append(TEXT_121);
    }
    stringBuffer.append(TEXT_734);
    for (int i = 0; i < allEVirtualIndexBitFields.size(); i++) {
    stringBuffer.append(TEXT_610);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_735);
    stringBuffer.append(allEVirtualIndexBitFields.get(i));
    stringBuffer.append(TEXT_736);
    }
    stringBuffer.append(TEXT_737);
    }
    }
    }
    if (genModel.isOperationReflection() && isImplementation && !genClass.getImplementedGenOperations().isEmpty()) {
    stringBuffer.append(TEXT_124);
    if (genModel.useClassOverrideAnnotation()) {
    stringBuffer.append(TEXT_121);
    }
    LOOP: for (GenOperation genOperation : (genModel.isMinimalReflectiveMethods() ? genClass.getImplementedGenOperations() : genClass.getAllGenOperations())) {
    for (GenParameter genParameter : genOperation.getGenParameters()) {
    if (genParameter.isUncheckedCast()) {
    stringBuffer.append(TEXT_68);
    break LOOP;}
    }
    }
    stringBuffer.append(TEXT_738);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.common.util.EList"));
    stringBuffer.append(singleWildcard);
    stringBuffer.append(TEXT_739);
    stringBuffer.append(genModel.getImportedName(isGWT ? "org.eclipse.emf.common.util.InvocationTargetException" : "java.lang.reflect.InvocationTargetException"));
    stringBuffer.append(TEXT_740);
    stringBuffer.append(negativeOperationOffsetCorrection);
    stringBuffer.append(TEXT_609);
    for (GenOperation genOperation : (genModel.isMinimalReflectiveMethods() ? genClass.getImplementedGenOperations() : genClass.getAllGenOperations())) { List<GenParameter> genParameters = genOperation.getGenParameters(); int size = genParameters.size();
    stringBuffer.append(TEXT_610);
    stringBuffer.append(genClass.getQualifiedOperationID(genOperation));
    stringBuffer.append(TEXT_611);
    if (genOperation.isVoid()) {
    stringBuffer.append(TEXT_222);
    stringBuffer.append(genOperation.getName());
    stringBuffer.append(TEXT_91);
    for (int i = 0; i < size; i++) { GenParameter genParameter = genParameters.get(i);
    if (!isJDK50 && genParameter.isPrimitiveType()) {
    stringBuffer.append(TEXT_91);
    }
    if (genParameter.getTypeGenDataType() == null || !genParameter.getTypeGenDataType().isObjectType() || !genParameter.getRawType().equals(genParameter.getType(genClass))) {
    stringBuffer.append(TEXT_91);
    stringBuffer.append(genParameter.getObjectType(genClass));
    stringBuffer.append(TEXT_85);
    }
    stringBuffer.append(TEXT_741);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_85);
    if (!isJDK50 && genParameter.isPrimitiveType()) {
    stringBuffer.append(TEXT_199);
    stringBuffer.append(genParameter.getPrimitiveValueFunction());
    stringBuffer.append(TEXT_23);
    }
    if (i < (size - 1)) {
    stringBuffer.append(TEXT_197);
    }
    }
    stringBuffer.append(TEXT_742);
    } else {
    stringBuffer.append(TEXT_642);
    if (!isJDK50 && genOperation.isPrimitiveType()) {
    stringBuffer.append(TEXT_412);
    stringBuffer.append(genOperation.getObjectType(genClass));
    stringBuffer.append(TEXT_91);
    }
    stringBuffer.append(genOperation.getName());
    stringBuffer.append(TEXT_91);
    for (int i = 0; i < size; i++) { GenParameter genParameter = genParameters.get(i);
    if (!isJDK50 && genParameter.isPrimitiveType()) {
    stringBuffer.append(TEXT_91);
    }
    if (genParameter.getTypeGenDataType() == null || !genParameter.getTypeGenDataType().isObjectType() || !genParameter.getRawType().equals(genParameter.getType(genClass))) {
    stringBuffer.append(TEXT_91);
    stringBuffer.append(genParameter.getObjectType(genClass));
    stringBuffer.append(TEXT_85);
    }
    stringBuffer.append(TEXT_741);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_85);
    if (!isJDK50 && genParameter.isPrimitiveType()) {
    stringBuffer.append(TEXT_199);
    stringBuffer.append(genParameter.getPrimitiveValueFunction());
    stringBuffer.append(TEXT_23);
    }
    if (i < (size - 1)) {
    stringBuffer.append(TEXT_197);
    }
    }
    stringBuffer.append(TEXT_85);
    if (!isJDK50 && genOperation.isPrimitiveType()) {
    stringBuffer.append(TEXT_85);
    }
    stringBuffer.append(TEXT_7);
    }
    }
    stringBuffer.append(TEXT_446);
    if (genModel.isMinimalReflectiveMethods()) {
    stringBuffer.append(TEXT_743);
    } else {
    stringBuffer.append(TEXT_744);
    }
    stringBuffer.append(TEXT_151);
    }
    if (!genClass.hasImplementedToStringGenOperation() && isImplementation && !genModel.isReflectiveDelegation() && !genModel.isDynamicDelegation() && !genClass.getToStringGenFeatures().isEmpty()) {
    stringBuffer.append(TEXT_124);
    if (genModel.useClassOverrideAnnotation()) {
    stringBuffer.append(TEXT_121);
    }
    stringBuffer.append(TEXT_745);
    { boolean first = true;
    for (GenFeature genFeature : genClass.getToStringGenFeatures()) {
    if (first) { first = false;
    stringBuffer.append(TEXT_746);
    stringBuffer.append(genFeature.getName());
    stringBuffer.append(TEXT_747);
    stringBuffer.append(genModel.getNonNLS());
    } else {
    stringBuffer.append(TEXT_748);
    stringBuffer.append(genFeature.getName());
    stringBuffer.append(TEXT_747);
    stringBuffer.append(genModel.getNonNLS());
    }
    if (genFeature.isUnsettable() && !genFeature.isListType()) {
    if (genModel.isVirtualDelegation() && !genFeature.isPrimitiveType()) {
    stringBuffer.append(TEXT_749);
    stringBuffer.append(genClass.getQualifiedFeatureID(genFeature));
    stringBuffer.append(positiveOffsetCorrection);
    stringBuffer.append(TEXT_750);
    stringBuffer.append(genClass.getQualifiedFeatureID(genFeature));
    stringBuffer.append(positiveOffsetCorrection);
    stringBuffer.append(TEXT_751);
    stringBuffer.append(genModel.getNonNLS());
    } else {
    if (genClass.isFlag(genFeature)) {
    if (genFeature.isBooleanType()) {
    stringBuffer.append(TEXT_133);
    if (genClass.isESetFlag(genFeature)) {
    stringBuffer.append(TEXT_91);
    stringBuffer.append(genClass.getESetFlagsField(genFeature));
    stringBuffer.append(TEXT_247);
    stringBuffer.append(genFeature.getUpperName());
    stringBuffer.append(TEXT_752);
    } else {
    stringBuffer.append(genFeature.getUncapName());
    stringBuffer.append(TEXT_398);
    }
    stringBuffer.append(TEXT_753);
    stringBuffer.append(genClass.getFlagsField(genFeature));
    stringBuffer.append(TEXT_247);
    stringBuffer.append(genFeature.getUpperName());
    stringBuffer.append(TEXT_754);
    stringBuffer.append(genModel.getNonNLS());
    } else {
    stringBuffer.append(TEXT_133);
    if (genClass.isESetFlag(genFeature)) {
    stringBuffer.append(TEXT_91);
    stringBuffer.append(genClass.getESetFlagsField(genFeature));
    stringBuffer.append(TEXT_247);
    stringBuffer.append(genFeature.getUpperName());
    stringBuffer.append(TEXT_752);
    } else {
    stringBuffer.append(genFeature.getUncapName());
    stringBuffer.append(TEXT_398);
    }
    stringBuffer.append(TEXT_755);
    stringBuffer.append(genFeature.getUpperName());
    stringBuffer.append(TEXT_249);
    stringBuffer.append(genClass.getFlagsField(genFeature));
    stringBuffer.append(TEXT_247);
    stringBuffer.append(genFeature.getUpperName());
    stringBuffer.append(TEXT_250);
    stringBuffer.append(genFeature.getUpperName());
    stringBuffer.append(TEXT_756);
    stringBuffer.append(genModel.getNonNLS());
    }
    } else {
    stringBuffer.append(TEXT_133);
    if (genClass.isESetFlag(genFeature)) {
    stringBuffer.append(TEXT_91);
    stringBuffer.append(genClass.getESetFlagsField(genFeature));
    stringBuffer.append(TEXT_247);
    stringBuffer.append(genFeature.getUpperName());
    stringBuffer.append(TEXT_752);
    } else {
    stringBuffer.append(genFeature.getUncapName());
    stringBuffer.append(TEXT_398);
    }
    stringBuffer.append(TEXT_755);
    stringBuffer.append(genFeature.getSafeName());
    stringBuffer.append(TEXT_757);
    stringBuffer.append(genModel.getNonNLS());
    }
    }
    } else {
    if (genModel.isVirtualDelegation() && !genFeature.isPrimitiveType()) {
    stringBuffer.append(TEXT_758);
    stringBuffer.append(genClass.getQualifiedFeatureID(genFeature));
    stringBuffer.append(positiveOffsetCorrection);
    if (!genFeature.isListType() && !genFeature.isReferenceType()){
    stringBuffer.append(TEXT_197);
    stringBuffer.append(genFeature.getEDefault());
    }
    stringBuffer.append(TEXT_244);
    } else {
    if (genClass.isFlag(genFeature)) {
    if (genFeature.isBooleanType()) {
    stringBuffer.append(TEXT_759);
    stringBuffer.append(genClass.getFlagsField(genFeature));
    stringBuffer.append(TEXT_247);
    stringBuffer.append(genFeature.getUpperName());
    stringBuffer.append(TEXT_760);
    } else {
    stringBuffer.append(TEXT_761);
    stringBuffer.append(genFeature.getUpperName());
    stringBuffer.append(TEXT_249);
    stringBuffer.append(genClass.getFlagsField(genFeature));
    stringBuffer.append(TEXT_247);
    stringBuffer.append(genFeature.getUpperName());
    stringBuffer.append(TEXT_250);
    stringBuffer.append(genFeature.getUpperName());
    stringBuffer.append(TEXT_762);
    }
    } else {
    stringBuffer.append(TEXT_761);
    stringBuffer.append(genFeature.getSafeName());
    stringBuffer.append(TEXT_205);
    }
    }
    }
    }
    }
    stringBuffer.append(TEXT_763);
    }
    if (isImplementation && genClass.isMapEntry()) { GenFeature keyFeature = genClass.getMapEntryKeyFeature(); GenFeature valueFeature = genClass.getMapEntryValueFeature();
    String objectType = genModel.getImportedName("java.lang.Object");
    String keyType = isJDK50 ? keyFeature.getObjectType(genClass) : objectType;
    String valueType = isJDK50 ? valueFeature.getObjectType(genClass) : objectType;
    String eMapType = genModel.getImportedName("org.eclipse.emf.common.util.EMap") + (isJDK50 ? "<" + keyType + ", " + valueType + ">" : "");
    stringBuffer.append(TEXT_124);
    if (isGWT) {
    stringBuffer.append(TEXT_47);
    stringBuffer.append(genModel.getImportedName("com.google.gwt.user.client.rpc.GwtTransient"));
    }
    stringBuffer.append(TEXT_764);
    stringBuffer.append(objectType);
    stringBuffer.append(TEXT_765);
    stringBuffer.append(keyType);
    stringBuffer.append(TEXT_766);
    if (!isJDK50 && keyFeature.isPrimitiveType()) {
    stringBuffer.append(TEXT_257);
    stringBuffer.append(keyFeature.getObjectType(genClass));
    stringBuffer.append(TEXT_767);
    } else {
    stringBuffer.append(TEXT_768);
    }
    stringBuffer.append(TEXT_769);
    stringBuffer.append(keyType);
    stringBuffer.append(TEXT_770);
    if (keyFeature.isListType()) {
    stringBuffer.append(TEXT_771);
    if (!genModel.useGenerics()) {
    stringBuffer.append(TEXT_91);
    stringBuffer.append(genModel.getImportedName("java.util.Collection"));
    stringBuffer.append(TEXT_85);
    }
    stringBuffer.append(TEXT_772);
    } else if (isJDK50) {
    stringBuffer.append(TEXT_773);
    } else if (keyFeature.isPrimitiveType()) {
    stringBuffer.append(TEXT_774);
    stringBuffer.append(keyFeature.getObjectType(genClass));
    stringBuffer.append(TEXT_775);
    stringBuffer.append(keyFeature.getPrimitiveValueFunction());
    stringBuffer.append(TEXT_323);
    } else {
    stringBuffer.append(TEXT_776);
    stringBuffer.append(keyFeature.getImportedType(genClass));
    stringBuffer.append(TEXT_777);
    }
    stringBuffer.append(TEXT_778);
    stringBuffer.append(valueType);
    stringBuffer.append(TEXT_779);
    if (!isJDK50 && valueFeature.isPrimitiveType()) {
    stringBuffer.append(TEXT_257);
    stringBuffer.append(valueFeature.getObjectType(genClass));
    stringBuffer.append(TEXT_780);
    } else {
    stringBuffer.append(TEXT_781);
    }
    stringBuffer.append(TEXT_778);
    stringBuffer.append(valueType);
    stringBuffer.append(TEXT_782);
    stringBuffer.append(valueType);
    stringBuffer.append(TEXT_783);
    stringBuffer.append(valueType);
    stringBuffer.append(TEXT_784);
    if (valueFeature.isListType()) {
    stringBuffer.append(TEXT_785);
    if (!genModel.useGenerics()) {
    stringBuffer.append(TEXT_91);
    stringBuffer.append(genModel.getImportedName("java.util.Collection"));
    stringBuffer.append(TEXT_85);
    }
    stringBuffer.append(TEXT_786);
    } else if (isJDK50) {
    stringBuffer.append(TEXT_787);
    } else if (valueFeature.isPrimitiveType()) {
    stringBuffer.append(TEXT_788);
    stringBuffer.append(valueFeature.getObjectType(genClass));
    stringBuffer.append(TEXT_789);
    stringBuffer.append(valueFeature.getPrimitiveValueFunction());
    stringBuffer.append(TEXT_323);
    } else {
    stringBuffer.append(TEXT_790);
    stringBuffer.append(valueFeature.getImportedType(genClass));
    stringBuffer.append(TEXT_791);
    }
    stringBuffer.append(TEXT_792);
    if (genModel.useGenerics()) {
    stringBuffer.append(TEXT_68);
    }
    stringBuffer.append(TEXT_128);
    stringBuffer.append(eMapType);
    stringBuffer.append(TEXT_793);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.EObject"));
    stringBuffer.append(TEXT_794);
    stringBuffer.append(eMapType);
    stringBuffer.append(TEXT_795);
    }
    stringBuffer.append(TEXT_796);
    stringBuffer.append(isInterface ? " " + genClass.getInterfaceName() : genClass.getClassName());
    // TODO fix the space above
    genModel.emitSortedImports();
    stringBuffer.append(TEXT_8);
    return stringBuffer.toString();
  }
}
