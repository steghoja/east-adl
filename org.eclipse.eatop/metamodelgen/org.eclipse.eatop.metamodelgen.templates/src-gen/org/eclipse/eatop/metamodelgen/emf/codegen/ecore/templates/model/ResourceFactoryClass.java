package org.eclipse.eatop.metamodelgen.emf.codegen.ecore.templates.model;

import org.eclipse.emf.codegen.ecore.genmodel.*;
import org.eclipse.eatop.metamodelgen.templates.internal.util.GenModels;
import org.eclipse.eatop.metamodelgen.templates.source.*;

public class ResourceFactoryClass
{
  protected static String nl;
  public static synchronized ResourceFactoryClass create(String lineSeparator)
  {
    nl = lineSeparator;
    ResourceFactoryClass result = new ResourceFactoryClass();
    nl = null;
    return result;
  }

  public final String NL = nl == null ? (System.getProperties().getProperty("line.separator")) : nl;
  protected final String TEXT_1 = "";
  protected final String TEXT_2 = "/**";
  protected final String TEXT_3 = NL + " * ";
  protected final String TEXT_4 = NL + " * <copyright>" + NL + " * " + NL + " * Copyright (c) 2014 itemis and others." + NL + " * All rights reserved. This program and the accompanying materials" + NL + " * are made available under the terms of the Eclipse Public License v1.0" + NL + " * which accompanies this distribution, and is available at" + NL + " * http://www.eclipse.org/legal/epl-v10.html" + NL + " * " + NL + " * Contributors: " + NL + " *     itemis - Initial API and implementation" + NL + " * " + NL + " * </copyright>";
  protected final String TEXT_5 = NL + " */" + NL + "package ";
  protected final String TEXT_6 = ";" + NL;
  protected final String TEXT_7 = NL + NL + "/**" + NL + " * <!-- begin-user-doc -->" + NL + " * The <b>Resource Factory</b> associated with the package." + NL + " * <!-- end-user-doc -->" + NL + " * @see ";
  protected final String TEXT_8 = NL + " * @generated" + NL + " */";
  protected final String TEXT_9 = NL + "public class ";
  protected final String TEXT_10 = " extends ";
  protected final String TEXT_11 = NL + "{";
  protected final String TEXT_12 = NL + "\t/**" + NL + "\t * <!-- begin-user-doc -->" + NL + "\t * <!-- end-user-doc -->" + NL + "\t * @generated" + NL + "\t */" + NL + "\tpublic static final ";
  protected final String TEXT_13 = " copyright = ";
  protected final String TEXT_14 = ";";
  protected final String TEXT_15 = NL;
  protected final String TEXT_16 = NL + "\t/**" + NL + "\t * <!-- begin-user-doc -->" + NL + "\t * <!-- end-user-doc -->" + NL + "\t * @generated" + NL + "\t */" + NL + "\tprotected ";
  protected final String TEXT_17 = " extendedMetaData;" + NL;
  protected final String TEXT_18 = " xmlMap = new ";
  protected final String TEXT_19 = "();" + NL;
  protected final String TEXT_20 = NL + "\t";
  protected final String TEXT_21 = NL + "\tpublic static final ";
  protected final String TEXT_22 = " EAST_ADL_";
  protected final String TEXT_23 = "_";
  protected final String TEXT_24 = "_RESOURCE_DESCRIPTOR = new EastADL";
  protected final String TEXT_25 = "ResourceDescriptor();" + NL + "\t";
  protected final String TEXT_26 = "_RESOURCE_DESCRIPTOR = ";
  protected final String TEXT_27 = ".INSTANCE;\t" + NL + "\t";
  protected final String TEXT_28 = NL + "\t/**" + NL + "\t * Creates an instance of the resource factory." + NL + "\t * <!-- begin-user-doc -->" + NL + "\t * <!-- end-user-doc -->" + NL + "\t * @generated" + NL + "\t */" + NL + "\tpublic ";
  protected final String TEXT_29 = "()" + NL + "\t{";
  protected final String TEXT_30 = NL + "\t\tsuper(";
  protected final String TEXT_31 = ".INSTANCE);";
  protected final String TEXT_32 = NL + "\t\tsuper();";
  protected final String TEXT_33 = NL + "\t\textendedMetaData = new ";
  protected final String TEXT_34 = "(new ";
  protected final String TEXT_35 = "(";
  protected final String TEXT_36 = ".Registry.INSTANCE));";
  protected final String TEXT_37 = "\t\t" + NL + "\t\textendedMetaData.putPackage(null, ";
  protected final String TEXT_38 = ".eINSTANCE);";
  protected final String TEXT_39 = "\t\t";
  protected final String TEXT_40 = NL + "\t\txmlMap.setNoNamespacePackage(";
  protected final String TEXT_41 = NL + "\t}" + NL + "" + NL + "\t/**" + NL + "\t * Creates an instance of the resource." + NL + "\t * <!-- begin-user-doc -->" + NL + "\t * <!-- end-user-doc -->" + NL + "\t * @generated" + NL + "\t */";
  protected final String TEXT_42 = NL + "\t@Override";
  protected final String TEXT_43 = NL + "\tpublic Resource createResource(URI uri)" + NL + "\t{";
  protected final String TEXT_44 = NL + "        ";
  protected final String TEXT_45 = " result = new ";
  protected final String TEXT_46 = "(uri);" + NL + "        initResource(result);" + NL + "        return result;";
  protected final String TEXT_47 = "        ";
  protected final String TEXT_48 = "(uri);" + NL + "        result.getDefaultSaveOptions().put(";
  protected final String TEXT_49 = ".OPTION_EXTENDED_META_DATA, ";
  protected final String TEXT_50 = "Boolean.TRUE";
  protected final String TEXT_51 = "extendedMetaData";
  protected final String TEXT_52 = ");" + NL + "        result.getDefaultLoadOptions().put(";
  protected final String TEXT_53 = ");" + NL + "" + NL + "        result.getDefaultSaveOptions().put(";
  protected final String TEXT_54 = ".OPTION_SCHEMA_LOCATION, Boolean.TRUE);" + NL + "" + NL + "        result.getDefaultLoadOptions().put(";
  protected final String TEXT_55 = ".OPTION_USE_ENCODED_ATTRIBUTE_STYLE, Boolean.TRUE);" + NL + "        result.getDefaultSaveOptions().put(";
  protected final String TEXT_56 = ".OPTION_USE_ENCODED_ATTRIBUTE_STYLE, Boolean.TRUE);" + NL + "" + NL + "        result.getDefaultLoadOptions().put(";
  protected final String TEXT_57 = ".OPTION_USE_LEXICAL_HANDLER, Boolean.TRUE);";
  protected final String TEXT_58 = NL + "        result.getDefaultLoadOptions().put(";
  protected final String TEXT_59 = ".OPTION_USE_DATA_CONVERTER, Boolean.TRUE);";
  protected final String TEXT_60 = ".OPTION_XML_MAP, xmlMap);" + NL + "        result.getDefaultLoadOptions().put(";
  protected final String TEXT_61 = ".OPTION_XML_MAP, xmlMap);";
  protected final String TEXT_62 = NL + "        Resource result = new ";
  protected final String TEXT_63 = "(uri);";
  protected final String TEXT_64 = "    return result;";
  protected final String TEXT_65 = NL + "\t}";
  protected final String TEXT_66 = NL + "\t" + NL + "\tprivate static class EastADL";
  protected final String TEXT_67 = "ResourceDescriptor extends ";
  protected final String TEXT_68 = " {" + NL + "  \t" + NL + "\t\tprivate static final String ID = \"org.eclipse.eatop.eastadl";
  protected final String TEXT_69 = "\";" + NL + "\t\tprivate static final String NS_POSTFIX = \"";
  protected final String TEXT_70 = "\";" + NL + "\t\tprivate static final String EPKG_PATTERN = \"";
  protected final String TEXT_71 = "\";" + NL + "\t\tprivate static final String NAME = \"";
  protected final String TEXT_72 = "\";" + NL + "\t\tprivate static final int MAJOR = ";
  protected final String TEXT_73 = ";" + NL + "\t\tprivate static final int MINOR = ";
  protected final String TEXT_74 = ";" + NL + "\t\tprivate static final int REVISION = ";
  protected final String TEXT_75 = ";" + NL + "\t\tprivate static final int ORDINAL = ";
  protected final String TEXT_76 = ";" + NL + "\t\t" + NL + "\t\tprivate EastADL";
  protected final String TEXT_77 = "ResourceDescriptor() {" + NL + "\t\t\tsuper(ID, new ";
  protected final String TEXT_78 = "(NAME, MAJOR, MINOR, REVISION));" + NL + "\t\t}" + NL + "" + NL + "\t\t";
  protected final String TEXT_79 = NL + "\t\t@Override";
  protected final String TEXT_80 = NL + "\t\tpublic String getDefaultContentTypeId() {" + NL + "\t\t\treturn getRootEPackageContentTypeId();" + NL + "\t\t}" + NL + "\t}" + NL + "\t";
  protected final String TEXT_81 = NL + NL + "\t/**" + NL + "\t * <!-- begin-user-doc -->" + NL + "\t * <!-- end-user-doc -->" + NL + "\t * @see org.artop.aal.common.resource.impl.EastADLResourceFactoryImpl#initSchemaLocationBaseURIs()" + NL + "\t * @generated" + NL + "\t */" + NL + "\t";
  protected final String TEXT_82 = NL + "\tprotected void initSchemaLocationBaseURIs() {" + NL + "\t\tschemaLocationURIHandler.addSchemaLocationBaseURI(";
  protected final String TEXT_83 = ".getPlugin(), \"model\"); //$NON-NLS-1$" + NL + "\t}";
  protected final String TEXT_84 = NL + "} //";

  public String generate(Object argument)
  {
    final StringBuffer stringBuffer = new StringBuffer();
    
/**
 * <copyright>
 *
 * Copyright (c) 2002-2006 IBM Corporation and others.
 * All rights reserved.   This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *   IBM - Initial API and implementation
 *
 * </copyright>
 */

    GenPackage genPackage = (GenPackage)argument; GenModel genModel=genPackage.getGenModel();
    stringBuffer.append(TEXT_1);
    stringBuffer.append(TEXT_2);
    {GenBase copyrightHolder = argument instanceof GenBase ? (GenBase)argument : argument instanceof Object[] && ((Object[])argument)[0] instanceof GenBase ? (GenBase)((Object[])argument)[0] : null;
    if (copyrightHolder != null && copyrightHolder.hasCopyright()) {
    stringBuffer.append(TEXT_3);
    stringBuffer.append(copyrightHolder.getCopyright(copyrightHolder.getGenModel().getIndentation(stringBuffer)));
    } else {
    stringBuffer.append(TEXT_4);
    }}
    stringBuffer.append(TEXT_5);
    stringBuffer.append(genPackage.getUtilitiesPackageName());
    stringBuffer.append(TEXT_6);
    genModel.getImportedName("org.eclipse.emf.common.util.URI");
    genModel.getImportedName("org.eclipse.emf.ecore.resource.Resource");
    genModel.markImportLocation(stringBuffer);
    stringBuffer.append(TEXT_7);
    stringBuffer.append(genPackage.getQualifiedResourceClassName());
    stringBuffer.append(TEXT_8);
    if (GenModels.getRootGenPackage(genModel).equals(genPackage)) {
    stringBuffer.append(TEXT_9);
    stringBuffer.append(genPackage.getResourceFactoryClassName());
    stringBuffer.append(TEXT_10);
    stringBuffer.append(genModel.getImportedName("org.eclipse.eatop.common.resource.impl.EastADLResourceFactoryImpl"));
    } else {
    stringBuffer.append(TEXT_9);
    stringBuffer.append(genPackage.getResourceFactoryClassName());
    stringBuffer.append(TEXT_10);
    stringBuffer.append(genPackage.getImportedResourceFactoryBaseClassName());
    }
    //ResourceFactoryClass/extends.override.javajetinc
    stringBuffer.append(TEXT_11);
    if (genModel.hasCopyrightField()) {
    stringBuffer.append(TEXT_12);
    stringBuffer.append(genModel.getImportedName("java.lang.String"));
    stringBuffer.append(TEXT_13);
    stringBuffer.append(genModel.getCopyrightFieldLiteral());
    stringBuffer.append(TEXT_14);
    stringBuffer.append(genModel.getNonNLS());
    stringBuffer.append(TEXT_15);
    }
    if (!GenModels.getRootGenPackage(genModel).equals(genPackage) && genPackage.hasExtendedMetaData() && !genPackage.hasTargetNamespace()) {
    stringBuffer.append(TEXT_16);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.util.ExtendedMetaData"));
    stringBuffer.append(TEXT_17);
    } else if (genPackage.hasXMLMap()) {
    stringBuffer.append(TEXT_16);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.xmi.XMLResource$XMLMap"));
    stringBuffer.append(TEXT_18);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.xmi.impl.XMLMapImpl"));
    stringBuffer.append(TEXT_19);
    }
    if (GenModels.getRootGenPackage(genModel).equals(genPackage)){
    stringBuffer.append(TEXT_20);
    for(EastADLSourceRevision sourceRevision : EastADLSourceRevisions.get(EastADLSourceRevisions.get().major)){
    stringBuffer.append(TEXT_20);
    if(sourceRevision.getRevisionOrdinal() < EastADLSourceRevisions.get().getRevisionOrdinal()){
    stringBuffer.append(TEXT_21);
    stringBuffer.append(genModel.getImportedName("org.eclipse.eatop.common.metamodel.EastADLReleaseDescriptor"));
    stringBuffer.append(TEXT_22);
    stringBuffer.append(sourceRevision.major);
    stringBuffer.append(TEXT_23);
    stringBuffer.append(sourceRevision.minor);
    stringBuffer.append(TEXT_23);
    stringBuffer.append(sourceRevision.revision);
    stringBuffer.append(TEXT_24);
    stringBuffer.append(sourceRevision.major);
    stringBuffer.append(sourceRevision.minor);
    stringBuffer.append(sourceRevision.revision);
    stringBuffer.append(TEXT_25);
    }else{
    stringBuffer.append(TEXT_21);
    stringBuffer.append(genModel.getImportedName("org.eclipse.eatop.common.metamodel.EastADLReleaseDescriptor"));
    stringBuffer.append(TEXT_22);
    stringBuffer.append(sourceRevision.major);
    stringBuffer.append(TEXT_23);
    stringBuffer.append(sourceRevision.minor);
    stringBuffer.append(TEXT_23);
    stringBuffer.append(sourceRevision.revision);
    stringBuffer.append(TEXT_26);
    stringBuffer.append(GenModels.getUtilityClassSimpleName(genModel, "ReleaseDescriptor"));
    stringBuffer.append(TEXT_27);
    }
    stringBuffer.append(TEXT_20);
    }
    }
    stringBuffer.append(TEXT_28);
    stringBuffer.append(genPackage.getResourceFactoryClassName());
    stringBuffer.append(TEXT_29);
    if (GenModels.getRootGenPackage(genModel).equals(genPackage)){
    stringBuffer.append(TEXT_30);
    stringBuffer.append(GenModels.getUtilityClassSimpleName(genModel, "ReleaseDescriptor"));
    stringBuffer.append(TEXT_31);
    } else {
    stringBuffer.append(TEXT_32);
    if (genPackage.hasExtendedMetaData() && !genPackage.hasTargetNamespace()) {
    stringBuffer.append(TEXT_33);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.util.BasicExtendedMetaData"));
    stringBuffer.append(TEXT_34);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.impl.EPackageRegistryImpl"));
    stringBuffer.append(TEXT_35);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.EPackage"));
    stringBuffer.append(TEXT_36);
    if (genPackage.getGenClassifiers().size() > 0) {
    stringBuffer.append(TEXT_37);
    stringBuffer.append(genPackage.getImportedPackageInterfaceName());
    stringBuffer.append(TEXT_38);
    }
    stringBuffer.append(TEXT_39);
    } else if (genPackage.hasXMLMap() && !genPackage.hasTargetNamespace()) {
    stringBuffer.append(TEXT_40);
    stringBuffer.append(genPackage.getImportedPackageInterfaceName());
    stringBuffer.append(TEXT_38);
    }
    }
    //ResourceFactoryClass/defaultConstructor.override.javajetinc
    stringBuffer.append(TEXT_41);
    if (genModel.useClassOverrideAnnotation()) {
    stringBuffer.append(TEXT_42);
    }
    stringBuffer.append(TEXT_43);
    if (GenModels.getRootGenPackage(genModel).equals(genPackage)) {
    stringBuffer.append(TEXT_44);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.xmi.XMLResource"));
    stringBuffer.append(TEXT_45);
    stringBuffer.append(genPackage.getResourceClassName());
    stringBuffer.append(TEXT_46);
    } else {
    stringBuffer.append(TEXT_47);
    if (genPackage.hasExtendedMetaData()) {
    stringBuffer.append(TEXT_44);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.xmi.XMLResource"));
    stringBuffer.append(TEXT_45);
    stringBuffer.append(genPackage.getResourceClassName());
    stringBuffer.append(TEXT_48);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.xmi.XMLResource"));
    stringBuffer.append(TEXT_49);
    if (genPackage.hasTargetNamespace()){
    stringBuffer.append(TEXT_50);
    }else{
    stringBuffer.append(TEXT_51);
    }
    stringBuffer.append(TEXT_52);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.xmi.XMLResource"));
    stringBuffer.append(TEXT_49);
    if (genPackage.hasTargetNamespace()){
    stringBuffer.append(TEXT_50);
    }else{
    stringBuffer.append(TEXT_51);
    }
    stringBuffer.append(TEXT_53);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.xmi.XMLResource"));
    stringBuffer.append(TEXT_54);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.xmi.XMLResource"));
    stringBuffer.append(TEXT_55);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.xmi.XMLResource"));
    stringBuffer.append(TEXT_56);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.xmi.XMLResource"));
    stringBuffer.append(TEXT_57);
    if (genPackage.isDataTypeConverters()) {
    stringBuffer.append(TEXT_58);
    stringBuffer.append(genPackage.getResourceClassName());
    stringBuffer.append(TEXT_59);
    }
    } else if (genPackage.hasXMLMap()) {
    stringBuffer.append(TEXT_44);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.xmi.XMLResource"));
    stringBuffer.append(TEXT_45);
    stringBuffer.append(genPackage.getResourceClassName());
    stringBuffer.append(TEXT_48);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.xmi.XMLResource"));
    stringBuffer.append(TEXT_60);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.xmi.XMLResource"));
    stringBuffer.append(TEXT_61);
    } else {
    stringBuffer.append(TEXT_62);
    stringBuffer.append(genPackage.getResourceClassName());
    stringBuffer.append(TEXT_63);
    }
    stringBuffer.append(TEXT_64);
    }
    //ResourceFactoryClass/createResource.override.javajetinc
    stringBuffer.append(TEXT_65);
    if(GenModels.getRootGenPackage(genModel).equals(genPackage)) {
    stringBuffer.append(TEXT_20);
    for(EastADLSourceRevision sourceRevision : EastADLSourceRevisions.get(EastADLSourceRevisions.get().major)){
    stringBuffer.append(TEXT_20);
    if(sourceRevision.getRevisionOrdinal() < EastADLSourceRevisions.get().getRevisionOrdinal()){
    stringBuffer.append(TEXT_66);
    stringBuffer.append(sourceRevision.major);
    stringBuffer.append(sourceRevision.minor);
    stringBuffer.append(sourceRevision.revision);
    stringBuffer.append(TEXT_67);
    stringBuffer.append(genModel.getImportedName("org.eclipse.eatop.common.metamodel.EastADLReleaseDescriptor"));
    stringBuffer.append(TEXT_68);
    stringBuffer.append(sourceRevision.major);
    stringBuffer.append(sourceRevision.minor);
    stringBuffer.append(sourceRevision.revision);
    stringBuffer.append(TEXT_69);
    stringBuffer.append(sourceRevision.getNamespacePostfix());
    stringBuffer.append(TEXT_70);
    stringBuffer.append(sourceRevision.getEPackageNsURIPostfixPattern());
    stringBuffer.append(TEXT_71);
    stringBuffer.append(sourceRevision.getReleaseLabel());
    stringBuffer.append(TEXT_72);
    stringBuffer.append(sourceRevision.major);
    stringBuffer.append(TEXT_73);
    stringBuffer.append(sourceRevision.minor);
    stringBuffer.append(TEXT_74);
    stringBuffer.append(sourceRevision.revision);
    stringBuffer.append(TEXT_75);
    stringBuffer.append(sourceRevision.getRevisionOrdinal());
    stringBuffer.append(TEXT_76);
    stringBuffer.append(sourceRevision.major);
    stringBuffer.append(sourceRevision.minor);
    stringBuffer.append(sourceRevision.revision);
    stringBuffer.append(TEXT_77);
    stringBuffer.append(genModel.getImportedName("org.eclipse.eatop.common.metamodel.EastADLMetaModelVersionData"));
    stringBuffer.append(TEXT_78);
    if(genModel.useClassOverrideAnnotation()){
    stringBuffer.append(TEXT_79);
    }
    stringBuffer.append(TEXT_80);
    }
    stringBuffer.append(TEXT_20);
    }
    stringBuffer.append(TEXT_81);
    if(genModel.useClassOverrideAnnotation()){
    stringBuffer.append(TEXT_42);
    }
    stringBuffer.append(TEXT_82);
    stringBuffer.append(genModel.getImportedName(genModel.getQualifiedModelPluginClassName()));
    stringBuffer.append(TEXT_83);
    }
    stringBuffer.append(TEXT_84);
    stringBuffer.append(genPackage.getResourceFactoryClassName());
    genModel.emitSortedImports();
    stringBuffer.append(TEXT_15);
    return stringBuffer.toString();
  }
}
