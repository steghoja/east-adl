package org.eclipse.eatop.metamodelgen.emf.codegen.ecore.templates.model;

import java.util.List;
import org.eclipse.emf.codegen.ecore.genmodel.*;
import org.eclipse.eatop.metamodelgen.templates.internal.util.GenModels;
import org.eclipse.eatop.metamodelgen.templates.source.*;
import org.eclipse.eatop.metamodelgen.templates.internal.util.GenModels;

public class PluginProperties
{
  protected static String nl;
  public static synchronized PluginProperties create(String lineSeparator)
  {
    nl = lineSeparator;
    PluginProperties result = new PluginProperties();
    nl = null;
    return result;
  }

  public final String NL = nl == null ? (System.getProperties().getProperty("line.separator")) : nl;
  protected final String TEXT_1 = "# ";
  protected final String TEXT_2 = NL + "#  <copyright>" + NL + "#" + NL + "# Copyright (c) 2014 itemis and others." + NL + "# All rights reserved. This program and the accompanying materials" + NL + "# are made available under the terms of the Eclipse Public License v1.0" + NL + "# which accompanies this distribution, and is available at" + NL + "# http://www.eclipse.org/legal/epl-v10.html" + NL + "# " + NL + "# Contributors: " + NL + "#     itemis - Initial API and implementation" + NL + "# " + NL + "# </copyright>";
  protected final String TEXT_3 = NL + NL + "# ====================================================================" + NL + "# To code developer:" + NL + "#   Do NOT change the properties between this line and the" + NL + "#   \"%%% END OF TRANSLATED PROPERTIES %%%\" line." + NL + "#   Make a new property name, append to the end of the file and change" + NL + "#   the code to use the new property." + NL + "# ====================================================================" + NL + "" + NL + "# ====================================================================" + NL + "# %%% END OF TRANSLATED PROPERTIES %%%" + NL + "# ====================================================================" + NL + "" + NL + "pluginName = ";
  protected final String TEXT_4 = " Meta-model" + NL + "providerName = Eatop.org";
  protected final String TEXT_5 = NL;
  protected final String TEXT_6 = NL + "_UI_";
  protected final String TEXT_7 = "_content_type = ";
  protected final String TEXT_8 = " XML File";

  public String generate(Object argument)
  {
    final StringBuffer stringBuffer = new StringBuffer();
    
/**
 * <copyright>
 *
 * Copyright (c) 2002-2004 IBM Corporation and others.
 * All rights reserved.   This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors: 
 *   IBM - Initial API and implementation
 *
 * </copyright>
 */

    GenModel genModel = (GenModel)argument;
    {GenBase copyrightHolder = argument instanceof GenBase ? (GenBase)argument : argument instanceof Object[] && ((Object[])argument)[0] instanceof GenBase ? (GenBase)((Object[])argument)[0] : null;
    if (copyrightHolder != null && copyrightHolder.hasCopyright()) {
    stringBuffer.append(TEXT_1);
    stringBuffer.append(copyrightHolder.getCopyright(copyrightHolder.getGenModel().getIndentation(stringBuffer)));
    } else {
    stringBuffer.append(TEXT_2);
    }}
    stringBuffer.append(TEXT_3);
    stringBuffer.append(EastADLSourceRevisions.get().getReleaseLabel());
    stringBuffer.append(TEXT_4);
    List<GenPackage> genPackagesWithClassifiers = genModel.getAllGenPackagesWithClassifiers();genPackagesWithClassifiers.add(GenModels.getRootGenPackage(genModel));
    boolean first = true; for (GenPackage genPackage : genPackagesWithClassifiers) {
    if (genPackage.isContentType()) {
    if (first) { first = false;
    stringBuffer.append(TEXT_5);
    }
    stringBuffer.append(TEXT_6);
    stringBuffer.append(genPackage.getPrefix());
    stringBuffer.append(TEXT_7);
    stringBuffer.append(EastADLSourceRevisions.get().getReleaseLabel());
    stringBuffer.append(TEXT_8);
    }
    }
    stringBuffer.append(TEXT_5);
    return stringBuffer.toString();
  }
}
