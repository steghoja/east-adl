package org.eclipse.eatop.metamodelgen.emf.codegen.ecore.templates.model;

import org.eclipse.emf.codegen.ecore.genmodel.*;
import org.eclipse.eatop.metamodelgen.templates.internal.util.GenModels;
import org.eclipse.eatop.metamodelgen.templates.internal.util.EASTADLGenModels;
import org.eclipse.eatop.metamodelgen.templates.source.*;

public class ReleaseDescriptor
{
  protected static String nl;
  public static synchronized ReleaseDescriptor create(String lineSeparator)
  {
    nl = lineSeparator;
    ReleaseDescriptor result = new ReleaseDescriptor();
    nl = null;
    return result;
  }

  public final String NL = nl == null ? (System.getProperties().getProperty("line.separator")) : nl;
  protected final String TEXT_1 = "";
  protected final String TEXT_2 = "/**";
  protected final String TEXT_3 = NL + " * ";
  protected final String TEXT_4 = NL + " * <copyright>" + NL + " * " + NL + " * Copyright (c) 2014 itemis and others." + NL + " * All rights reserved. This program and the accompanying materials" + NL + " * are made available under the terms of the Eclipse Public License v1.0" + NL + " * which accompanies this distribution, and is available at" + NL + " * http://www.eclipse.org/legal/epl-v10.html" + NL + " * " + NL + " * Contributors: " + NL + " *     itemis - Initial API and implementation" + NL + " * " + NL + " * </copyright>";
  protected final String TEXT_5 = NL + " */" + NL + "package ";
  protected final String TEXT_6 = ";" + NL;
  protected final String TEXT_7 = NL + NL + "public class ";
  protected final String TEXT_8 = " extends ";
  protected final String TEXT_9 = " {" + NL + "\t" + NL + "\t/**" + NL + "\t * The id of the content type for ";
  protected final String TEXT_10 = " XML files." + NL + "\t */" + NL + "\tpublic static final String EAXML_CONTENT_TYPE_ID = \"";
  protected final String TEXT_11 = "\"; //$NON-NLS-1$" + NL + "" + NL + "\t/**" + NL + "\t * Identifier." + NL + "\t */" + NL + "\tprivate static final String ID = \"";
  protected final String TEXT_12 = "\";" + NL + "\tprivate static final String NAME = \"";
  protected final String TEXT_13 = "\";" + NL + "\tprivate static final int MAJOR = ";
  protected final String TEXT_14 = ";" + NL + "\tprivate static final int MINOR = ";
  protected final String TEXT_15 = ";" + NL + "\tprivate static final int REVISION = ";
  protected final String TEXT_16 = ";" + NL + "" + NL + "\t/**" + NL + "\t * Default instance." + NL + "\t */" + NL + "\tpublic static final ";
  protected final String TEXT_17 = " INSTANCE = new ";
  protected final String TEXT_18 = "();" + NL + "" + NL + "\t/**" + NL + "\t * Default constructor." + NL + "\t */" + NL + "\tpublic ";
  protected final String TEXT_19 = "() {" + NL + "\t\tsuper(ID, new ";
  protected final String TEXT_20 = "(NAME, MAJOR, MINOR, REVISION));" + NL + "\t}" + NL + "\t";
  protected final String TEXT_21 = NL + "\t@Override";
  protected final String TEXT_22 = NL + "\tpublic String getDefaultContentTypeId() {" + NL + "\t\treturn EAXML_CONTENT_TYPE_ID;" + NL + "\t}" + NL;
  protected final String TEXT_23 = NL + "\tpublic  ";
  protected final String TEXT_24 = "<";
  protected final String TEXT_25 = "> getCompatibleResourceVersionDescriptors() {" + NL + "\t\t";
  protected final String TEXT_26 = "<IMetaModelDescriptor> result = new ";
  protected final String TEXT_27 = "<IMetaModelDescriptor>();";
  protected final String TEXT_28 = NL + "\t\tresult.add(";
  protected final String TEXT_29 = ".EAST_ADL_";
  protected final String TEXT_30 = "_";
  protected final String TEXT_31 = "_RESOURCE_DESCRIPTOR);";
  protected final String TEXT_32 = NL + "\t\treturn ";
  protected final String TEXT_33 = ".unmodifiableList(result);" + NL + "\t}   " + NL + "}";

  public String generate(Object argument)
  {
    final StringBuffer stringBuffer = new StringBuffer();
    
/**
 * <copyright>
 * 
 * Copyright (c) Continental AG and others.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 
 * which accompanies this distribution, and is
 * available at http://www.eclipse.org/org/documents/epl-v10.php
 * 
 * Contributors: 
 *     Continental AG - Initial API and implementation
 * 
 * </copyright>
 */

    GenPackage genPackage = (GenPackage)argument; GenModel genModel=genPackage.getGenModel();
    stringBuffer.append(TEXT_1);
    stringBuffer.append(TEXT_2);
    {GenBase copyrightHolder = argument instanceof GenBase ? (GenBase)argument : argument instanceof Object[] && ((Object[])argument)[0] instanceof GenBase ? (GenBase)((Object[])argument)[0] : null;
    if (copyrightHolder != null && copyrightHolder.hasCopyright()) {
    stringBuffer.append(TEXT_3);
    stringBuffer.append(copyrightHolder.getCopyright(copyrightHolder.getGenModel().getIndentation(stringBuffer)));
    } else {
    stringBuffer.append(TEXT_4);
    }}
    stringBuffer.append(TEXT_5);
    stringBuffer.append(GenModels.getRootGenPackage(genModel).getUtilitiesPackageName());
    stringBuffer.append(TEXT_6);
    genModel.markImportLocation(stringBuffer);
    stringBuffer.append(TEXT_7);
    stringBuffer.append(GenModels.getUtilityClassSimpleName(genModel, "ReleaseDescriptor"));
    stringBuffer.append(TEXT_8);
    stringBuffer.append(genModel.getImportedName("org.eclipse.eatop.common.metamodel.EastADLReleaseDescriptor"));
    stringBuffer.append(TEXT_9);
    stringBuffer.append(EastADLSourceRevisions.get().getReleaseLabel());
    stringBuffer.append(TEXT_10);
    stringBuffer.append(GenModels.getRootGenPackage(genModel).getContentTypeIdentifier());
    stringBuffer.append(TEXT_11);
    stringBuffer.append(EASTADLGenModels.getEastADLMMDescriptorID(genModel));
    stringBuffer.append(TEXT_12);
    stringBuffer.append(EastADLSourceRevisions.get().getReleaseLabel());
    stringBuffer.append(TEXT_13);
    stringBuffer.append(EastADLSourceRevisions.get().major);
    stringBuffer.append(TEXT_14);
    stringBuffer.append(EastADLSourceRevisions.get().minor);
    stringBuffer.append(TEXT_15);
    stringBuffer.append(EastADLSourceRevisions.get().revision);
    stringBuffer.append(TEXT_16);
    stringBuffer.append(GenModels.getUtilityClassSimpleName(genModel, "ReleaseDescriptor"));
    stringBuffer.append(TEXT_17);
    stringBuffer.append(GenModels.getUtilityClassSimpleName(genModel, "ReleaseDescriptor"));
    stringBuffer.append(TEXT_18);
    stringBuffer.append(GenModels.getUtilityClassSimpleName(genModel, "ReleaseDescriptor"));
    stringBuffer.append(TEXT_19);
    stringBuffer.append(genModel.getImportedName("org.eclipse.eatop.common.metamodel.EastADLMetaModelVersionData"));
    stringBuffer.append(TEXT_20);
    if(genModel.useClassOverrideAnnotation()){
    stringBuffer.append(TEXT_21);
    }
    stringBuffer.append(TEXT_22);
    if(genModel.useClassOverrideAnnotation()){
    stringBuffer.append(TEXT_21);
    }
    stringBuffer.append(TEXT_23);
    stringBuffer.append(genModel.getImportedName("java.util.Collection"));
    stringBuffer.append(TEXT_24);
    stringBuffer.append(genModel.getImportedName("org.eclipse.sphinx.emf.metamodel.IMetaModelDescriptor"));
    stringBuffer.append(TEXT_25);
    stringBuffer.append(genModel.getImportedName("java.util.List"));
    stringBuffer.append(TEXT_26);
    stringBuffer.append(genModel.getImportedName("java.util.ArrayList"));
    stringBuffer.append(TEXT_27);
    for(EastADLSourceRevision sourceRevision : EastADLSourceRevisions.get(EastADLSourceRevisions.get().major)){
    if(sourceRevision.getRevisionOrdinal() <= EastADLSourceRevisions.get().getRevisionOrdinal()){
    stringBuffer.append(TEXT_28);
    stringBuffer.append(genPackage.getResourceFactoryClassName());
    stringBuffer.append(TEXT_29);
    stringBuffer.append(sourceRevision.major);
    stringBuffer.append(TEXT_30);
    stringBuffer.append(sourceRevision.minor);
    stringBuffer.append(TEXT_30);
    stringBuffer.append(sourceRevision.revision);
    stringBuffer.append(TEXT_31);
    }
    }
    stringBuffer.append(TEXT_32);
    stringBuffer.append(genModel.getImportedName("java.util.Collections"));
    stringBuffer.append(TEXT_33);
    genModel.emitSortedImports();
    return stringBuffer.toString();
  }
}
