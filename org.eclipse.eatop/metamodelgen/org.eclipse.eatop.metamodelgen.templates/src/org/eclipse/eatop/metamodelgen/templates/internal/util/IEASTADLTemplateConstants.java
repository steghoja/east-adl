/**
 * <copyright>
 *
 * Copyright (c) 2014 itemis and others.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License
 * which accompanies this distribution, and is
 * available at http://www.eclipse.org/org/documents/epl-v10.php
 *
 * Contributors:
 *     itemis - Initial API and implementation
 *
 * </copyright>
 *
 */
package org.eclipse.eatop.metamodelgen.templates.internal.util;

import org.eclipse.eatop.metamodelgen.serialization.generators.util.IEASTADLGeneratorConstants;

@SuppressWarnings("nls")
public interface IEASTADLTemplateConstants {
	/* GEastadl Super Classes */
	public static String GEASTADL_MODEL_NAME = "geastadl";
	public static String FEATURE_MAPPING = "feature.mapping";
	public static String TGT_SEPARATOR = ",";
	public static String TGT_FEATURE_PATH_SEGMENT_SEPARATOR = "/";

	/* EASTADL */
	public static String EATOP_PROJECT_PREFIX = "org.eclipse.eatop.";
	public static String EASTADL_RELEASE = "eastadl";
	public static String EASTADL_ROOT_PACKAGE = "eastadl" + IEASTADLGeneratorConstants.SHORT_VERSION;;
	public static String EASTADL_ROOT_PKG_FNAME = EASTADL_ROOT_PACKAGE + ".Eastadl" + IEASTADLGeneratorConstants.SHORT_VERSION + "Package";

	public static String DEFAULT_NSURI = "http://east-adl.info";
	public static String DEFAULT_EASTADL_FILE_EXTENSION = "eaxml";
	public static String EASTADL_CONTENT_TYPE = "org.eclipse.eatop.eastadl.eastadlXMLFile";

	public static String G_GETTER_PREFIX = "gGet";
	public static String G_SETTER_PREFIX = "gSet";
	public static String GETTER_PREFIX = "get";
	public static String SETTER_PREFIX = "set";

	public static String MIXED_TEXT = "mixedText";

	/* Annotations types */
	public static String ANNOTATION_TAGGED_VALUES = "TaggedValues";
	public static String EAADAPTER = "EAAdapter";
}
