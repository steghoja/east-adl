# General Information
The code in this directory is a condensed and adapted variant of the code in the sibling directory <code>metamodelgen</code>, where some code portions are simply copied and partially slightly adapted.
Thus, the copyright for the copied code portions remains the same, as also can be seen from the corresponding copyright headers.
The code in <code>metamodelgen</code> aims at generating an .ecore metamodel from a metamodel in Enterprise Architect and generating further artifacts (.genmodel, .xsd, and model/edit code).
In contrast, the code in this directory aims at only generating the further artifacts mentioned above from an existing .ecore metamodel without having the overhead of having a dedicated Enterprise Architect adapter.
We opted for this approach of copying and adapting selected code portions from <code>metamodelgen</code>, as extending and subclassing this code would have resulted in an awkward code design.

# Setting up the Development Workspace
1. Install Xtend SDK
2. Import project <code>org.eclipse.eatop/tools/org.eclipse.eatop.targets/2019-12</code> and load target definition <code>2019-12.target</code>
3. Import the projects...
  - ...from <code>org.eclipse.eatop/metamodelgen</code> (sometimes, multiple project refreshs/cleanups and Eclipse restarts are required to get these plugins running with the Xtend SDK and the targed definition):
    - <code>org.eclipse.eatop.metamodelgen.serialization.generators</code>
    - <code>org.eclipse.eatop.metamodelgen.serialization.source</code>
    - <code>org.eclipse.eatop.metamodelgen.serialization.templates</code>
  - ...from <code>org.eclipse.eatop/genmodelcodegen</code>:
    - <code>org.eclipse.eatop.genmodelcodegen</code>
    - <code>org.eclipse.eatop.genmodelcodegen.ui</code>
3. Run the plugins as Eclipse application

# Setting up the Runtime Workspace
1. Import the projects from <code>org.eclipse.eatop/plugins</code>:
   - <code>org.eclipse.eatop.common</code>  
   - <code>org.eclipse.eatop.geastadl</code>
   - <code>org.eclipse.eatop.geastadl.edit</code>
   - <code>org.eclipse.eatop.serialization</code>
2. Create an empty Java project <code>org.eclipse.eatop.eastadl\<majorVersion>\<minorVersion>[\<patchVersion>]</code> (e.g., <code>org.eclipse.eatop.eastadl22</code>), preferrably with JavaSE-1.6 as execution environment JRE (fitting to the execution environment JRE of the other EATOP plugins)
3. Create in this plugin a new folder <code>model</code> and copy an EAST-ADL Ecore metamodel with the name <code>eastadl\<majorVersion>\<minorVersion>[\<patchVersion>].ecore</code> into it. For example, you can use <code>org.eclipse.eatop/plugins/org.eclipse.eatop.eastadl22/model/eastadl22.ecore</code>.
4. Open <code>eastadl\<majorVersion>\<minorVersion>[\<patchVersion>].ecore</code> with a text editor, search for the string <code>geastadl.ecore#</code>, and either...
   - ...(recommended way) check that all corresponding references point to <code>../../org.eclipse.eatop.geastadl/model/geastadl.ecore#</code>. 
   If this is not the case, replace all occurrences with the text editor with this String. 
   Additionally, check that the plugin <code>org.eclipse.eatop.geastadl</code> resides in the file system as a sibling to the plugin <code>org.eclipse.eatop.eastadl\<majorVersion>\<minorVersion>[\<patchVersion>]</code> in the runtime workspace. 
   If this is not the case, re-import this plugin with the option "Copy projects into the workspace" enabled.
   - ...or check that all corresponding references point to <code>./geastadl.ecore#</code>. 
   If this is not the case, replace all occurrences with the text editor with this String. 
   Additionally, copy the files <code>geastadl.ecore</code> and <code>geastadl.genmodel</code> from <code>org.eclipse.eatop.geastadl/model</code> into the same folder than <code>eastadl\<majorVersion>\<minorVersion>[\<patchVersion>].ecore</code>.
5. Right-click the metamodel, select <code>EATOP - Generate EAST-ADL Editing Infrastructure -> Generate EAST-ADL Genmodel, XSD, and Model/Edit Code Plugins</code>, and wait for the operation to finish.
6. Optionally, overwrite/adapt the generated generic icon figures in <code>org.eclipse.eatop.eastadl\<majorVersion>\<minorVersion>[\<patchVersion>].edit/icons</code>.

## Adding new Metaclasses
1. Open the <code>eastadl\<majorVersion>\<minorVersion>[\<patchVersion>].ecore</code> metamodel with the "Sample Ecore Model Editor".
Notes: 
You might have to expliclitly open the properties view for the metamodel via "Window" -> "Show view" -> "Other..." ->  "General" -> "Properties".
You can also edit the metamodel via [class diagrams](https://www.vogella.com/tutorials/EclipseEMF/article.html).
2. Add a new metaclass by right-clicking on the root package <code>eastadl\<majorVersion>\<minorVersion>[\<patchVersion>]</code> and select "New Child" -> "EClass". 
Specify it's name, attributes, and references.
3. To add metadata that is important for the representation in the generated .xsd schema file and thereby the EATOP serialization as well as other EATOP features, add the following EAnnotations via right-clicking the newly created metaclass and selecting "New Child" -> "EAnnotation" -> ...
   1. ..."EAnnotation - http://www.eclipse.org/emf/2002/GenModel": Specify the value for the property "Documentation", which will 

Note: Currently, no subpackages of the root package are supported, as the generation of Enterprise Architect flattened all subpackages.


# Setting up EATOP with the Generated Plugins
1. Import project <code>org.eclipse.eatop/tools/org.eclipse.eatop.targets/2019-12</code> and load target definition <code>2019-12.target</code>
2. Import the projects...
  - ...<code>org.artop.eel.common</code>
  - ...from <code>org.eclipse.eatop/plugins</code>:
    - <code>org.eclipse.eatop.common</code>
    - <code>org.eclipse.eatop.common.ui</code>
    - <code>org.eclipse.eatop.geastadl</code>
    - <code>org.eclipse.eatop.geastadl.edit</code>
    - <code>org.eclipse.eatop.sdk</code>
    - <code>org.eclipse.eatop.serialization</code>
    - <code>org.eclipse.eatop.workspace</code>
    - <code>org.eclipse.eatop.workspace.ui</code>
  - ...from <code>org.eclipse.eatop/examples</code>:
    - <code>org.eclipse.eatop.examples.actions</code>
    - <code>org.eclipse.eatop.examples.common.ui</code>
    - <code>org.eclipse.eatop.examples.editor</code>
    - <code>org.eclipse.eatop.examples.explorer</code>
    - optionally, further plugins like <code>org.eclipse.eatop.demonstrator</code> or graphical editors
  - ...from your runtime workspace, the freshly generated plugins
    - <code>org.eclipse.eatop.eastadl\<majorVersion>\<minorVersion>[\<patchVersion>]</code>
    - <code>org.eclipse.eatop.eastadl\<majorVersion>\<minorVersion>[\<patchVersion>].edit</code>
3. Run the plugins as Eclipse application
