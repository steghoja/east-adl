package org.eclipse.eatop.genmodelcodegen;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.eatop.genmodelcodegen.logging.Logger;
import org.eclipse.eatop.genmodelcodegen.logging.LoggerImpl;
import org.eclipse.eatop.genmodelcodegen.postprocessings.AnalysisPersistencePattern;
import org.eclipse.eatop.genmodelcodegen.postprocessings.CreateGenModel;
import org.eclipse.eatop.genmodelcodegen.postprocessings.CreateReleaseZipFile;
import org.eclipse.eatop.genmodelcodegen.postprocessings.EASTADLAddPersistenceMapping;
import org.eclipse.eatop.genmodelcodegen.postprocessings.EASTADLAdjustGenModel;
import org.eclipse.eatop.genmodelcodegen.postprocessings.EASTADLCreateXSDSchema;
import org.eclipse.eatop.genmodelcodegen.postprocessings.EASTADLGenerateEMFProjects;
import org.eclipse.eatop.genmodelcodegen.postprocessings.MoveGeneratedEditPlugin;
import org.eclipse.eatop.genmodelcodegen.postprocessings.PostProcessingTemplate;
import org.eclipse.eatop.genmodelcodegen.util.IEASTADLConstants;
import org.eclipse.eatop.metamodelgen.serialization.generators.util.IEASTADLGeneratorConstants;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;

public class GenmodelCodeGenerator {
	
	private List<PostProcessingTemplate> postProcessings;
	private IFile ecoreIFile;
	private EPackage model;
	private Logger logger;
	private boolean enableLoggingPostProcessing = true;

	
	public GenmodelCodeGenerator(IFile ecoreFile, boolean generateGenmodelAndCode) {
		this.logger = new LoggerImpl();
		this.postProcessings = new ArrayList<PostProcessingTemplate>();
		this.ecoreIFile = ecoreFile;		
		ResourceSet resSet = new ResourceSetImpl();
        Resource resource = resSet.getResource(URI.createURI(ecoreFile.getLocationURI().toString()), true);        
        this.model = (EPackage)resource.getContents().get(0);		
	}
	
	public EObject generate(IProgressMonitor monitor) throws Exception {
		File ecoreFile = new File(this.ecoreIFile.getLocationURI());
		String projectName = this.ecoreIFile.getProject().getName();
		
		this.postProcessings.add(new EASTADLAddPersistenceMapping(ecoreFile, SubMonitor.convert(monitor, 1)));
		this.postProcessings.add(new AnalysisPersistencePattern());
		this.postProcessings.add(new CreateGenModel(ecoreFile, projectName));
		this.postProcessings.add(new EASTADLAdjustGenModel(ecoreFile, projectName, IEASTADLGeneratorConstants.EASTADL_VERSION, IEASTADLConstants.METAMODEL_TYPE_EASTADL));
		this.postProcessings.add(new EASTADLGenerateEMFProjects(SubMonitor.convert(monitor, 1), ecoreFile, true, true, IEASTADLGeneratorConstants.EASTADL_VERSION));
		this.postProcessings.add(new EASTADLCreateXSDSchema(SubMonitor.convert(monitor, 1), ecoreIFile, IEASTADLGeneratorConstants.EASTADL_VERSION));
//		this.postProcessings.add(new CustomEASTADLCreateXSDSchema(SubMonitor.convert(monitor, 1), ecoreFile, null, ecoreIFile, IEASTADLGeneratorConstants.EASTADL_VERSION));
		this.postProcessings.add(new CreateReleaseZipFile(SubMonitor.convert(monitor, 1), projectName));
		this.postProcessings.add(new MoveGeneratedEditPlugin(SubMonitor.convert(monitor, 1), projectName));
		
		
		for (PostProcessingTemplate p : postProcessings) {
			p.setModel(model);
			p.setLogger(logger);
			if (enableLoggingPostProcessing) {
				p.setLoggingEnabled();
			} else {
				p.setLoggingDisabled();
			}
			model = p.invoke();
		}


		return null;
	}
}
