/**
 * <copyright>
 * 
 * Copyright (c) 2014 Continental AG and others.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 
 * which accompanies this distribution, and is
 * available at http://www.eclipse.org/org/documents/epl-v10.php
 * 
 * Contributors: 
 *     Continental AG - Initial API and implementation
 * 
 * </copyright>
 */

package org.eclipse.eatop.genmodelcodegen.postprocessings;

import java.util.LinkedList;
import java.util.List;

import org.eclipse.eatop.genmodelcodegen.util.IConstants;
import org.eclipse.eatop.genmodelcodegen.logging.Logger;
import org.eclipse.emf.common.util.TreeIterator;
import org.eclipse.emf.ecore.EAnnotation;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.ENamedElement;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EcoreFactory;

public abstract class PostProcessingTemplate {

	protected EPackage model;	
	protected Logger logger;
	protected EcoreFactory ecoreFactory = EcoreFactory.eINSTANCE;

	public abstract void execute() throws Exception;


	protected EPackage findEPackageByName(String name) {
		TreeIterator<EObject> iter = model.eAllContents();
		while (iter.hasNext()) {
			EObject element = iter.next();
			if (element instanceof EPackage) {
				if (((EPackage) element).getName().equals(name)) {
					return (EPackage) element;
				}
			}
		}
		return null;
	}

	protected ENamedElement findENamedElementByGUID(String guid) {
		TreeIterator<EObject> iter = model.eAllContents();
		while (iter.hasNext()) {
			EObject element = iter.next();
			if (element instanceof ENamedElement) {
				EAnnotation metaData = ((ENamedElement) element).getEAnnotation(IConstants.METADATA);
				if (metaData != null) {
					if (element.equals(guid)) {
						return (ENamedElement) element;
					}
				}
			}
		}
		return null;
	}


	protected ENamedElement findENamedElementByName(String name) {
		TreeIterator<EObject> iter = model.eAllContents();
		while (iter.hasNext()) {
			EObject element = iter.next();
			if (element instanceof ENamedElement) {
				if (((ENamedElement) element).getName().equalsIgnoreCase(name)) {
					return (ENamedElement) element;
				}
			}
		}
		return null;
	}

	protected EClassifier findEClassifierByName(String name) {
		TreeIterator<EObject> iter = model.eAllContents();
		while (iter.hasNext()) {
			EObject element = iter.next();
			if (element instanceof EClassifier) {
				if (((EClassifier) element).getName().equalsIgnoreCase(name)) {
					return (EClassifier) element;
				}
			}
		}
		return null;
	}

	protected static String createPathOfEPackage(EPackage pkg) {
		List<EPackage> superPackages = new LinkedList<EPackage>();
		superPackages.add(pkg);

		EPackage p = pkg;
		while (p.getESuperPackage() != null) {
			superPackages.add(0, p.getESuperPackage()); // insert to first place
			p = p.getESuperPackage();
		}

		String path = ""; //$NON-NLS-1$
		for (EPackage p2 : superPackages) {
			path = path + "/" + p2.getName(); //$NON-NLS-1$
		}
		return path;
	}


	public EPackage getModel() {
		return model;
	}

	public void setModel(EPackage model) {
		this.model = model;
	}
	
	public void setLogger(Logger logger) {
		this.logger = logger;
	}

	public Logger getLogger() {
		return logger;
	}

	public void setLoggingEnabled() {
		logger.setLoggingEnabled();
	}

	public void setLoggingDisabled() {
		logger.setLoggingDisabled();
	}

	public EPackage invoke() throws Exception {
		execute();
		return model;
	}
}
