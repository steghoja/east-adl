Build tooling using
mvn install -Declipse.targetrelease=2019-12 -Dplatform.version.name=2019-12

artop.org must be authenticated, see [1]
	Use settings.xml in ~/.m2
	Needs a tycho version >1.4 (currently configured for tycho 2.0)

[1] https://maven.apache.org/guides/mini/guide-encryption.html
